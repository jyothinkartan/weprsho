-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 23, 2022 at 05:13 PM
-- Server version: 8.0.30-0ubuntu0.20.04.2
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `weprsho`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `fullname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `phone` int DEFAULT NULL,
  `city` varchar(191) DEFAULT NULL,
  `state` varchar(191) DEFAULT NULL,
  `country` varchar(191) DEFAULT NULL,
  `postcode` int DEFAULT NULL,
  `landmark` varchar(191) DEFAULT NULL,
  `address` varchar(191) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `user_id`, `fullname`, `phone`, `city`, `state`, `country`, `postcode`, `landmark`, `address`, `created_at`, `updated_at`) VALUES
(7, 1, 'abcabcabc', 2147483647, 'bbbbbbbbbb', 'dsadsarr', 'dfvdxgvdrr', 585858, 'vfdvgdxvgxdrr', 'abcabc abc', '0000-00-00 00:00:00', '2022-05-05 11:42:05'),
(9, 1, 'hello', 985874587, 'kakinada', 'ap', 'india', 874854, 'beside government schooll', '2/34 ghdd dfsfds', '0000-00-00 00:00:00', '2022-05-10 06:46:42'),
(10, 1, 'jyothi fullname', 987656543, 'kakinada', 'telangana', 'india', 485983, 'near blah....', 'abc12345', '0000-00-00 00:00:00', '2022-05-13 17:13:59'),
(11, 3, 'sdsdsdfds', 2147483647, 'fsdfdsf', 'fsf', 'fsfs', 8585, NULL, 'fffff', '0000-00-00 00:00:00', '2022-05-18 11:22:39'),
(12, 4, 'sdsdsdfds', 2147483647, 'fsdfdsf', 'fsf', 'fsfs', 8585, NULL, 'fffff', '0000-00-00 00:00:00', '2022-05-18 11:22:39');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int NOT NULL,
  `name` varchar(191) DEFAULT NULL,
  `email` varchar(191) DEFAULT NULL,
  `mobile` int DEFAULT NULL,
  `password` varchar(191) DEFAULT NULL,
  `role` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `mobile`, `password`, `role`, `status`, `created_at`, `updated_at`) VALUES
(1, 'WePrSho', 'admin', NULL, 'admin', 0, 1, '2022-01-07 09:51:07', '2022-01-07 09:51:07'),
(12, 'kartan', 'kartan@gmail.com', 987656789, 'admin@123', 1, 1, '2022-01-18 04:38:37', '2022-01-18 04:38:37'),
(13, 'flipkart', 'flipkart@gmail.com', 2147483647, '12345', 1, 1, '2022-01-20 03:42:48', '2022-01-20 03:42:48'),
(14, 'amazon', 'amazon@gmail.com', 2147483647, '8127', 1, 0, '2022-01-20 03:43:51', '2022-01-20 03:43:51'),
(15, 'kartan11', 'kartan11@gmail.com', 2147483647, '1630', 1, 1, '2022-01-20 12:35:48', '2022-01-20 12:35:48');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int NOT NULL,
  `added_by` int NOT NULL,
  `image` varchar(191) NOT NULL,
  `title` varchar(191) NOT NULL,
  `published_on` datetime NOT NULL,
  `status` int NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cantact_info`
--

CREATE TABLE `cantact_info` (
  `id` int NOT NULL,
  `name` varchar(191) DEFAULT NULL,
  `email` varchar(191) DEFAULT NULL,
  `message` varchar(191) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `cantact_info`
--

INSERT INTO `cantact_info` (`id`, `name`, `email`, `message`, `created_at`) VALUES
(1, 'dsa', 'vdsvcdv@gmail.ckjdn', 'cdscc', '2022-01-20 09:56:01');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int NOT NULL,
  `cookie_id` int DEFAULT NULL,
  `product_id` int NOT NULL,
  `quantity` int NOT NULL,
  `status` int NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `cookie_id`, `product_id`, `quantity`, `status`, `created_at`, `updated_at`) VALUES
(3, 6155, 3, 1, 0, '2022-05-03 12:36:01', '2022-05-03 12:36:01'),
(4, 1139, 2, 1, 0, '2022-05-03 16:48:46', '2022-05-03 16:48:46'),
(5, 1022, 2, 1, 0, '2022-05-04 06:13:36', '2022-05-04 06:13:36'),
(6, 8034, 1, 1, 0, '2022-05-06 05:06:39', '2022-05-06 05:06:39');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int NOT NULL,
  `image` varchar(191) DEFAULT NULL,
  `ven_id` int DEFAULT NULL,
  `cat_name` varchar(191) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `image`, `ven_id`, `cat_name`, `created_at`) VALUES
(1, '11.jpg', 12, 'desktops', '2022-03-22 09:38:13'),
(2, '2.jpeg', 13, 'cellphones', '2022-03-22 09:44:11'),
(3, 'e80599ea0a8c9e34a2e6ac537842d1a2.jpg', 13, 'flipcart', '2022-03-23 08:06:51'),
(4, 'c7dd52a478881bf4dfcdd2e73fddc526.jpeg', 14, 'food', '2022-03-23 09:28:48'),
(5, 'c471efa63a58272772fb399f01702a6a.jpg', 14, 'electronics', '2022-03-23 09:32:54');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int NOT NULL,
  `fname` varchar(191) DEFAULT NULL,
  `lname` varchar(191) DEFAULT NULL,
  `email` varchar(191) DEFAULT NULL,
  `password` varchar(191) DEFAULT NULL,
  `image` varchar(191) DEFAULT NULL,
  `otp` varchar(191) DEFAULT NULL,
  `otp_verify` int NOT NULL DEFAULT '0',
  `status` int DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `fname`, `lname`, `email`, `password`, `image`, `otp`, `otp_verify`, `status`, `created_at`) VALUES
(1, 'nampellyyy', 'jyothiii', 'abc@gmail.com', '123', NULL, '87177', 1, 1, '2022-03-24 16:01:27'),
(2, 'test', '1', 'asd@gmail.com', 'test@123', NULL, '76293', 1, 1, '2022-04-22 13:03:32'),
(3, 'sdsd', 'sdfds', 'root@gmail.com', 'aaa', NULL, '16982', 0, 1, '2022-05-18 11:22:39'),
(4, 'sdsd', 'sdfds', 'root@gmail.com', 'aaa', NULL, '16622', 1, 1, '2022-05-18 11:22:39');

-- --------------------------------------------------------

--
-- Table structure for table `home_banner`
--

CREATE TABLE `home_banner` (
  `id` int NOT NULL,
  `banner_image` varchar(191) DEFAULT NULL,
  `status` int NOT NULL DEFAULT '1',
  `catid` int DEFAULT NULL,
  `title` varchar(191) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `home_banner`
--

INSERT INTO `home_banner` (`id`, `banner_image`, `status`, `catid`, `title`, `created_at`, `updated_at`) VALUES
(1, '5276c70c534665b26c16b692bf59538e.jpg', 1, 2, 'Desktops', NULL, '2022-04-22 05:59:06'),
(2, '4249fe80f68e4c96cda22d486b46dc8a.jpg', 1, 3, '', NULL, '2022-04-22 05:59:29'),
(3, 'e3bd45daa50fa0f10cdb986da1128a90.jpeg', 1, 4, 'Food', NULL, '2022-04-22 05:59:47');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `order_id` varchar(191) NOT NULL,
  `addres_id` int DEFAULT NULL,
  `payment_type` varchar(191) DEFAULT NULL,
  `payment_id` int DEFAULT NULL,
  `payment_status` varchar(191) DEFAULT NULL,
  `total_price` int DEFAULT NULL,
  `notes` varchar(191) DEFAULT NULL,
  `status` int NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `order_id`, `addres_id`, `payment_type`, `payment_id`, `payment_status`, `total_price`, `notes`, `status`, `date`, `updated_at`) VALUES
(1, 1, 'WPS15590', 8, 'cod', NULL, NULL, 45444, NULL, 1, NULL, '2022-05-09 07:16:14'),
(2, 1, 'WPS73023', 8, 'cod', NULL, NULL, 50, NULL, 1, NULL, '2022-05-09 07:24:08'),
(3, 1, 'WPS19863', 7, 'cod', NULL, NULL, 50, NULL, 1, '0000-00-00', '2022-05-09 07:49:00'),
(4, 1, 'WPS19591', 7, 'cod', NULL, NULL, 50, NULL, 1, '0000-00-00', '2022-05-09 07:49:49'),
(5, 1, 'WPS31882', 7, 'cod', NULL, NULL, 50, NULL, 1, NULL, '2022-05-09 07:51:46'),
(6, 1, 'WPS16183', 8, 'cod', NULL, NULL, 85, NULL, 1, '2022-05-09', '2022-05-09 07:56:38'),
(7, 1, 'WPS12265', 9, 'cod', NULL, NULL, 54, NULL, 1, '2022-05-10', '2022-05-10 06:47:22'),
(8, 1, 'WPS18917', 7, 'cod', NULL, NULL, 46, NULL, 1, '2022-05-13', '2022-05-13 10:08:56'),
(9, 1, 'WPS31020', 10, 'cod', NULL, NULL, 416, NULL, 1, '2022-05-13', '2022-05-13 17:14:31');

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `id` int NOT NULL,
  `order_id` int NOT NULL,
  `product_id` int NOT NULL,
  `quantity` int NOT NULL,
  `price` int NOT NULL,
  `total_price` int DEFAULT NULL,
  `status` int NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `order_products`
--

INSERT INTO `order_products` (`id`, `order_id`, `product_id`, `quantity`, `price`, `total_price`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 45444, 45444, 0, '0000-00-00 00:00:00', '2022-05-09 07:16:14'),
(2, 2, 2, 1, 50, 50, 1, '0000-00-00 00:00:00', '2022-05-09 07:24:08'),
(3, 3, 2, 1, 50, 50, 1, '0000-00-00 00:00:00', '2022-05-09 07:49:00'),
(4, 4, 2, 1, 50, 50, 1, '0000-00-00 00:00:00', '2022-05-09 07:49:50'),
(5, 5, 2, 1, 50, 50, 1, '0000-00-00 00:00:00', '2022-05-09 07:51:46'),
(6, 6, 6, 1, 85, 85, 1, '0000-00-00 00:00:00', '2022-05-09 07:56:39'),
(7, 7, 5, 1, 54, 54, 1, '0000-00-00 00:00:00', '2022-05-10 06:47:22'),
(8, 8, 1, 2, 23, 46, 1, '0000-00-00 00:00:00', '2022-05-13 10:08:56'),
(9, 9, 2, 4, 4, 16, 1, '0000-00-00 00:00:00', '2022-05-13 17:14:31'),
(10, 9, 5, 2, 200, 400, 1, '0000-00-00 00:00:00', '2022-05-13 17:14:31');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int NOT NULL,
  `sku_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `ven_id` int NOT NULL,
  `pro_price` int DEFAULT NULL,
  `is_stock` int DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `status` int NOT NULL DEFAULT '1',
  `description` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `sku_id`, `ven_id`, `pro_price`, `is_stock`, `quantity`, `status`, `description`, `created_at`, `updated_at`) VALUES
(1, '2', 12, 23, 1, 4, 1, 'ntjg', '2022-05-13 08:57:41', '2022-05-13 08:57:41'),
(2, '4', 12, 4, 1, 89, 1, 'nthggg', '2022-05-13 08:59:22', '2022-05-13 08:59:22'),
(3, '2', 11, 89, 1, 3, 1, 'yes we have addededddd', '2022-05-13 12:22:24', '2022-05-13 12:22:24'),
(5, '6', 12, 200, 1, 2, 1, 'yes  its boat', '2022-05-13 17:07:17', '2022-05-13 17:07:17'),
(6, '6', 11, 43, 1, 4, 1, 'its added by kartan vendor', '2022-05-13 17:17:17', '2022-05-13 17:17:17');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int NOT NULL,
  `sku_id` int NOT NULL,
  `image` varchar(191) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `sku_id`, `image`, `created_at`, `updated_at`) VALUES
(9, 2, 'sku292205.jpg', '0000-00-00', '2022-05-12 12:21:56'),
(10, 2, 'sku2922051.jpg', '0000-00-00', '2022-05-12 12:21:56'),
(11, 2, 'sku2922052.jpg', '0000-00-00', '2022-05-12 12:21:57'),
(12, 2, 'sku2922051.jpeg', '0000-00-00', '2022-05-12 12:21:57'),
(13, 3, 'sku316351.jpg', '0000-00-00', '2022-05-12 12:26:56'),
(14, 3, 'sku316351.jpeg', '0000-00-00', '2022-05-12 12:26:56'),
(15, 3, 'sku3163511.jpg', '0000-00-00', '2022-05-12 12:26:57'),
(16, 3, 'sku3163511.jpeg', '0000-00-00', '2022-05-12 12:26:57'),
(17, 4, 'sku455733.jpeg', '0000-00-00', '2022-05-13 08:46:43'),
(18, 4, 'sku4557331.jpg', '0000-00-00', '2022-05-13 08:46:43'),
(19, 4, 'sku4557332.jpg', '0000-00-00', '2022-05-13 08:46:43'),
(20, 6, 'sku6456771.jpg', '0000-00-00', '2022-05-13 17:05:46'),
(21, 6, 'sku6456772.jpg', '0000-00-00', '2022-05-13 17:05:46'),
(22, 6, 'sku6456773.jpg', '0000-00-00', '2022-05-13 17:05:48');

-- --------------------------------------------------------

--
-- Table structure for table `sku`
--

CREATE TABLE `sku` (
  `id` int NOT NULL,
  `sku_id` varchar(191) DEFAULT NULL,
  `pro_name` varchar(191) NOT NULL,
  `pro_image` varchar(191) NOT NULL,
  `cat_id` int NOT NULL,
  `sub_catid` int NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `sku`
--

INSERT INTO `sku` (`id`, `sku_id`, `pro_name`, `pro_image`, `cat_id`, `sub_catid`, `created_at`, `updated_at`) VALUES
(2, 'sku2', 'real me 8', 'sku292205.jpeg', 2, 5, '0000-00-00 00:00:00', '2022-05-12 12:21:53'),
(3, 'sku3', 'shopping', 'sku316351.png', 3, 8, '0000-00-00 00:00:00', '2022-05-12 12:26:56'),
(4, 'sku4', 'reddmii', 'sku455733.jpg', 2, 4, '0000-00-00 00:00:00', '2022-05-13 08:46:41'),
(5, 'sku5', 'rtrtrt', 'sku510324.jpeg', 2, 6, '0000-00-00 00:00:00', '2022-05-13 08:58:42'),
(6, 'sku6', 'boat bluetooth', 'sku645677.jpg', 5, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE `sub_category` (
  `id` int NOT NULL,
  `cat_id` int NOT NULL,
  `subcat_name` varchar(191) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `cat_id`, `subcat_name`, `created_at`, `updated_at`) VALUES
(1, 1, 'desk', '2022-03-22 09:38:15', '2022-03-22 09:38:15'),
(2, 1, 'top', '2022-03-22 09:38:15', '2022-03-22 09:38:15'),
(3, 1, 'lapi', '2022-03-22 09:38:15', '2022-03-22 09:38:15'),
(4, 2, 'redmi', '2022-03-22 09:44:12', '2022-03-22 09:44:12'),
(5, 2, 'realme', '2022-03-22 09:44:12', '2022-03-22 09:44:12'),
(6, 2, 'samsung', '2022-03-22 09:44:12', '2022-03-22 09:44:12'),
(7, 3, 'online', '2022-03-23 08:06:51', '2022-03-23 08:06:51'),
(8, 3, 'shopping', '2022-03-23 08:06:51', '2022-03-23 08:06:51'),
(9, 4, 'veg', '2022-03-23 09:28:48', '2022-03-23 09:28:48'),
(10, 4, 'non-veg', '2022-03-23 09:28:48', '2022-03-23 09:28:48'),
(11, 4, 'cripsy', '2022-03-23 09:28:48', '2022-03-23 09:28:48'),
(12, 4, 'sweet', '2022-03-23 09:28:49', '2022-03-23 09:28:49'),
(13, 5, 'head phones', '2022-03-23 09:32:55', '2022-03-23 09:32:55'),
(14, 5, 'Bluetooth', '2022-03-23 09:32:56', '2022-03-23 09:32:56');

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `token` varchar(191) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `token`
--

INSERT INTO `token` (`id`, `user_id`, `token`, `created_at`, `updated_at`) VALUES
(1, 7, 'QlgvsJUH0mivfNM72VBkouCipcxidUmfiXfE8s6tPsENGupVxRsG3m68LuQWXAHDLRS5VO', '2022-01-28 12:50:47', '2022-01-28 12:50:47'),
(6, 24, 'QIR6enYuat1rJv06L9cSvN3VKx3L9XYn7nMmTRH8m0d7a6IZxUPm9HyvD4rmwKR6a9jO41', '2022-01-28 13:08:26', '2022-01-28 13:08:26'),
(7, 25, 'Xu5IMvQUajgst5e6LldCl5thvXIUJnA4ah3XYylD4mkedWoa4cFuJiGjtSSNrOFWZY5QTS', '2022-01-28 13:09:13', '2022-01-28 13:09:13'),
(12, 26, 'SMRMVuwdbErI7iXx09LyxSaGu4zHWsviYVRveLDP99vAOYCWIZb042M22GprocUu1fZho1', '2022-01-28 13:18:34', '2022-01-28 13:18:34');

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `name` varchar(191) DEFAULT NULL,
  `image` varchar(191) DEFAULT NULL,
  `year` varchar(191) DEFAULT NULL,
  `gst` int DEFAULT NULL,
  `city` varchar(191) DEFAULT NULL,
  `state` varchar(191) DEFAULT NULL,
  `country` varchar(191) DEFAULT NULL,
  `pincode` int DEFAULT NULL,
  `address` longtext,
  `status` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`id`, `user_id`, `name`, `image`, `year`, `gst`, `city`, `state`, `country`, `pincode`, `address`, `status`, `created_at`, `updated_at`) VALUES
(11, 12, 'kartan', 'db930d1c442eb27ff586a28cc007bcf7.jpg', '2021-10-07', 12222, 'chennaiiiiiii', 'tamilnaduuuuuu', 'italy', 2147483647, 'gdghdukshlfisjflffftttttttt', 1, '2022-01-18 04:38:37', '2022-01-18 04:38:37'),
(12, 13, 'flipkart', '08d19dcceac7bd69623a2957ca27c7ca.jpg', '2021-12-02', 44, 'selam', 'tamilnadu', 'italy', 232422, '3456sdfghjmdfghnjxcv b', 1, '2022-01-20 03:42:48', '2022-01-20 03:42:48'),
(13, 14, 'amazon', '7c8e557b8fba043a59cee82afeba4570.jpg', '2022-01-12', 22, 'warangal', 'telangana', 'italy', 34332, 'fgbnmnbvcvxx', 0, '2022-01-20 03:43:51', '2022-01-20 03:43:51'),
(14, 15, 'kartan113433', '7004b4fbde850a9eb82d2404b3b085cd.jpg', '2022-01-11', 12, 'selam', 'tamilnadu', 'russia', 477585, 'fdsbfkjsdnfkjsdnkjfnbgfgb', 1, '2022-01-20 12:35:48', '2022-01-20 12:35:48');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `product_id` int NOT NULL,
  `quantity` int NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`id`, `user_id`, `product_id`, `quantity`, `created_at`, `updated_at`) VALUES
(10, 1, 6, 2, '0000-00-00 00:00:00', '2022-04-18 10:55:13'),
(11, 1, 1, 1, '0000-00-00 00:00:00', '2022-04-20 03:21:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cantact_info`
--
ALTER TABLE `cantact_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_banner`
--
ALTER TABLE `home_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sku`
--
ALTER TABLE `sku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cantact_info`
--
ALTER TABLE `cantact_info`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `home_banner`
--
ALTER TABLE `home_banner`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `sku`
--
ALTER TABLE `sku`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `token`
--
ALTER TABLE `token`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
