<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }




public function product_insert($data){


        if ($this->db->insert('product', $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
}

public function cart_insert($data){


    if ($this->db->insert('cart', $data)) {
        return $this->db->insert_id();
    } else {
        return false;
    }
}
public function insert_order($data){


    if ($this->db->insert('orders', $data)) {
        return $this->db->insert_id();
    } else {
        return false;
    }
}
public function insert_orderpro($data){


    if ($this->db->insert('order_products', $data)) {
        return $this->db->insert_id();
    } else {
        return false;
    }
}


public function check_pro($cid,$proid){
    $this->db->select('*'); 
    $this->db->from('cart');
    $this->db->where('cookie_id',$cid);
    $this->db->where('product_id',$proid);
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $details = $sql->result();
       return $details;
           
        } else {
            return false;
        }
}

    public function get_allorders(){

        $this->db->select('C.id as cid,C.fname,C.lname,O.*'); 
        $this->db->from('customers C');
        $this->db->join('orders O', 'C.id = O.user_id', 'inner'); 
        $this->db->where('O.status =',1);
      $sql = $this->db->get();
            if ($sql->num_rows() > 0) {
    
                $ven_details = $sql->result();
           return $ven_details;
               
            } else {
                return false;
            }
    
    }
    public function get_allordersid($id){

        $this->db->select('C.id as cid,C.fname,C.lname,C.phone,C.email,C.country,C.city,C.state,C.postcode,C.landmark,C.address,O.*'); 
        $this->db->from('customers C');
        $this->db->join('orders O', 'C.id = O.user_id', 'inner'); 
        $this->db->where('O.status =',1);
        $this->db->where('O.id',$id);
      $sql = $this->db->get();
            if ($sql->num_rows() > 0) {
    
                $ven_details = $sql->result();
           return $ven_details;
               
            } else {
                return false;
            }
    
    }

public function select_products($id=false){


 $this->db->select('ven.name,cat.cat_name,scat.subcat_name,pro.*,s.pro_name,s.pro_image,s.sku_id'); 
    $this->db->from('vendor ven');
    $this->db->join('product pro', 'pro.ven_id = ven.id', 'inner'); 
    $this->db->join('sku s', 's.id = pro.sku_id', 'inner'); 
    $this->db->join('category cat', 's.cat_id = cat.id', 'inner'); 
    $this->db->join('sub_category scat', 'scat.id = s.sub_catid', 'inner'); 
    if($id!=false)
    {
        $this->db->where('ven.user_id',$id);
    }
    
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $ven_details = $sql->result();
       return $ven_details;
           
        } else {
            return false;
        }

    }

    public function sku_id($id){

        $this->db->select('sku_id'); 
           $this->db->from('product');
           $this->db->where('id',$id);
         $sql = $this->db->get();
               if ($sql->num_rows() > 0) {
       
                   $details = $sql->result();
              return $details;
                  
               } else {
                   return false;
               }
           }

           public function get_probyid($id){

            $this->db->select('ven.name,pro.*'); 
               $this->db->from('vendor ven');
               $this->db->join('product pro', 'pro.ven_id = ven.id', 'inner'); 
               $this->db->join('admin adm', 'adm.id = ven.user_id', 'inner'); 
           
               $this->db->where('ven.user_id',$id);
             $sql = $this->db->get();
                   if ($sql->num_rows() > 0) {
           
                       $details = $sql->result();
                  return $details;
                      
                   } else {
                       return false;
                   }
               }
    
    public function get_catpro($id){
        $this->db->select('ven.name as venname,ven.id as venid,cat.id as cat_id,cat.cat_name,pro.*,scat.id as scatid,scat.subcat_name,s.pro_name,s.pro_image,s.sku_id,s.id as skuid'); 
           $this->db->from('vendor ven');
           $this->db->join('product pro', 'pro.ven_id = ven.id', 'inner'); 
           $this->db->join('sku s', 's.id = pro.sku_id', 'inner'); 
           $this->db->join('category cat', 'cat.id = s.cat_id', 'inner'); 
           $this->db->join('sub_category scat', 'scat.id = s.sub_catid', 'inner'); 
           $this->db->where('s.cat_id',$id);
           $this->db->where('ven.status =',1);
           $sql = $this->db->get();
               if ($sql->num_rows() > 0) {
       
                   $details = $sql->result();
              return $details;
                  
               } else {
                   return false;
               }
       
           }

    public function get_pro($id){
    $this->db->select('ven.name as venname,ven.id as venid,cat.id as cat_id,cat.cat_name,pro.*,scat.id as scatid,scat.subcat_name,s.pro_name,s.pro_image,s.sku_id,s.id as skuid'); 
        $this->db->from('vendor ven');
        $this->db->join('product pro', 'pro.ven_id = ven.id', 'inner');
        $this->db->join('sku s', 's.id = pro.sku_id', 'inner'); 
        $this->db->join('category cat', 'cat.id = s.cat_id', 'inner'); 
        $this->db->join('sub_category scat', 'scat.id = s.sub_catid', 'inner'); 
        $this->db->where('pro.id',$id);
        $this->db->where('ven.status =',1);
    $sql = $this->db->get();
            if ($sql->num_rows() > 0) {

                $details = $sql->result();
        return $details;
            
            } else {
                return false;
            }

        }

    public function get_related_pro($proid,$catid){
        $this->db->select('ven.name as venname,ven.id as venid,cat.id as cat_id,cat.cat_name,pro.*,scat.id as scatid,scat.subcat_name,s.pro_name,s.pro_image,s.sku_id,s.id as skuid'); 
           $this->db->from('vendor ven');
           $this->db->join('product pro', 'pro.ven_id = ven.id', 'inner'); 
           $this->db->join('sku s', 's.id = pro.sku_id', 'inner'); 
           $this->db->join('category cat', 'cat.id = s.cat_id', 'inner'); 
           $this->db->join('sub_category scat', 'scat.id = s.sub_catid', 'inner'); 
            $this->db->where('scat.cat_id',$catid);
            $this->db->where('pro.id !=',$proid);
            $this->db->where('ven.status =',1);
         $sql = $this->db->get();
               if ($sql->num_rows() > 0) {
       
                   $details = $sql->result();
              return $details;
                  
               } else {
                   return false;
               }
       
           }
    

    public function get_allpro(){

        $this->db->select('P.*,C.cat_name,s.pro_name,s.pro_image');
            $this->db->from('product P');
            $this->db->join('sku s', 's.id = P.sku_id', 'inner'); 
            $this->db->join('category C', 'C.id = s.cat_id', 'inner'); 
            $this->db->where('status =',1);
             $sql = $this->db->get();
                if ($sql->num_rows() > 0) {
        
                    $details = $sql->result();
               return $details;
                   
                } else {
                    return false;
                }
        
        
        }
     public function select_onevendor($id){

            $this->db->select('id,name');
                $this->db->from('vendor');
                $this->db->where('user_id',$id);
                $this->db->where('status =',1);
              $sql = $this->db->get();
                    if ($sql->num_rows() > 0) {
            
                        $details = $sql->result();
                   return $details;
                       
                    } else {
                        return false;
                    }
            
            
        }

public function select_all(){

$this->db->select('id,name');
    $this->db->from('vendor');
    $this->db->where('status =',1);
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $details = $sql->result();
       return $details;
           
        } else {
            return false;
        }


}

public function get_img($id){

    $this->db->select('pro_image'); 
    $this->db->from('product');
    $this->db->where('id',$id);
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $details = $sql->result();
       return $details;
           
        } else {
            return false;
        }

}

public function multiple_images($image = array()){

    if ($this->db->insert('product_images', $image)) {
        return $this->db->insert_id();
    } else {
        return false;
    }

}


public function get_sku($id){

    $this->db->select('S.*,cat.cat_name,scat.subcat_name'); 
    $this->db->from('sku S');
    $this->db->join('category cat', 'cat.id = S.cat_id', 'inner'); 
    $this->db->join('sub_category scat', 'scat.id = S.sub_catid', 'inner'); 
    $this->db->where('S.id',$id);
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $details = $sql->result();
       return $details;
           
        } else {
            return false;
        }

}

public function sku_images($id){

    $this->db->select('*'); 
    $this->db->from('product_images');
    $this->db->where('sku_id',$id);
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $details = $sql->result();
       return $details;
           
        } else {
            return false;
        }

}

public function select_subcat(){

    $this->db->select('*'); 
    $this->db->from('sub_category');
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $details = $sql->result();
       return $details;
           
        } else {
            return false;
        }

}

public function get_cat_id($id){

    $this->db->select('s.cat_id'); 
    $this->db->from('product pro');
    $this->db->join('sku s', 's.id = pro.sku_id', 'inner');
    $this->db->where('pro.id',$id);
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $details = $sql->result();
       return $details;
           
        } else {
            return false;
        }

}

public function get_subcategory($catid){

    $this->db->select('*'); 
    $this->db->from('sub_category');
    $this->db->where('cat_id',$catid);
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $details = $sql->result();
       return $details;
           
        } else {
            return false;
        }

}

public function get_category(){

$this->db->select('cat.*,ven.name');
    $this->db->from('category cat');
        $this->db->join('vendor ven', 'cat.ven_id = ven.id', 'inner');
        
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $details = $sql->result();
       return $details;
           
        } else {
            return false;
        }


}

public function select_sku(){

    $this->db->select('id,sku_id');
        $this->db->from('sku');
      $sql = $this->db->get();
            if ($sql->num_rows() > 0) {
    
                $details = $sql->result();
           return $details;
               
            } else {
                return false;
            }
    
    
    }

public function select_cat(){

$this->db->select('id,cat_name');
    $this->db->from('category');
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $details = $sql->result();
       return $details;
           
        } else {
            return false;
        }


}
public function get_wishlist($cid){

    $this->db->select('W.*,P.id as proid,P.pro_name,P.pro_image,P.pro_price,P.is_stock');
        $this->db->from('wishlist W');
        $this->db->join('product P', 'W.product_id = P.id', 'inner');
        $this->db->where('W.user_id',$cid);
      $sql = $this->db->get();
            if ($sql->num_rows() > 0) {
    
                $details = $sql->result();
           return $details;
               
            } else {
                return false;
            }
    
    
    }
    
    public function get_orders($cid){

        $this->db->select('*');
            $this->db->from('orders');
            $this->db->where('user_id',$cid);
          $sql = $this->db->get();
                if ($sql->num_rows() > 0) {
        
                    $details = $sql->result();
               return $details;
                   
                } else {
                    return false;
                }
        
        
        }
          
    public function get_orders_products($cid){

        $this->db->select('OP.*,s.pro_name,s.pro_image');
            $this->db->from('order_products OP');
            $this->db->join('product P', 'OP.product_id = P.id', 'inner');
            $this->db->join('sku s', 's.id = P.sku_id', 'inner');
            $this->db->where('OP.order_id',$cid);
          $sql = $this->db->get();
                if ($sql->num_rows() > 0) {
        
                    $details = $sql->result();
               return $details;
                   
                } else {
                    return false;
                }
        
        
        }

        public function get_ordered_products($id){

            $this->db->select('OP.*,P.pro_name,P.pro_image,V.name');
                $this->db->from('order_products OP');
                $this->db->join('product P', 'OP.product_id = P.id', 'inner');
                $this->db->join('vendor V', 'V.id = P.ven_id', 'inner');
                $this->db->where('OP.order_id',$id);
              $sql = $this->db->get();
                    if ($sql->num_rows() > 0) {
            
                        $details = $sql->result();
                   return $details;
                       
                    } else {
                        return false;
                    }
    
            }

public function get_cartpro($cid){

    $this->db->select('C.*,P.id as proid,s.pro_name,s.pro_image,P.pro_price,P.sku_id,P.is_stock');
        $this->db->from('cart C');
        $this->db->join('product P', 'C.product_id = P.id', 'inner');
        $this->db->join('sku s', 's.id = P.sku_id', 'inner');
        $this->db->where('C.cookie_id',$cid);
      $sql = $this->db->get();
            if ($sql->num_rows() > 0) {
    
                $details = $sql->result();
           return $details;
               
            } else {
                return false;
            }
    
    
    }

public function cart_update($id,$data){

    $this->db->where('id', $id);
        if ($this->db->update('cart', $data)) {
            return true;
        } else {
            return false;
        }

        
}

public function address_update($id,$data){

    $this->db->where('id', $id);
        if ($this->db->update('address', $data)) {
            return true;
        } else {
            return false;
        }

        
}

public function update_totalprice($id,$data){

    $this->db->where('id', $id);
        if ($this->db->update('orders', $data)) {
            return true;
        } else {
            return false;
        }

        
}
public function wish_update($id,$data){

    $this->db->where('id', $id);
        if ($this->db->update('wishlist', $data)) {
            return true;
        } else {
            return false;
        }

        
}

public function get_qty($id){
    $this->db->select('*');
    $this->db->from('cart');
    $this->db->where('id',$id);
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $details = $sql->result();
       return $details;
           
        } else {
            return false;
        }
}

public function get_wishqty($id){
    $this->db->select('*');
    $this->db->from('wishlist');
    $this->db->where('id',$id);
    $sql = $this->db->get();
        if ($sql->num_rows() > 0) {
            $details = $sql->result();
       return $details;
        } else {
            return false;
        }
}

public function cart_count($cid){
    $this->db->select('*');
    $this->db->from('cart');
    $this->db->where('cookie_id',$cid);
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $details = $sql->result();
       return $details;
           
        } else {
            return false;
        }
}

public function wish_count($cid){
    $this->db->select('*');
    $this->db->from('wishlist');
    $this->db->where('user_id',$cid);
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $details = $sql->result();
       return $details;
           
        } else {
            return false;
        }
}

public function product_update($id,$data){

    $this->db->where('id', $id);
        if ($this->db->update('product', $data)) {
            return true;
        } else {
            return false;
        }

}

function delete_sku($id)
    {
     $this->db->where('id',$id);
        if ($this->db->delete('sku')) {
            $this->db->where('sku_id',$id);
            $this->db->delete('product_images');
        return true;
        }else{
        return false;

        }
    }
 function delete_product($id)
    {
     $this->db->where('id',$id);
        if ($this->db->delete('product')) {

        return true;
        }else{
        return false;

        }
    }

    function empty_cart($id)
    {
     $this->db->where('cookie_id',$id);
        if ($this->db->delete('cart')) {

return true;
}else{
return false;

}


    }
    
    function cart_prodel($id)
    {
     $this->db->where('id',$id);
        if ($this->db->delete('cart')) {

return true;
}else{
return false;
}
    }

    function wish_prodel($id)
    {
     $this->db->where('id',$id);
        if ($this->db->delete('wishlist')) {

return true;
}else{
return false;
}
    }

    function delete_cat($id)
    {
     $this->db->where('id',$id);
        if ($this->db->delete('category')) {

return true;
}else{
return false;
}
    }

public function category_insert($data){



        if ($this->db->insert('category', $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
}

public function subcategory_insert($data){



    if ($this->db->insert('sub_category', $data)) {
        return $this->db->insert_id();
    } else {
        return false;
    }
}


public function add_towishlist($data){



    if ($this->db->insert('wishlist', $data)) {
        return $this->db->insert_id();
    } else {
        return false;
    }
}

public function check_wishlist($uid, $proid){

    $this->db->select('*');
    $this->db->from('wishlist');
    $this->db->where('user_id',$uid);
    $this->db->where('product_id',$proid);
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $details = $sql->result();
       return $details;
           
        } else {
            return false;
        }
}

public function select_allsku(){

    $this->db->select('S.*,cat.cat_name,scat.subcat_name');
    $this->db->from('sku S');
    $this->db->join('category cat', 'S.cat_id = cat.id', 'inner'); 
    $this->db->join('sub_category scat', 'scat.id = S.sub_catid', 'inner'); 
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $details = $sql->result();
       return $details;
           
        } else {
            return false;
        }
}

public function get_skuid(){

    $this->db->select('sku_id');
    $this->db->from('sku');
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $details = $sql->result();
       return $details;
           
        } else {
            return false;
        }
}

    public function add_address($data){


        if ($this->db->insert('address', $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    function delete_adrs($id)
    {
    $this->db->where('id',$id);
        if ($this->db->delete('address')) {

    return true;
    }else{
    return false;
    }
    }


public function insert_sku($data){


    if ($this->db->insert('sku', $data)) {
        return $this->db->insert_id();
    } else {
        return false;
    }
}

public function update_sku($id,$data){

    $this->db->where('id', $id);
        if ($this->db->update('sku', $data)) {
            return true;
        } else {
            return false;
        }

        
}

}