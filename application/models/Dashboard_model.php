<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function banner_insert($data)
    {

        if ($this->db->insert('home_banner', $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }

    }
    
    function get_all()
    {
        
$this->db->select('H.*,C.cat_name,C.id as catid');
$this->db->from('home_banner H');
$this->db->join('category C', 'H.catid = C.id');
$query = $this->db->get();

if ( $query->num_rows() > 0 )
{
    $row = $query->result_array();
    return $row;
}
else{
    return false;
}
    }
    
    function get_cat()
    {
$this->db->select('*');
$this->db->from('category');
$query = $this->db->get();
if ( $query->num_rows() > 0 )
{
$row = $query->result_array();
return $row;
}
else{
return false;
}
    }


function get_banner($id)
{
    
$this->db->select('*');

$this->db->from('home_banner');
$this->db->where('id', $id );
$query = $this->db->get();

if ( $query->num_rows() > 0 )
{
$row = $query->result_array();
return $row;
}
else{
return false;
}



    }

    function checkimg($id)
{
    
$this->db->select('banner_image');

$this->db->from('home_banner');
$this->db->where('id', $id );
$query = $this->db->get();

if ( $query->num_rows() > 0 )
{
$row = $query->row_array();
return $row;
}
else{
return false;
}



    }

    function update_img($id,$data)
    {

      $this->db->where('id', $id);
        if ($this->db->update('home_banner', $data)) {
            return true;
        } else {
            return false;
        }


    }
	function delete_imgs($id)
		{
		 $this->db->where('id',$id);
			if ($this->db->delete('home_banner')) {
	
	return true;
	}else{
	return false;
	}
	
	
		}

}