<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Customer_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }




    function customer_insert($data)
    {

        if ($this->db->insert('customers', $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }

    }

    function otp_check($id,$email,$otp)
    {

    $this->db->select('*');
    $this->db->from('customers');
    $this->db->where('id', $id);
    $this->db->where('email', $email);
    $this->db->where('otp', $otp);
    $query = $this->db->get();
    if ( $query->num_rows() > 0 )
    {
        $row = $query->row_array();
        return $row;
    }
    else{
        return false;
    }

    }
    

    function otp_update($id,$email, $data)

    {
 $this->db->where('id', $id);
 $this->db->where('email', $email);
        if ($this->db->update('customers', $data)) {
            return true;
        } else {
            return false;
        }

        }
        
function update_otpverify($id,$data)

    {
 $this->db->where('id', $id);
        if ($this->db->update('customers', $data)) {
            return true;
        } else {
            return false;
        }

        }
        
        function get_email($email)

    {

$this->db->select('*');

    $this->db->from('customers');

    $this->db->where('email', $email );
    

    $query = $this->db->get();

    if ( $query->num_rows() > 0 )
    {
        $row = $query->row_array();
        return $row;
    }
    else{
        return false;
    }

    }


  function get_otp($email,$otp)

    {
        $this->db->select('*');

    $this->db->from('customers');

    $this->db->where('email', $email );
    $this->db->where('otp', $otp );

    $query = $this->db->get();

    if ( $query->num_rows() > 0 )
    {
        $row = $query->row_array();
        return $row;
    }
    else{
        return false;
    }
    }


function get_inserttoken($id)
{

        $this->db->select('token');
        $this->db->from('token');
        $this->db->where('id', $id );
    $query = $this->db->get();

    if ( $query->num_rows() > 0 )
    {
        $row = $query->row_array();
        return $row;
    }
    else{
        return false;
    }

}



function get_token($id)
{

        $this->db->select('token');
        $this->db->from('token');
        $this->db->where('user_id', $id );
    $query = $this->db->get();

    if ( $query->num_rows() > 0 )
    {
        $row = $query->row_array();
        return $row;
    }
    else{
        return false;
    }

}
    function create_token($data)
    {

if ($this->db->insert('token', $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }


    }

    function api_clogin($email,$password)
    {

$this->db->select('*');

    $this->db->from('customers');

    $this->db->where('email', $email );
    $this->db->where('password', $password );

    $query = $this->db->get();

    if ( $query->num_rows() > 0 )
    {
        $row = $query->row_array();
        return $row;
    }
    else{
        return false;
    }


    }
  function api_verifylogin($email,$password)
    {

$this->db->select('*');

    $this->db->from('customers');

    $this->db->where('email', $email );
    $this->db->where('password', $password );
  $this->db->where('otp_verify', 1 );
    $query = $this->db->get();

    if ( $query->num_rows() > 0 )
    {
        $row = $query->row_array();
        return $row;
    }
    else{
        return false;
    }





    }

    

    function api_customer($data)
    {

        if ($this->db->insert('customers', $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }

    }







    function contact_insert($data)
    {

        if ($this->db->insert('cantact_info', $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }

    }

    function customer_login($email,$password)
    {

        $this->db->select('*');
        $this->db->from('customers');
        $this->db->where('email',$email);
        $this->db->where('password',$password);
        $result = $this->db->get();

        if($result->num_rows() > 0)
        {
            return $result->result();
        }
        else
        {
            return false;
        }

    }
      
    function customer_statuscheck($email,$password)
    {

        $this->db->select('*');
        $this->db->from('customers');
        $this->db->where('email',$email);
        $this->db->where('password',$password);
        $this->db->where('status =',1);
        $result = $this->db->get();

        if($result->num_rows() > 0)
        {
            return $result->result();
        }
        else
        {
            return false;
        }

    }
    
   




}