<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Vendor_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

function add_admin($data)
    {

        if ($this->db->insert('admin', $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }

    }

    

function add_org($data)
    {

        if ($this->db->insert('vendor', $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }

    }

    function select_all()
    {
    $this->db->select('*');
    $this->db->from('vendor');
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $ven_details = $sql->result();
       return $ven_details;
           
        } else {
            return false;
        }


    }

function show_category()
    {
    $this->db->select('*');
    $this->db->from('category');
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $org_details = $sql->result();
       return $org_details;
           
        } else {
            return false;
        }



    }

    function get_address($id)
    {
    $this->db->select('*');
    $this->db->from('address');
    $this->db->where('user_id',$id);
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $org_details = $sql->result();
       return $org_details;
           
        } else {
            return false;
        }


    }

    function get_customer($id)
    {
    $this->db->select('*');
    $this->db->from('customers');
    $this->db->where('id',$id);
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $org_details = $sql->result();
       return $org_details;
           
        } else {
            return false;
        }


    }
    

    function get_oneaddress($adrsid)
    {
    $this->db->select('*');
    $this->db->from('address');
    $this->db->where('id',$adrsid);
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $org_details = $sql->result();
       return $org_details;
           
        } else {
            return false;
        }


    }
    

    function get_orderproducts($coockieid)
    {
        $this->db->select('C.*,s.pro_name,P.pro_price'); 
        $this->db->from('cart C');
        $this->db->join('product P', 'P.id = C.product_id', 'inner');  
        $this->db->join('sku s', 's.id = P.sku_id', 'inner'); 
        $this->db->where('C.cookie_id',$coockieid);
      $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $org_details = $sql->result();
       return $org_details; 
           
        } else {
            return false;
        }


    }
    function show_allsubcategory($id)
    {
    $this->db->select('S.*,P.pro_name,P.pro_image,P.pro_price');
    $this->db->from('sub_category S');
    $this->db->join('product P', 'P.subcat_id = S.id', 'inner');  
    $this->db->where('cat_id',$id);
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $org_details = $sql->result();
       return $org_details;
           
        } else {
            return false;
        }


    }
    
    function show_subcategory($id)
    {
    $this->db->select('*');
    $this->db->from('sub_category');
    $this->db->where('cat_id',$id);
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $org_details = $sql->result();
       return $org_details;
           
        } else {
            return false;
        }
    }

    function show_subcategoryid($id)
    {
    $this->db->select('P.*,s.pro_name,s.pro_image');
    $this->db->from('product P');
    $this->db->join('sku s', 's.id = P.sku_id', 'inner');
    $this->db->where('s.sub_catid',$id);
    $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $org_details = $sql->result();
       return $org_details;
           
        } else {
            return false;
        }
    }


      function get_ven($id)
    {
    $this->db->select('*');
    $this->db->from('vendor');
     $this->db->where('id',$id);
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $ven_details = $sql->result();
       return $ven_details;
           
        } else {
            return false;
        }


    }
    
      function get_venbyid($id)
    {
    
 
 $this->db->select('ven.*'); 
    $this->db->from('vendor ven');
    $this->db->join('admin adm', 'adm.id = ven.user_id', 'inner'); 
    $this->db->where('ven.user_id',$id);
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $ven_details = $sql->result();
       return $ven_details;
           
        } else {
            return false;
        }



    }



    function get_admin($id)
    {
    
    $this->db->select('*');
    $this->db->from('admin');
     $this->db->where('id',$id);
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $details = $sql->result();
       return $details;
           
        } else {
            return false;
        }


    }
    function get_adminid($id)
    {
    
    $this->db->select('user_id');
    $this->db->from('vendor');
     $this->db->where('id',$id);
  $sql = $this->db->get();
        if ($sql->num_rows() > 0) {

            $details = $sql->row_array();
       return $details;
           
        } else {
            return false;
        }


    }
    
 function delete_ven($id)
    {
     $this->db->where('id',$id);
        if ($this->db->delete('vendor')) {

return true;
}else{
return false;
}


    }


function update_ven($id,$data)
    {

      $this->db->where('id', $id);
        if ($this->db->update('vendor', $data)) {
            return true;
        } else {
            return false;
        }


    }

function status_update($id,$data)
    {

      $this->db->where('id', $id);
        if ($this->db->update('vendor', $data)) {
            return true;
        } else {
            return false;
        }


    }
    
    function admin_status_update($id,$data)
    {

      $this->db->where('id', $id);
        if ($this->db->update('admin', $data)) {
            return true;
        } else {
            return false;
        }


    }

    function search_result($search)
    {
        $this->db->select('P.*,C.cat_name,S.subcat_name'); 
        $this->db->from('product P');
        $this->db->join('category C', 'P.cat_id = C.id', 'inner'); 
        $this->db->join('sub_category S', 'P.subcat_id = S.id', 'inner');
        $this->db->like('P.pro_name',$search);
        $this->db->like('C.cat_name',$search);
        $this->db->like('S.subcat_name',$search);
      $sql = $this->db->get();
            if ($sql->num_rows() > 0) {
    
                $ven_details = $sql->result();
           return $ven_details;
               
            } else {
                return false;
            }

    }
    


}