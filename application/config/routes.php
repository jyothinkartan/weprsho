<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//Admin
$route['admin'] = 'admin/Login';
$route['admin/login'] = 'admin/Login/check';
$route['admin/banner'] = 'admin/Dashboard/banner';
$route['admin/add_bannerimg'] = 'admin/Dashboard/add_banner';
$route['admin/edit_ban/(:num)'] = 'admin/Dashboard/edit_ban/$1';
$route['admin/delete_ban/(:num)'] = 'admin/Dashboard/delete_ban/$1';
$route['admin/blog'] = 'admin/Dashboard/blog';
$route['admin/add_blog'] = 'admin/Dashboard/add_blog';
$route['admin/employee'] = 'admin/Dashboard/employee';
$route['admin/insert_img'] = 'admin/Dashboard/insert_image';
$route['admin/category'] = 'admin/Category/category';
$route['admin/add_category'] = 'admin/Category/add_category';
$route['admin/delete_cat/(:num)'] = 'admin/Product/delete_cat/$1';
$route['admin/insert_category'] = 'admin/Category/insert_category';
$route['admin/sub_category'] = 'admin/Category/sub_category';
$route['admin/getsub_category/(:num)'] = 'admin/Category/getsub_category/$1';
$route['admin/delete_product/(:num)'] = 'admin/Product/delete_product/$1';
$route['admin/sku'] = 'admin/Product/sku';
$route['admin/add_sku'] = 'admin/Product/add_sku';
$route['admin/insert_sku'] = 'admin/Product/insert_sku';
$route['admin/edit_sku/(:num)'] = 'admin/Product/edit_sku/$1';
$route['admin/sku_images/(:num)'] = 'admin/Product/sku_images/$1';
$route['admin/delete_sku/(:num)'] = 'admin/Product/delete_sku/$1';
$route['admin/vendor'] = 'admin/Vendor/Vendor';
$route['admin/add_vendor'] = 'admin/vendor/add_vendor';
$route['admin/view_vendor/(:num)'] = 'admin/Vendor/view_vendor/$1';
$route['admin/edit_vendor/(:num)'] = 'admin/Vendor/edit_vendor/$1';
$route['admin/delete_vendor/(:num)'] = 'admin/Vendor/delete_vendor/$1';
$route['admin/ven_insert'] = 'admin/Vendor/ven_insert';
$route['admin/makeactive/(:num)'] = 'admin/Vendor/ven_makeactive/$1';
$route['admin/makeinactive/(:num)'] = 'admin/Vendor/ven_makeinactive/$1';
$route['admin/product'] = 'admin/Product/index';
$route['admin/orders'] = 'admin/Order/index';
$route['admin/order_products/(:num)'] = 'admin/Order/order_products/$1';
$route['admin/add_product'] = 'admin/Product/add_product';
$route['admin/insert_product'] = 'admin/Product/insert_product';
$route['admin/edit_product/(:num)'] = 'admin/Product/edit_product/$1';

$route['admin/customers'] = 'admin/Login/customers';


$route['admin/view_customers/(:num)'] = 'admin/Login/view_customers/$1';

/*
|
| API routing
|
*/

$route['api/signin'] = 'api/Api_login/index';
$route['api/signup'] = 'api/Api_login/signup';

$route['api/otp_verification'] = 'api/Api_login/otp_verification';
$route['api/resend_otp'] = 'api/Api_login/resend_otp';


/*
|
| Front end  routing
|
*/
$route['signup']='Home/sign_up';
$route['signin']='Home/sign_in';
$route['signout']='Home/signout';
$route['otp']='Home/otp';
$route['otpcheck']='Home/otpcheck';
$route['check']='Home/customer_check';
$route['insert_user']='Home/insert_user';
$route['shop'] = 'Home/shop';
$route['search'] = 'Home/search';
$route['cantact']='Home/cantact';
$route['blog']='Home/blog';
$route['cantact_info']='Home/cantact_info';
$route['category/(:num)']='Home/get_category/$1';
$route['subcategory/(:num)']='Home/get_subcategory/$1';
$route['product/(:num)']='Home/product_details/$1';
$route['cart']='Cart/index/';
$route['dashboard']='Customer/dashboard';
$route['checkout']='Cart/checkout';
$route['wishlist']='Customer/wishlist';
$route['placeorder']='Customer/place_order';
$route['editaddress/(:num)']='Customer/edit_address/$1';
$route['updateaddress']='Customer/update_address';
$route['customeraddress']='Customer/customer_address';
$route['success']='Customer/success';
$route['track/(:num)']='Customer/track/$1';
