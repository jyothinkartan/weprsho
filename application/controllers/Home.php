<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();

       // vallidate to not access dahsboard with out login
         
        $this->load->model('customer_model');
        $this->load->model('vendor_model');
		$this->load->model('dashboard_model');
		$this->load->model('product_model');
}

	public function index()
	{
		
		 $data = array(
            'tableData' =>$this->vendor_model->show_category(),
			'banner' =>$this->dashboard_model->get_all(),
			'cat_slider' =>$this->dashboard_model->get_cat(),
        );

		$this->load->view('home' ,$data);
		$this->load->view('includes/footer');
	}
      
	public function sign_up()
{  

	if($this->session->userdata('cid'))
	{
	
		redirect('dashboard');
	}
	$data = array(
		'category' =>$this->vendor_model->show_category(),
		'products' =>$this->product_model->get_allpro(),
	);
	$this->load->view('includes/header',$data);
   
	$this->load->view('register');

	$this->load->view('includes/footer');
}

public function sign_in()
{  
	if($this->session->userdata('cid'))
	{
	
		redirect('dashboard');
	}

	$data = array(
		'category' =>$this->vendor_model->show_category(),
		'products' =>$this->product_model->get_allpro(),
	);
	$this->load->view('includes/header',$data);
	   $this->load->view('login');
	     $this->load->view('includes/footer');
}

public function otp()
{  
	$data = array(
		'category' =>$this->vendor_model->show_category(),
		'products' =>$this->product_model->get_allpro(),
	);
	$this->load->view('includes/header',$data);
	
	   $this->load->view('otp');
	     $this->load->view('includes/footer');
}

public function otpcheck()
{  

	$this->load->library('form_validation');

	$this->form_validation->set_rules("otp","Otp","required");
	if ($this->form_validation->run() == false )
	{
	   $this->otp();
   }else
   {
	$cid =$this->input->post('cid');
	$email = $this->input->post('email');
	$otp = $this->input->post('otp');

 $otpcheck = $this->customer_model->otp_check($cid,$email,$otp);
if($otpcheck == false) 
{
	$data = $this->session->set_flashdata('error',"Invalid Otp");
	redirect('otp',$data);
}else{
	$updateverify = array(
		'otp_verify' => 1,
	);
	$otpcheck = $this->customer_model->otp_update($cid,$email,$updateverify);
	$data = $this->session->set_flashdata('success',"Otp Veryfied Successfully");
	redirect('signin',$data);
}

   }
}

public function insert_user()
{  


		 $this->load->library('form_validation');

	   $this->form_validation->set_rules("fname","first name","required");
	   $this->form_validation->set_rules("lname","last name","required");
	   $this->form_validation->set_rules("phone","phone","required");
	   $this->form_validation->set_rules("email","email","required|valid_email");
	   $this->form_validation->set_rules("country","country","required");

	   $this->form_validation->set_rules("password","password","required");
	   $this->form_validation->set_rules("c_password","confirm password","required");
	   $this->form_validation->set_rules("city","city","required");
	   $this->form_validation->set_rules("state","state","required");
	   $this->form_validation->set_rules("postcode","postcode","required");
	   $this->form_validation->set_rules("address","address","required");

   if ($this->form_validation->run() == false )
	{ 
		$this->session->set_flashdata('error', validation_errors());
             redirect('signup');
		
   }else
   {
	$random  = substr(rand(), 0, 5);
	 $users = array(
	'fname' => $this->input->post('fname'),
	'lname' => $this->input->post('lname'),
	'email' => $this->input->post('email'),
	'password' => $this->input->post('password'),
	'otp' => $random,
	 );

	 $customerid = $this->customer_model->customer_insert($users);

	 if($customerid == false)
	 {
		$data = $this->session->set_flashdata('error',"Database error");
		redirect('signup',$data );
	 }else{
		$add = array(
			'user_id' => $customerid,
			'fullname' => $this->input->post('fname') . $this->input->post('lname'),
			'phone' => $this->input->post('phone'),
			'country' => $this->input->post('country'),
			'city' => $this->input->post('city'),
			'state' => $this->input->post('state'),
			'postcode' => $this->input->post('postcode'),
			'address' => $this->input->post('address'),
			
			 );
			 $addressid = $this->product_model->add_address($add);
		if($addressid)
		{
			$sessionData = array(
				'cid' => $customerid ,
				'email' => $this->input->post('email'),
		
			);
			$this->session->set_userdata($sessionData);
			$data = $this->session->set_flashdata('success',"OTP has been sent to your email");
			redirect('otp',$data);
			
		}else{
			$data = $this->session->set_flashdata('error',"something went wrong");
		redirect('signup',$data );
		}
		
	 }
}

}


public function customer_check()
{
	$cookie_id  = get_cookie('cookie_id'); 
	$this->load->library('form_validation');
	$this->form_validation->set_rules("email","email","required|valid_email");

	$this->form_validation->set_rules("password","password","required");
	if ($this->form_validation->run() == FALSE)
	{
		$this->session->set_flashdata('email','email field required');
		$this->session->set_flashdata('password', 'password field required');
             redirect('signin'); 
	
	}
	else
	{ 
		$email = trim($this->input->post('email'));
		$password = trim($this->input->post('password'));

		$result = $this->customer_model->customer_login($email, $password);

		if ($result == false) {

			$data = $this->session->set_flashdata('error',"Wrong Credentials");
			redirect('signin',$data );

		} else {
			$status = $this->customer_model->customer_statuscheck($email, $password);
		
			$otp = $this->customer_model->api_verifylogin($email, $password);
		if($otp==false){
			$data = $this->session->set_flashdata('error',"Otp Not Verified");
			redirect('signin',$data );
		}
			if($status ==false){
				$data = $this->session->set_flashdata('error',"Please Contact Admin To Activate your account");
			redirect('signin',$data );
			}else{


				$sessionData = array(
					'cid' => $status[0]->id,
					'fname' => $status[0]->fname,
					'lname' => $status[0]->lname,
				);
			
				//user session set
				$this->session->set_userdata($sessionData);
				$data = $this->session->set_flashdata('success',"Signin Successfully");
				if($cookie_id==0){
					redirect('dashboard',$data);
				}else{
					redirect('checkout',$data);
				}
			    


			}
  
	    }
    }

}
	public function signout()
	{
		$this->session->sess_destroy();
		delete_cookie('buynow'); 
		redirect('/');
				
	}
     public function cantact()
	{
		$data = array(
            'category' =>$this->vendor_model->show_category(),
			'products' =>$this->product_model->get_allpro(),
        );
		$this->load->view('includes/header',$data);
		$this->load->view('cantact');
		$this->load->view('includes/footer');
	}

    public function cantact_info()
	{  

            $this->load->library('form_validation');
           $this->form_validation->set_rules("name","your name","required");
           $this->form_validation->set_rules("email","your email","required|valid_email");
           $this->form_validation->set_rules("message","message","required");
           

           

       if ($this->form_validation->run() == false )
        {
       	$this->cantact();
       }else
       {
         $con = array(
        'name' => $this->input->post('name'),
		'email' => $this->input->post('email'),
		'message' => $this->input->post('message'),
		
		


         );
           

     $usersinserted=$this->user_model->contact_insert($con);

     if ($usersinserted==false) 
     {	
     	 

     }else
	{      echo "<script>
			alert('Entered Successfully');
			window.location.href='home';
			</script>";

			}


       }

	}

public function shop()
	{ 

		 $data = array(
            'category' =>$this->vendor_model->show_category(),
			'products' =>$this->product_model->get_allpro(),
        );
            $this->load->view('includes/header',$data);
       
		$this->load->view('shop');
 
		$this->load->view('includes/footer');
	}

	public function blog()
	{ 

		 $data = array(
            'category' =>$this->vendor_model->show_category(),
			'products' =>$this->product_model->get_allpro(),
        );
            $this->load->view('includes/header',$data);
       
		$this->load->view('blog');
 
		$this->load->view('includes/footer');
	}
	
	public function get_category($id)
	{ 
		
		 $data = array(
			'category' =>$this->vendor_model->show_category(),
			'products' =>$this->product_model->get_catpro($id),
        );
            $this->load->view('includes/header',$data);
       
		$this->load->view('category');
 
		$this->load->view('includes/footer');
	}
	public function get_subcategory($id)
	{ 
		
		 $data = array(
			'category' =>$this->vendor_model->show_category(),
			'subcategory' =>$this->vendor_model->show_subcategoryid($id),
			'products' =>$this->product_model->get_catpro($id),
        );
            $this->load->view('includes/header',$data);
       
		$this->load->view('sub_category');
 
		$this->load->view('includes/footer');
	}

	public function product_details($id)
	{ 
	
		$catid = $this->product_model->get_cat_id($id);
		if($catid==false)
		{
			redirect('/');
		}
		$skuid = $this->product_model->sku_id($id);

		 $data = array(
			'category' =>$this->vendor_model->show_category(),
			'product' =>$this->product_model->get_pro($id),
			'skuimages' =>$this->product_model->sku_images($skuid[0]->sku_id),
			'relatedpro' =>$this->product_model->get_related_pro($id,$catid[0]->cat_id),
        );
            $this->load->view('includes/header',$data);
			$this->load->view('product_details');
			$this->load->view('includes/footer');
	}

	
	public function search()
	{ 
	
		
		$seacrh = $this->input->post('name');

     
		$data = array(
            'category' =>$this->vendor_model->show_category(),
			'search' =>$this->vendor_model->search_result($seacrh),
			
        );
		
            $this->load->view('includes/header',$data);
       
		$this->load->view('search');
 
		$this->load->view('includes/footer');
	}





}
