<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	

 function __construct()
    {
        parent::__construct();

       // vallidate to not access dahsboard with out login
         if(!$this->session->userdata('aid'))
        {
            $this->session->sess_destroy();
            redirect('admin');
        }
        $this->load->library('session');
		$this->load->model('dashboard_model');
		$this->load->model('product_model');
    }


	public function index()
	{

		$this->load->view('admin/includes/header');
		$this->load->view('admin/dashboard');
		$this->load->view('admin/includes/footer');
	}
    public function banner()
	{
	
		$data = array(
            'tableData' =>$this->dashboard_model->get_all(),
            
        );
		$this->load->view('admin/includes/header');
		$this->load->view('admin/banner',$data);
		$this->load->view('admin/includes/footer');
	}
    public function add_banner()
	{
		
		$data = array(
            'category' =>$this->product_model->select_cat(),
         
        );
		$this->load->view('admin/includes/header');
		$this->load->view('admin/add_bannerimg',$data);
		$this->load->view('admin/includes/footer');
	}

	public function insert_image()
	{

		$imgUrl = '';
	
		$config['upload_path'] = '././assets/admin/banner_images/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['encrypt_name'] = TRUE;
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('image')) {
			
		   $data = $this->session->set_flashdata('error',"Unable to Upload");
		 redirect('admin/add_bannerimg',$data);
		} else {
			$img = array('upload_data' => $this->upload->data());
			//echo $img['upload_data']['full_path'];
			$imgUrl = $img['upload_data']['file_name'];
		}

		$id=trim($this->input->post('id'));

if ($id=="new") {

	$img = array(
		
		   'banner_image' => $imgUrl,
		   'catid' => $this->input->post('catid'),
		   'title' => $this->input->post('title'),

	  );

		$banner=$this->dashboard_model->banner_insert($img);
		if ($banner==false) {
			
			$data = $this->session->set_flashdata('error',"Unable to Upload");
			redirect('admin/add_bannerimg',$data);
		}else{
			$data = $this->session->set_flashdata('success',"succesfully added");
		
			redirect('admin/banner',$data);

		}

}else{

	$banner_img=$this->dashboard_model->checkimg($id);

	if(!empty($banner_img)){ 

		@unlink($config['upload_path'] .$banner_img['banner_image']);  

	}
	

	$img = array(
		
		   'banner_image' => $imgUrl,
		   'catid' => $this->input->post('catid'),
		   'title' => $this->input->post('title'),

	  );

		$banner=$this->dashboard_model->update_img($id,$img);
		if ($banner==false) {
			
			$data = $this->session->set_flashdata('error',"Unable to Upload");
			redirect('admin/add_bannerimg',$data);
		}else{
			$data = $this->session->set_flashdata('success',"succesfully Updated");
		
			redirect('admin/banner',$data);

		}

}

	}

	public function edit_ban($id)
	{
	


		$banner=$this->dashboard_model->get_banner($id);
	
		if ($banner==false) {
			$data = $this->session->set_flashdata('error',"Unable to get data");
			redirect('admin/add_bannerimg',$data);
			}else{
				
				$banners= array(
					'category' =>$this->product_model->select_cat(),
					'id' => $banner[0]['id'],
					'banner_image' => $banner[0]['banner_image'],
					'catid' => $banner[0]['catid'],
					'title' => $banner[0]['title'],
				);
			}
		$this->load->view('admin/includes/header');
		$this->load->view('admin/add_bannerimg',$banners);
		$this->load->view('admin/includes/footer');
	}
	
	public function delete_ban($id)
	{

		if ($id !=false) {

			$img=$this->dashboard_model->delete_imgs($id);
	 
			if ($img==true) {

		$config['upload_path'] = '././assets/admin/banner_images/';

		$banner_img=$this->dashboard_model->checkimg($id);

	if(!empty($banner_img)){ 

		@unlink($config['upload_path'] .$banner_img['banner_image']);  

	}

			   redirect('admin/banner');
			}
			}


	}

    
	public function blog()
	{
	
		$this->load->view('admin/includes/header');
		$this->load->view('admin/blog');
		$this->load->view('admin/includes/footer');
	}

	public function add_blog()
	{
		
		$this->load->view('admin/includes/header');
		$this->load->view('admin/add_blog');
		$this->load->view('admin/includes/footer');
	}

	public function employee()
	{
		
		$this->load->view('admin/includes/header');
		$this->load->view('admin/employee');
		$this->load->view('admin/includes/footer');
	}


}