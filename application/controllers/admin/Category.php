<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	

 function __construct()
    {
        parent::__construct();

       // vallidate to not access dahsboard with out login
         if(!$this->session->userdata('aid'))
        {
            $this->session->sess_destroy();
            redirect('admin');
        }
        $this->load->model('product_model');
    }




public function category()
	{
        $subcats='';
        $categorys= $this->product_model->get_category();
        if($categorys){
        foreach($categorys as $catid)
        {
           
            $subcats= $this->product_model->get_subcategory($catid->id);
           
        }
    }

            $data = array(
            'tableData' =>$this->product_model->get_category(),
            
        );
        
		$this->load->view('admin/includes/header');
		$this->load->view('admin/category',$data);
		$this->load->view('admin/includes/footer');
	}

public function add_category()
	{

$data = array(
            'tableData' =>$this->product_model->select_all(),
            
        );

		$this->load->view('admin/includes/header');
		$this->load->view('admin/add_category',$data);
		$this->load->view('admin/includes/footer');
	}


public function insert_category()
	{

        $imgUrl = '';
	
		$config['upload_path'] = '././assets/admin/category_images/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['encrypt_name'] = TRUE;
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('image')) {
			
		   $data = $this->session->set_flashdata('error',"Unable to Upload");
		 redirect('admin/add_category',$data);
		} else {
			$img = array('upload_data' => $this->upload->data());
			//echo $img['upload_data']['full_path'];
			$imgUrl = $img['upload_data']['file_name'];
		}

              $cat = array(
                  'ven_id' => trim($this->input->post('venid')),
                   'cat_name' => trim($this->input->post('cname')),
                   'image' => $imgUrl
                );
               

       $categoryid=$this->product_model->category_insert($cat);

       if ($categoryid==false) {
        $data = $this->session->set_flashdata('error',"Unable to Insert Category");
          redirect('admin/add_category',$data);
       }else{
        $subcat=$this->input->post('category');
        $subcategory= explode(",",$subcat);
        foreach($subcategory as $scat)
        {
            $subcat = array(
                'cat_id' => $categoryid,
                 'subcat_name' => $scat,
              );

       $sub_category=$this->product_model->subcategory_insert($subcat);
        }
           
        $data = $this->session->set_flashdata('success',"succesfully added");
          redirect('admin/category',$data);

       }
	}

    public function getsub_category($catid=false)
    {
       
        if($data=$this->product_model->get_subcategory($catid))
		{
			$response = array('status' => 200, 'data' =>$data);
		}
		else
		{
			$response = array('status' => 500 );
		}
		$this->output->set_content_type('application/json');
		echo json_encode($response);

    }
}