<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	

 function __construct()
    {
        parent::__construct();
       
      $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model('login_model');
        $this->load->model('product_model');
    }


	public function index()
	{
		$data = array(
            'orders' =>$this->product_model->get_allorders(),
            
        );
		$this->load->view('admin/includes/header');
    	$this->load->view('admin/orders',$data);
        $this->load->view('admin/includes/footer');
	}

    public function order_products($id)
	{
		$data = array(
            'orders' =>$this->product_model->get_allordersid($id),
            'products' =>$this->product_model->get_ordered_products($id),
            
        );
		$this->load->view('admin/includes/header');
    	$this->load->view('admin/order_products',$data);
        $this->load->view('admin/includes/footer');
	}
    
}

