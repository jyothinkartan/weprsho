<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	

 function __construct()
    {
        parent::__construct();
       
      $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model('login_model');
    }


	public function index()
	{
		
		$this->load->view('admin/login');
		
	}



public function customers()
    {
        
         $data = array(
            'tableData' =>$this->login_model->all_customers(),
            
        );

         $this->load->view('admin/includes/header');
        $this->load->view('admin/customers',$data);
        $this->load->view('admin/includes/footer');
    }

 
        

public function view_customers()
    {
        
         $data = array(
            'tableData' =>$this->login_model->all_customers(),
            
        );

         $this->load->view('admin/includes/header');
        $this->load->view('admin/view_customers',$data);
        $this->load->view('admin/includes/footer');
    }


		public function check()
	{


                if ($this->form_validation->run('admin_login') == FALSE)
                {
                     
                         $this->load->view('admin/login');	
                }
                else
                { 
        $email = trim($this->input->post('email'));
        $password = trim($this->input->post('password'));

        $result = $this->login_model->login($email, $password);


        if ($result == false) {

            $data = $this->session->set_flashdata('error',"Wrong Credentials");
            redirect('admin',$data );

        } else {

         $status = $this->login_model->statuscheck($email, $password);
if($status==false)
{
    $data = $this->session->set_flashdata('error',"Please Contact admin To activate your account");
    redirect('admin',$data );
}else{

    $sessionData = array(
        'aid' => $result[0]->id,
        'name' => $result[0]->name,
        'role' => $result[0]->role,
    );

    //user session set
    $this->session->set_userdata($sessionData);
  redirect('admin/dashboard');

}
        }
                 
                       
                }



}
            public function logout()
                {
$this->session->sess_destroy();
            redirect('admin');

                }


}
