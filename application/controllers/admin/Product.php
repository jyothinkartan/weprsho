<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	

 function __construct()
    {
        parent::__construct();

       // vallidate to not access dahsboard with out login
         if(!$this->session->userdata('aid'))
        {
            $this->session->sess_destroy();
            redirect('admin');
        }
          $this->load->model('product_model');

     
    }

    public function index()
    {
     $role=$this->session->userdata('role');
                    if ($role==0) {
                        $data = array(
                        'tableData' =>$this->product_model->select_products(), 
                    );
                    }else{

            $aid=$this->session->userdata('aid');
           
                        $data = array( 
                        'tableData' =>$this->product_model->select_products($aid),
                        
                    );
                    }

        $this->load->view('admin/includes/header');
        $this->load->view('admin/product',$data);
        $this->load->view('admin/includes/footer');

  }

public function add_product(){

    
    $userid = $this->session->userdata('aid');
    

    $data = array(
            'tableData' =>$this->product_model->select_all(),
            'vendor' =>$this->product_model->select_onevendor($userid),
    //    'catdata' =>$this->product_model->select_cat(), 
       'sku' =>$this->product_model->select_sku(),   
         
        );

        $this->load->view('admin/includes/header');
        $this->load->view('admin/add_product',$data);
        $this->load->view('admin/includes/footer');

}

public function edit_product($id){

    $userid = $this->session->userdata('aid');
$itms="";
$itmdata= $this->product_model->get_pro($id);

           if ($itmdata==true) {
            $itms = array(
                'id' => $itmdata[0]->id,
                'venid' => $itmdata[0]->ven_id,
                'venname' => $itmdata[0]->venname,
                'skuid' => $itmdata[0]->skuid,
                'sku_id' => $itmdata[0]->sku_id,
                'quantity' => $itmdata[0]->quantity,
                     'pro_name' => $itmdata[0]->pro_name,
                     'pro_price' => $itmdata[0]->pro_price,
                     'is_stock' => $itmdata[0]->is_stock,
                     'description' => $itmdata[0]->description,
                        
               );
           }
           $data = array(
            'sku' =>$this->product_model->select_sku(),  
            'vendor' =>$this->product_model->select_onevendor($userid), 
            'tableData' =>$this->product_model->select_all(),
            'catdata' =>$this->product_model->select_cat(), 
            'subcatdata' =>$this->product_model->get_subcategory($itmdata[0]->cat_id),    
            
        );


$this->load->view('admin/includes/header',$itms);
        $this->load->view('admin/add_product',$data);
        $this->load->view('admin/includes/footer');


}


public function delete_sku($id){

    if ($id !=false) {
    
           $sku=$this->product_model->delete_sku($id);
    
           if ($sku==true) {
              redirect('admin/sku');
           }
           }
    }


public function delete_product($id){

if ($id !=false) {

       $ven=$this->product_model->delete_product($id);

       if ($ven==true) {
          redirect('admin/product');
       }
       }
}

public function delete_cat($id){

if ($id !=false) {

       $ven=$this->product_model->delete_cat($id);

       if ($ven==true) {
          redirect('admin/category');
       }
       }
}



public function insert_product(){


$id=trim($this->input->post('id'));



if ($id=="new") {
    
 $product = array(
                  'ven_id' => trim($this->input->post('venid')),
                  'sku_id' => trim($this->input->post('skuid')),
                  'quantity' => trim($this->input->post('quantity')),
                  'pro_price' => trim($this->input->post('product_price')),
                    'is_stock' => trim($this->input->post('stock')),
                   'description' => trim($this->input->post('description')),

                );

       $admin=$this->product_model->product_insert($product);
       if ($admin==false) {
        $data = $this->session->set_flashdata('error',"Unable to insert product details");
        
          redirect('admin/add_product',$data);
       }else{
        $data = $this->session->set_flashdata('success',"succesfully added");
       
          redirect('admin/product',$data);

       }


    }else{
              $id=trim($this->input->post('id'));


          $product = array(
                  'ven_id' => trim($this->input->post('venid')),
                  'sku_id' => trim($this->input->post('skuid')),
                  'quantity' => trim($this->input->post('quantity')),
                   'pro_price' => trim($this->input->post('product_price')),
                    'is_stock' => trim($this->input->post('stock')),
                   'description' => trim($this->input->post('description')),
                );

       $admin=$this->product_model->product_update($id,$product);
       if ($admin==false) {
           
           $data = array('error' => 'Unable to Update product details');
          redirect('admin/add_product/'.$id,$data);
       }else{
         $data = array('message' => 'succesfully added');
          redirect('admin/product',$data);

       }



   }
}


    public function sku(){


      $data = array(
                'tableData' =>$this->product_model->select_allsku(),
           'catdata' =>$this->product_model->select_cat(),  
             
            );
    
            $this->load->view('admin/includes/header');
            $this->load->view('admin/sku',$data);
            $this->load->view('admin/includes/footer');
    
    }

    public function add_sku(){

       
        $data = array(
               
               'catdata' =>$this->product_model->select_cat(),  
                );
        
                $this->load->view('admin/includes/header');
                $this->load->view('admin/add_sku',$data);
                $this->load->view('admin/includes/footer');
        
     }

     public function edit_sku($id){

       
        $itms="";
       $dataimg= $this->product_model->get_sku($id);

           if ($dataimg==true) {
            $itms = array( 
                'id' => $dataimg[0]->id,
                'sku_id' => $dataimg[0]->sku_id,
                'pro_name' => $dataimg[0]->pro_name,
                'catid' => $dataimg[0]->cat_id,
                'cat_name' => $dataimg[0]->cat_name,
                'scatid' => $dataimg[0]->sub_catid,
                'subcat_name' => $dataimg[0]->subcat_name,
                        'image' => $dataimg[0]->pro_image,
             );
           }
           $data = array(
            
            'catdata' =>$this->product_model->select_cat(), 
            'subcatdata' =>$this->product_model->get_subcategory($dataimg[0]->cat_id),    
            
        );

       
                $this->load->view('admin/includes/header',$itms);
                $this->load->view('admin/add_sku',$data);
                $this->load->view('admin/includes/footer');
        
     }



     public function sku_images($id){

       
        $data = array(
               
               'images' =>$this->product_model->sku_images($id),  
                );
        
                $this->load->view('admin/includes/header');
                $this->load->view('admin/skuimages',$data);
                $this->load->view('admin/includes/footer');
        
     }
     

    public function insert_sku(){


   $id=trim($this->input->post('id'));



        if ($id=="new") {

            $product = array(
                'pro_name' => trim($this->input->post('product_name')),
                'cat_id' => trim($this->input->post('catid')),
                'sub_catid' => trim($this->input->post('subcatid')),
                'created_at' =>date("d-m-Y"),
            );

        $sku=$this->product_model->insert_sku($product);

        }else{

            $product = array(
                'pro_name' => trim($this->input->post('product_name')),
                'cat_id' => trim($this->input->post('catid')),
                'sub_catid' => trim($this->input->post('subcatid')),
                'updated_at' =>date("d-m-Y"),
            );

           $sku=$this->product_model->update_sku($id,$product);

        }
            
        if ($id=="new") {
            $skuid = 'sku'.$sku;
        }else{
            $skuid = 'sku'.$id;
        }
        $random  = substr(rand(), 0, 5);
   
     if ($sku!=false) {

              // for single image upload
        if (isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {

            if (!is_dir('././assets/product_images/'.$skuid)) {
                mkdir('././assets/product_images/'.$skuid, 0777, true);
                    }

             $config['upload_path'] = '././assets/product_images/'.$skuid;
            $config['allowed_types'] = 'jpg|png|jpeg';
            // $config['max_size'] = '1024';
            // $config['max_width']  = '1024';
            // $config['max_height']  = '768'; 
            $new_name = $skuid.$random;
            $config['file_name'] = $new_name;
           
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('image')) {
                $data = $this->session->set_flashdata('error',"Unable to update Sku image");
                redirect('admin/add_sku/',$data);
            } else {
            $img = array('upload_data' => $this->upload->data());
            //echo $img['upload_data']['full_path'];
            $imgUrl = $img['upload_data']['file_name'];

            }
            $updatesku = array(
                'sku_id' => $skuid,
                'pro_image' => $imgUrl,
            );
            $skuup=$this->product_model->update_sku($sku,$updatesku);

        }else{
            $updatesku = array(
                'sku_id' => $skuid,
              );
              $skuup=$this->product_model->update_sku($sku,$updatesku);
        }

        // for multiple image upload

      if(!empty($_FILES['files']['name']) && count(array_filter($_FILES['files']['name'])) > 0){ 
        $filesCount = count($_FILES['files']['name']); 
                for($i = 0; $i < $filesCount; $i++){ 
                $_FILES['file']['name']     = $_FILES['files']['name'][$i]; 
                $_FILES['file']['type']     = $_FILES['files']['type'][$i]; 
                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i]; 
                $_FILES['file']['error']     = $_FILES['files']['error'][$i]; 
                $_FILES['file']['size']     = $_FILES['files']['size'][$i]; 
                
                // File upload configuration 
                $uploadPath = '././assets/product_images/'.$skuid;
                $config['upload_path'] = $uploadPath; 
                $config['allowed_types'] = 'jpg|jpeg|png|gif'; 
                //$config['max_size']    = '100'; 
                //$config['max_width'] = '1024'; 
                //$config['max_height'] = '768'; 
                
                // Load and initialize upload library 
                $this->load->library('upload', $config);
                $this->upload->initialize($config); 
                
                // Upload file to server 
                if($this->upload->do_upload('file')){ 
                    // Uploaded file data 
                    $fileData = $this->upload->data(); 
                    if ($id="new") {
                        $sku = $sku;
                    }else{
                        $sku = $id;
                    }
                    $updateskuimg = array(
                        'sku_id' => $sku,
                        'image' => $fileData['file_name'],
                    );
                    $skuup=$this->product_model->multiple_images($updateskuimg);
            
                }else{  
                    $data = $this->session->set_flashdata('error',"File Type Error");
                    redirect('admin/add_sku/',$data); 
                } 
            } 
     
          
        }


      $data = $this->session->set_flashdata('success',"succesfully added");
     
        redirect('admin/sku',$data);

       }else{
        if ($id=="new") {
            $data = $this->session->set_flashdata('error',"Unable to insert Sku details");
      
            redirect('admin/add_sku',$data);
        }else{
            $data = $this->session->set_flashdata('error',"Unable to update Sku details");
      
        redirect('admin/edit_sku/'.$id,$data);
        }
        
     }
             
            
             
    }

        





}