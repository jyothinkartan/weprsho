<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends CI_Controller {

	

 function __construct()
    {
        parent::__construct();

       // vallidate to not access dahsboard with out login
         if(!$this->session->userdata('aid'))
        {
            $this->session->sess_destroy();
            redirect('admin');
        }
        $this->load->model('vendor_model');
        $this->load->library('session');
    }





    public function vendor()
    {
        $role=$this->session->userdata('role');
        if ($role==0) {
             $data = array(
            'tableData' =>$this->vendor_model->select_all(),
           
        );
        }else{
            $aid=$this->session->userdata('aid');


            $data = array(
            'tableData' =>$this->vendor_model->get_venbyid($aid),
            
        );
        }
       

        $this->load->view('admin/includes/header');
        $this->load->view('admin/vendor',$data);
        $this->load->view('admin/includes/footer');
    }



    public function add_vendor()
    {
       
        $this->load->view('admin/includes/header');
        $this->load->view('admin/add_vendor');
        $this->load->view('admin/includes/footer');
    } 

public function view_vendor($id)
    {


         


           $ven= $this->vendor_model->get_ven($id);
           if ($ven==true) {
            $data = array(
                'id' => $ven[0]->id,
                'oname' => $ven[0]->name,
                     'year' => $ven[0]->year,
                     'gst' => $ven[0]->gst,
                     'city' => $ven[0]->city,
                     'state' => $ven[0]->state,
                     'country' => $ven[0]->country,
                      'pincode' => $ven[0]->pincode,
                       'address' => $ven[0]->address,
                        'image' => $ven[0]->image,

);
           }
            $per= $this->vendor_model->get_admin($ven[0]->user_id,);
            
           if ($per==true) {
            $data1 = array(
                'id' => $per[0]->id,
                'name' => $per[0]->name,
                     'email' => $per[0]->email,
                     'mobile' => $per[0]->mobile,
                    

);
           }

    
       $this->load->view('admin/includes/header',$data1);
        $this->load->view('admin/view_vendor',$data);
        $this->load->view('admin/includes/footer');

        }
    public function edit_vendor($id)
    {
           $ven= $this->vendor_model->get_ven($id);
           if ($ven==true) {
            $data = array(
                'id' => $ven[0]->id,
                'oname' => $ven[0]->name,
                     'year' => $ven[0]->year,
                     'gst' => $ven[0]->gst,
                     'city' => $ven[0]->city,
                     'state' => $ven[0]->state,
                     'country' => $ven[0]->country,
                      'pincode' => $ven[0]->pincode,
                       'address' => $ven[0]->address,
                        'image' => $ven[0]->image,

);
           }
            $per= $this->vendor_model->get_admin($ven[0]->user_id,);
            
           if ($per==true) {
            $data1 = array(
                'id' => $per[0]->id,
                'name' => $per[0]->name,
                     'email' => $per[0]->email,
                     'mobile' => $per[0]->mobile,
                    

);
           }

    
       $this->load->view('admin/includes/header',$data1);
        $this->load->view('admin/add_vendor',$data);
        $this->load->view('admin/includes/footer');

        }




          public function delete_vendor($id)
    {
       if ($id !=false) {

       $ven=$this->vendor_model->delete_ven($id);

       if ($ven==true) {
          redirect('admin/vendor');
       }
       }
       

        }


        public function ven_makeactive($id)
        {
   $statusupdate=array(
        'status' => 1,
    );
$status=$this->vendor_model->status_update($id,$statusupdate);

if ($status == true) {
    $get_id=$this->vendor_model->get_adminid($id);
    if($get_id!=false)
    {
$uid=$get_id['user_id'];

$admin=$this->vendor_model->admin_status_update($uid,$statusupdate);

    }
   
redirect ('admin/vendor');
}
        }


 public function ven_makeinactive($id)
        {
            
$statusupdate=array(

        'status' => 0,
    );

$status=$this->vendor_model->status_update($id,$statusupdate);

if ($status == true) {
    $get_id=$this->vendor_model->get_adminid($id);
    if($get_id!=false)
    {
$uid=$get_id['user_id'];

$admin=$this->vendor_model->admin_status_update($uid,$statusupdate);

    }

redirect ('admin/vendor');
}

        }
        


    public function ven_insert()
    {

$id=trim($this->input->post('id'));


if ($id=="new") {
   
 $random  = substr(rand(), 0, 4); // pwd
                $personal = array(
                  'name' => trim($this->input->post('name')),
                   'email' => trim($this->input->post('email')),
                   'mobile' => trim($this->input->post('mobile')),
                    'password' => $random,
                    'role' => 1,
                     'status' => 1,
                );

       $admin=$this->vendor_model->add_admin($personal);


if ($admin==false) {
      $data = array('error' => 'Unable to insert personal details');
          redirect('admin/add_vendor',$data);
}
else{


$imgUrl = '';
        $config['upload_path'] = '././assets/ven_images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('image')) {
           $data = array('error' => 'unable to upload image');
  redirect('admin/add_vendor',$data);
        } else {



            $img = array('upload_data' => $this->upload->data());

            //echo $img['upload_data']['full_path'];
            $imgUrl = $img['upload_data']['file_name'];

        }


            $veninsert = array(
                  'user_id' => $admin,
                   'name' => trim($this->input->post('oname')),
                  'year' => trim($this->input->post('year')),
                  'gst' => trim($this->input->post('gst')),
                  'city' => trim($this->input->post('city')),
                  'state' => trim($this->input->post('state')),
                  'country' => trim($this->input->post('country')),
                   'pincode' => trim($this->input->post('postcode')),
                    'address' => trim($this->input->post('address')),
                    'image' => $imgUrl,
                    'status' => 1,

                );

       $ven=$this->vendor_model->add_ven($veninsert);
                if ($ven==false) {

                     $data = array('error' => 'Unable to insert ven details');
             redirect('admin/add_vendor',$data); 
                }else{

            $data = array('message' => 'Successfully inserted');
             redirect('admin/vendor',$data);

            

                }

}
   
}else{

$id=trim($this->input->post('id'));

if (isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
    
$pathfile=$this->input->post('image');

unlink(base_url().'/assets/ven_images'.$pathfile);


    if(unlink($pathfile)) {
     
}

$imgUrl = '';
        $config['upload_path'] = '././assets/ven_images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('image')) {
           $data = array('error' => 'unable to upload image');
  redirect('admin/add_vendor',$data);
        } else {



            $img = array('upload_data' => $this->upload->data());

            //echo $img['upload_data']['full_path'];
            $imgUrl = $img['upload_data']['file_name'];

        }


            $venupdate= array(
                  
                   'name' => trim($this->input->post('oname')),
                  'year' => trim($this->input->post('year')),
                  'gst' => trim($this->input->post('gst')),
                  'city' => trim($this->input->post('city')),
                  'state' => trim($this->input->post('state')),
                  'country' => trim($this->input->post('country')),
                   'pincode' => trim($this->input->post('postcode')),
                    'address' => trim($this->input->post('address')),
                    'image' => $imgUrl,
                    

                );

       $ven=$this->vendor_model->update_ven($id,$venupdate);
                if ($ven==false) {

                     $data = array('error' => 'Unable to update ven details');
             redirect('admin/add_vendor',$data); 
                }else{

            $data = array('message' => 'Successfully updated');
             redirect('admin/vendor',$data);

            

                }

}else{

            $venupdate = array(
                
                   'name' => trim($this->input->post('oname')),
                  'year' => trim($this->input->post('year')),
                  'gst' => trim($this->input->post('gst')),
                  'city' => trim($this->input->post('city')),
                  'state' => trim($this->input->post('state')),
                  'country' => trim($this->input->post('country')),
                   'pincode' => trim($this->input->post('postcode')),
                    'address' => trim($this->input->post('address')),
    
                );

       $ven=$this->vendor_model->update_ven($id,$venupdate);
                if ($ven==false) {

                     $data = array('error' => 'Unable to updated ven details');
             redirect('admin/add_vendor',$data); 
                }else{

            $data = array('message' => 'Successfully Updated');
             redirect('admin/vendor',$data);

            

                }






}


}

          
  

    }





}