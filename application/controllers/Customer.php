<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */ 
	
	function __construct()
    {
        parent::__construct();

       // vallidate to not access dahsboard with out login
	   if(!$this->session->userdata('cid'))
	   {
		   $this->session->sess_destroy();
		   redirect('/');
	   }
        $this->load->model('customer_model');
        $this->load->model('vendor_model');
		$this->load->model('dashboard_model');
		$this->load->model('login_model');
			$this->load->model('product_model');

	}



	public function dashboard()
	{
	
	$cid = $this->session->userdata('cid');
	$orders = $this->product_model->get_orders($cid);
	$order_products='';
			if($orders)
			{
				$order_products = $this->product_model->get_orders_products($orders[0]->id);
			}

	$data = array(
		'category' =>$this->vendor_model->show_category(),
		'address' =>$this->vendor_model->get_address($cid),
		'customer' =>$this->vendor_model->get_customer($cid),
		'orders' =>$orders,
		'order_products' =>$order_products,
		'products' =>$this->product_model->get_allpro(),
	);
	$this->load->view('includes/header',$data);
	$this->load->view('dashboard');
	  $this->load->view('includes/footer');
 }

public function wishlist()
{
	$cid='';
	if($this->session->userdata('cid'))
	{
		$cid = $this->session->userdata('cid');
	}
	
	$data = array(
		'category' =>$this->vendor_model->show_category(),
		'products' =>$this->product_model->get_allpro(),
		'wishproduct' =>$this->product_model->get_wishlist($cid),
	);
	$this->load->view('includes/header',$data);
	$this->load->view('wishlist');
	  $this->load->view('includes/footer');
	}

	public function add_wishlist()
	{
	$uid = $this->input->post('uid');
	$proid = $this->input->post('proid');
	$checktowish = $this->product_model->check_wishlist($uid,$proid);

	if($checktowish == false)
	{
		$data=array(
			'user_id' =>$uid,
			'product_id' =>$proid,
			'quantity' =>1,
		);

		$addtowish = $this->product_model->add_towishlist($data);
			if($addtowish!=false)
			{
				$response = array('status' => 200, 'data' =>$data);
			}

	}else{
		$response = array('status' => 500, 'data' =>$uid);
	}


		$this->output->set_content_type('application/json');
		echo json_encode($response);
	
	}

	public function wish_updateqty()
	{
	$listid =$this->input->post('listid');
        $qty =$this->input->post('qty');

		$insertcart = $this->product_model->get_wishqty($listid );

		if($qty=="dec"){
			$delqty= $insertcart[0]->quantity;
			if($delqty==1)
			{
				$delete = $this->product_model->wish_prodel($listid);
		if($delete==true)
		{
			$response = array('status' => 200, 'data' =>$delete);
		}
			}
			$data=array(
				'quantity' =>$insertcart[0]->quantity-1,
			);
		}
		if($qty=="inc"){
			$data=array(
				'quantity' =>$insertcart[0]->quantity+1,
			);
		}

		$updateqty = $this->product_model->wish_update($listid,$data);
		if($updateqty!=false)
		{
			$response = array('status' => 200, 'data' =>$updateqty);
		}

		$this->output->set_content_type('application/json');
		echo json_encode($response);
	}


	public function deletecart()
	{
		$cartid =$this->input->post('cartid');
		

			$delete = $this->product_model->cart_prodel($cartid);
			if($delete==true)
			{
				$response = array('status' => 200, 'data' =>$delete);
			}

			$this->output->set_content_type('application/json');
			echo json_encode($response);
	}

	public function wishlist_delete()
	{
		$listid =$this->input->post('listid');
		

			$delete = $this->product_model->wish_prodel($listid);
			if($delete==true)
			{
				$response = array('status' => 200, 'data' =>$delete);
			}

			$this->output->set_content_type('application/json');
			echo json_encode($response);
	}




	public function place_order()
	{
		
		$cid = $this->session->userdata('cid');
		$cookieid  = get_cookie('cookie_id'); 

		if(!$cookieid)
		{
			redirect('dashboard');
		}

		$data = array(
		'product' =>$this->vendor_model->get_orderproducts($cookieid),
		'address' =>$this->vendor_model->get_address($cid),
		'category' =>$this->vendor_model->show_category(),
		'products' =>$this->product_model->get_allpro(),
		);
		$this->load->view('includes/header',$data);
		$this->load->view('placeorder');
		$this->load->view('includes/footer');
	}


	public function edit_address($aid)
	{
		$data = array(
			'address' =>$this->vendor_model->get_oneaddress($aid),
			'category' =>$this->vendor_model->show_category(),
			'products' =>$this->product_model->get_allpro(),
			);

		$this->load->view('includes/header',$data);
		$this->load->view('editaddress');
		$this->load->view('includes/footer');
	}

	public function update_address()
	{
	
		$cid = $this->session->userdata('cid');
		$type = $this->input->post('type');
		$aid =$this->input->post('id');
	$users = array(
		'fullname' => $this->input->post('fname'),
		'phone' => $this->input->post('phone'),	
		'country' => $this->input->post('country'),
		'city' => $this->input->post('city'),
		'state' => $this->input->post('state'),
		'postcode' => $this->input->post('postcode'),
		'landmark' => $this->input->post('landmark'),
		'address' => $this->input->post('address'),
		 );
		 $updateadd = $this->product_model->address_update($aid,$users);
		 if($updateadd)
		 {
			 if($type=="profile")
			 {
				$data = $this->session->set_flashdata('success',"Successfully Updated the address");
				redirect('dashboard',$data );

			 }else{
				$data = $this->session->set_flashdata('success',"Updated the address");
				redirect('placeorder',$data );
			 }

			
			
		 }else{
			if($type=="profile")
			{
				$data = $this->session->set_flashdata('error',"Unable to update the address");
			redirect('dashboard',$data );
			}else{
				$data = $this->session->set_flashdata('error',"Unable to update the address");
			redirect('placeorder',$data );
			}

		 }
		
	}

		public function adrsadd()
		{
			$cid = $this->session->userdata('cid');
			$users = array(
				'user_id' => $cid,
				'fullname' => $this->input->post('fname'),
				'phone' => $this->input->post('phone'),	
				'city' => $this->input->post('city'),
				'state' => $this->input->post('state'),
				'country' => $this->input->post('country'),
				'postcode' => $this->input->post('postcode'),
				'landmark' => $this->input->post('landmark'),
				'address' => $this->input->post('address'),
				);
				$add = $this->product_model->add_address($users);
				if($add==false)
				{
					$response = array('status' => 300, 'data' =>'Unable to insert');
				
				}else{
					$response = array('status' => 200, 'data' =>'Successfully inserted');
				
				}

				$this->output->set_content_type('application/json');
				echo json_encode($response);

		}

// public function customer_address()
// {
	
// 		$cid = $this->session->userdata('cid');
// 		$type = $this->input->post('type');
	
// 	$users = array(
// 		'fname' => $this->input->post('fname'),
// 		'lname' => $this->input->post('lname'),
// 		'phone' => $this->input->post('phone'),	
// 		'country' => $this->input->post('country'),
// 		'city' => $this->input->post('city'),
// 		'state' => $this->input->post('state'),
// 		'postcode' => $this->input->post('postcode'),
// 		'landmark' => $this->input->post('landmark'),
// 		'address' => $this->input->post('address'),
// 		 );
// 		 $updateadd = $this->product_model->address_update($cid ,$users);
// 		 if($updateadd)
// 		 {
// 			 if($type=="profile")
// 			 {
// 				$data = $this->session->set_flashdata('success',"Successfully Updated the address");
// 				redirect('dashboard',$data );

// 			 }else{
// 				$data = $this->session->set_flashdata('success',"Updated the address");
// 				redirect('placeorder',$data );
// 			 }

			
			
// 		 }else{
// 			if($type=="profile")
// 			{
// 				$data = $this->session->set_flashdata('error',"Unable to update the address");
// 			redirect('dashboard',$data );
// 			}else{
// 				$data = $this->session->set_flashdata('error',"Unable to update the address");
// 			redirect('placeorder',$data );
// 			}

// 		 }
		
// }


		public function finish_order()
		{
			$adrsid = $this->input->post('adrsid');
			$cookieid = $this->input->post('cookieid');
			$cid = $this->session->userdata('cid');
		
			$getcartproducts = $this->product_model->get_cartpro($cookieid);
			if($getcartproducts==false)
			{
				$response = array('status' => 400, 'data' =>'Products not found');
			}else{

				$random  = substr(rand(), 0, 5);
				$orderid= 'WPS'.$random;
	
				$orderdata = array(
					'order_id' => $orderid,
					'addres_id' =>$adrsid,
					'user_id' => $cid,
					'payment_type' =>"cod",
					'date'      => date('Y-m-d H:i:s'),
					'status' => 1,
				);
	
					 $cartproducts = $this->product_model->insert_order($orderdata);
			     if($cartproducts)
			       {
				$getproducts = $this->product_model->get_cartpro($cookieid);
				$total_prices=0;
				foreach($getproducts as $value)
				{
					$data = array(
						'order_id' => $cartproducts,
						'product_id' => $value->product_id,
						'quantity' => $value->quantity,
						'price' =>$value->pro_price,
						'total_price' =>$value->quantity*$value->pro_price,
						'status' =>1
						);

						$insertpro = $this->product_model->insert_orderpro($data);

						$total_prices +=$value->quantity*$value->pro_price;
				}
				$totalprice = array(
					'total_price' => $total_prices,
					);

					$updateprice = $this->product_model->update_totalprice($cartproducts,$totalprice);
			
						if ($updateprice == true) {
						
									$emptycart = $this->product_model->empty_cart($cookieid);
									delete_cookie('cookie_id'); 

									$sessionData = array(
										'orderid' => $orderid,
										'ordertid' => $cartproducts,
									);
									$this->session->set_userdata($sessionData);
						
									$response = array('status' => 200, 'data' =>'true');
								
						 }else{
								$response = array('status' => 300, 'data' =>'Unable to place order Some products seems out of stock');
								
							}

			}

		}
		$this->output->set_content_type('application/json');
		echo json_encode($response);
	
		}
		


	public function delete_address()
	{
	$adrsid =$this->input->post('adrsid');
      

		$delete = $this->product_model->delete_adrs($adrsid);
		if($delete==true)
		{
			$response = array('status' => 200, 'data' =>$delete);
		}

		$this->output->set_content_type('application/json');
		echo json_encode($response);
		}


		public function success()
     {
		$orderid = $this->session->userdata('orderid');
		$ordertid = $this->session->userdata('ordertid');
			
		$this->load->view('includes/header');
		$this->load->view('success');
	  $this->load->view('includes/footer');
     }

	 public function track()
     {
		$orderid = $this->session->userdata('orderid');
		$ordertid = $this->session->userdata('ordertid');
			
		$this->load->view('includes/header');
		$this->load->view('track');
	  $this->load->view('includes/footer');
     }
		
}

