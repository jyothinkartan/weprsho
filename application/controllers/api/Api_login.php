<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_login extends CI_Controller {


 function __construct()
    {
        parent::__construct();

    
        $this->load->library('form_validation');
         $this->load->model('customer_model');
       
    }



public function index()
    {

        $REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
        switch ($REQUEST_METHOD) {
            case 'POST':


                 $email = $this->input->post('email');
               $password = $this->input->post('password');



    if (!empty($email) && !empty($password)) {
                   
               

              $customers=$this->customer_model->api_clogin($email,$password); 

   if ($customers!=false) {

       $verify=$this->customer_model->api_verifylogin($email,$password); 

                        if ($verify!=false) {

                                        $userid=$verify['id'];

          $token = $this->create_token($userid);


                            $response = array('message' => 'success','result' => $token);
                                            $this->response_data($response);
                           
                        }else{
                             $response = array('message' => 'error','result' => 'Otp not verified');
                        $this->response_error('error',$response);

                            }
                  
        }else{

                              $response = array('message' => 'error','result' => 'Email or Password does not match');
                        $this->response_error('error',$response);
         
        }


}else{
    $response = array('message' => 'error','result' => 'please fill all details');
                $this->response_error('error',$response);
}





                break;

            default:
                # invalid method
                $response = array('result' => 'Method Not Allowed');
                $this->response_error('405 Method Not Allowed', $response);
                break;
        }


    }


public function signup()
    {

        $REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
        switch ($REQUEST_METHOD) {
            case 'POST':

            $name = $this->input->post('name');
                $mobile = $this->input->post('mobile');
                $email = $this->input->post('email');
               $password = $this->input->post('password');


if (!empty($name) && !empty($mobile) && !empty($email) && !empty($password)) {
 

 $random  = substr(rand(), 0, 5);

$data = array(

        'fname' => $name,
        'Phone' => $mobile,
        'email' => $email,
        'password' => $password,
        'otp' => $random,

                    );



     $customers=$this->customer_model->api_customer($data); 


            if ($customers) {


                 


            $response = array('message' => 'success','otp'=>$random,'result' => 'registered successfully');
                           
            $this->response_data($response);
              
            }else
            {
                $response = array('message' => 'error','result' => 'unable to register');
                            $this->response_error('error',$response);
            }
                   


}
else{
      $response = array('message' => 'error','result' => 'please fill all details');
                $this->response_error('error',$response);
}


                break;

            default:
                # invalid method
                $response = array('message' => 'error','result' => 'Method Not Allowed');
                $this->response_error('405 Method Not Allowed', $response);
                break;
        }


    }



public function resend_otp()
    {

        $REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
        switch ($REQUEST_METHOD) {
            case 'POST':

           
                $email = $this->input->post('email');
              


if (!empty($email)) {
 

     $get_email=$this->customer_model->get_email($email); 

              
                        if ($get_email!=false) {
                                  $id= $get_email['id'];

            $random  = substr(rand(), 0, 5);

            $data = array(

                    'otp' => $random ,
                    
                                );
 
            $get_otp=$this->customer_model->update_otpverify($id,$data);



            $response = array('message' => 'success','otp'=>$random,'result' => 'Otp Resend Successfully');
                           
            $this->response_data($response);
              
            }else
            {
                $response = array('message' => 'error','result' => 'Email Does not match');
                            $this->response_error('error',$response);
            }
                
}
else{
      $response = array('message' => 'error','result' => 'please enter email address');
                $this->response_error('error',$response);
}

                break;

            default:
                # invalid method
                $response = array('message' => 'error','result' => 'Method Not Allowed');
                $this->response_error('405 Method Not Allowed', $response);
                break;
        }


    }




public function otp_verification()
    {

        $REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
        switch ($REQUEST_METHOD) {
            case 'POST':

           
                $email = $this->input->post('email');
               $otp = $this->input->post('otp');


if (!empty($email) && !empty($otp)) {
 

     $get_otp=$this->customer_model->get_otp($email,$otp); 

    

            if ($get_otp!=false) {

      $id= $get_otp['id'];
$data = array(

        'otp_verify' => 1,
        
                    );
 
            $get_otp=$this->customer_model->update_otpverify($id,$data);



            $response = array('message' => 'success','result' => 'Otp Verify successfully');
                           
            $this->response_data($response);
              
            }else
            {
                $response = array('message' => 'error','result' => 'Email or Otp Does not match');
                            $this->response_error('error',$response);
            }
                   


}
else{
      $response = array('message' => 'error','result' => 'please fill all details');
                $this->response_error('error',$response);
}

                break;

            default:
                # invalid method
                $response = array('message' => 'error','result' => 'Method Not Allowed');
                $this->response_error('405 Method Not Allowed', $response);
                break;
        }


    }



public function create_token($id)
    {

$token = $this->customer_model->get_token($id);

if ($token==false) {


            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < 70; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            $array = array('user_id' => $id, 'token' => $randomString);

       $uid= $this->customer_model->create_token($array);

       if ($uid!=false) {


            $tokens = $this->customer_model->get_inserttoken($uid);

               return  $tokens;
     
       }

    

    
}else{
 return $token;
}

    }



 //response functions
    public function response_error($header, $response)
    {
        $hedder = "HTTP/1.1 " . $header;
        header($hedder);
        //header('Access-Control-Allow-Origin: *');
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));

    }

    public function response_data($response)
    {
        $hedder = "HTTP/1.1 200 ok";
        header($hedder);
        //header('Access-Control-Allow-Origin: *');
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }



}