
<div id="frontsuccess">
                            <?php echo $this->session->flashdata('success'); ?>
                        </div>
                            <div id="fronterror"><?php echo $this->session->flashdata('error'); ?>
                                    </div>
  

    <!-- Shoping Cart Section Begin -->
    <section class="shoping-cart spad">
        <div class="container">
        <?php            
                if($cartproduct){
                    $subtotal=0;
                    ?>
            <div class="row">
               
                <div class="col-lg-8">
                    <div class="shoping__cart__table">
                        <table>
                            <thead>
                                <tr>
                                    <th class="shoping__product">Products</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php   
                         
                
                    foreach ($cartproduct as  $value) {
                        ?>
                                <tr>
                                    <td class="shoping__cart__item">
                                        <img src="<?php echo base_url();?>assets/product_images/sku<?php echo  $value->sku_id?>/<?php echo  $value->pro_image?>" alt="" class="cartimg">
                                        <h5><a href="<?php echo base_url('product/'.$value->proid); ?>"><?php echo  $value->pro_name?></a></h5>
                                    </td>
                                    <td class="shoping__cart__price">$
                                    <?php echo  $value->pro_price?>
                                    </td>
                         
                                    <td class="shoping__cart__quantity">
                                        <div class="quantity">
                                  
                                            <button><span class="dec decqtybtn" data-id="<?php echo $value->id?>" >-</span></button>
                                            
                                                <?php echo $value->quantity?>
                                                <button><span class="inc incqtybtn" data-id="<?php echo $value->id?>">+</span></button>
                                                </div>
                                      
                                    </td>
                                       
                                    
                                    <td class="shoping__cart__total">$
                                    <?php 
                                    $total=$value->quantity * $value->pro_price;
                                    echo $total
                                    ?>
                                    </td>
                                    <td class="shoping__cart__item__close">
                                    <button><span class="icon_close" data-id="<?php echo $value->id?>"></span> </button>
                                    </td>
                                </tr>
                               <?php  $subtotal +=$value->quantity * $value->pro_price;
                             }  ?>
                            </tbody>
                        </table>
                      </div>
                      </div>
            
                     <div class="col-lg-4">
                     <div class="shoping__checkout">
                        <h5>Cart Total</h5>
                        <ul>
                            <li>Subtotal <span>$<?php 
                                  echo $subtotal;
                                    ?></span></li>
                            <li>Total <span>$<?php 
                                  echo $subtotal;
                                    ?></span></li>
                        </ul>
                        <?php   $cookie_id  = get_cookie('cookie_id');
                        if(empty($cookie_id)){
                            $cookie_id=0;
                        }?>
                        <a href="#" data-id="<?php echo $cookie_id?>" id="checkout" class="primary-btn">PROCEED TO CHECKOUT</a>
                    </div>
                </div>
            </div>
            <div class="row">
              <h5>Add more products to cart</h5>
                <div class="col-lg-12">
                    <div class="shoping__cart__btns">
                        <a href="<?php echo base_url();?>shop" class="warning-btn">CONTINUE SHOPPING</a>
                       
                    </div>
                </div>
            </div>
            <?php } else{
                ?>

            <div class="row">
              <h5>Your Cart is empty</h5>
                <div class="col-lg-12">
                    <div class="shoping__cart__btns">
                        <a href="<?php echo base_url();?>shop" class="warning-btn">CONTINUE SHOPPING</a>
                       
                    </div>
                </div>
                <!-- <div class="col-lg-6">
                    <div class="shoping__continue">
                        <div class="cart-btn-right">
                         
                            <form action="#">
                                <input type="text" placeholder="Enter your coupon code">
                                <button type="submit" class="site-btn">APPLY COUPON</button>
                            </form>
                        </div>
                    </div>
                </div> -->
                
            </div>
            <?php } ?>
        </div>
    </section>
    <script>

   $('.decqtybtn').click(function() {
    var cartid =$(this).attr("data-id");
    $.ajax({
        url: '<?php echo base_url('Cart/updateqty'); ?>',
        type: 'POST',
        data: {
            'cartid': cartid,
            'qty': 'dec',
        },
        dataType: 'json',
        success: function(data) {
                            //result=JSON.parse(result);
//  console.log(data);
                            if(data.status==200)
                            {
                            
                                window.location = "<?php echo base_url('cart'); ?>";
                            }
        }
    });
});

$('.incqtybtn').click(function() {
    var cartid =$(this).attr("data-id");

    $.ajax({
        url: '<?php echo base_url('Cart/updateqty'); ?>',
        type: 'POST',
        data: {
            'cartid': cartid,
            'qty': 'inc',
        },
        dataType: 'json',
        success: function(data) {
                            //result=JSON.parse(result);
//  console.log(data);
                            if(data.status==200)
                            {
                        
                                window.location = "<?php echo base_url('cart'); ?>";
                            }
        }
    });
});

$('.icon_close').click(function() {
    var cartid =$(this).attr("data-id");
    $.ajax({
        url: '<?php echo base_url('Cart/deletecart'); ?>',
        type: 'POST',
        data: {
            'cartid': cartid,
        },
        dataType: 'json',
        success: function(data) {
                            //result=JSON.parse(result);
//  console.log(data);
                            if(data.status==200)
                            {
                            
                                window.location = "<?php echo base_url('cart'); ?>";
                            }
        }
    });
});

$('#checkout').click(function() {
    var cookieid =$(this).attr("data-id");
    
    $.ajax({
        url: '<?php echo base_url('Cart/checkout'); ?>',
        type: 'POST',
        data: {
            'cookieid': cookieid,
        },
        dataType: 'json',
        success: function(data) {
            console.log(data);
                            if(data.status==200)
                            {
                                window.location = "<?php echo base_url('placeorder'); ?>";
                               
                            }
                            if(data.status==300)
                            {
                                window.location = "<?php echo base_url('signin'); ?>";
                               
                            }
                            if(data.status==400)
                            {
                                window.location = "<?php echo base_url('cart'); ?>";
                               
                            }
        }
    });
});

        </script>