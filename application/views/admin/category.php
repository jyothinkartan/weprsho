<div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                  <?php if($this->session->flashdata('error')){
                      ?>
                  <h5 class="error-msg">
               
                  <?php echo $this->session->flashdata('error'); ?>
          
                   </h5>
                   <?php } 
                   if($this->session->flashdata('success')){?>
                   <h5 class="success-msg">
                
                <?php echo $this->session->flashdata('success'); ?>
                 </h5>
                 <?php } ?>
                    <h4 class="card-title">All Categories</h4>
                    <a href="<?php echo base_url();?>admin/add_category"><button type="button" class="btn btn-primary btn-rounded btn-fw">Add category</button></a>
                    <table id="datatable1" class="table table-bordered">
                      <thead>
                        <tr>
                          
                        
                          <th> Category  </th>
                          <th> Vendor Name</th>
                            <th>SUb Category  </th>
                          <th> Action </th>
                        </tr>
                      </thead>
                      <tbody>
                                   <?php
                if($tableData)
                {

                  
                  foreach ($tableData as $key)
                  {
                   
                    ?>
                        <tr>
                         
                      
                          <td> <img src="<?php echo base_url('assets/admin/category_images/'. $key->image)?>" />&nbsp<?php echo $key->cat_name;?> </td>
                          <td> <?php echo $key->name;?></td>
                          <td> <?php 
                          
                          $this->load->model('product_model');
                          
                          $subcats= $this->product_model->get_subcategory($key->id);
                          if(!empty($subcats)){
                       
                          foreach($subcats as $scat){
                          echo $scat->subcat_name; ?>,<?php  } }?> </td>
                          <td>   
        <a href="<?php echo base_url('admin/delete_cat/'.$key->id); ?>" onClick="return confirm('Are you sure you want to delete?')"><button class="badge badge-danger">Deelet</button></a></td>
                        </tr>
                      <?php } } ?>

                        
                        
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>