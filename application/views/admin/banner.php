<!-- content-wrapper starts -->
          <div class="content-wrapper">
             <!-- Quick Action Toolbar Ends-->
            <div class="row">
              <div class="col-md-12 grid-margin">
                <div class="card">
                <?php if($this->session->flashdata('error')){
                      ?>
                  <h5 class="error-msg">
               
                  <?php echo $this->session->flashdata('error'); ?>
          
                   </h5>
                   <?php } 
                   if($this->session->flashdata('success')){?>
                   <h5 class="success-msg">
                
                <?php echo $this->session->flashdata('success'); ?>
                 </h5>
                 <?php } ?>
                 <div class="card-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="d-sm-flex align-items-baseline report-summary-header">
                          <h5 class="font-weight-semibold">Home Banner</h5>
                           <span class="ml-auto"></span> <a href="<?php echo base_url();?>admin/add_bannerimg"><button type="button" class="btn btn-primary btn-rounded btn-fw">Add Banner Images</button></a>
                        </div>
                      </div>
                    </div>
                   
                    <div class="row report-inner-cards-wrapper">
                    <?php
                if($tableData)
                {

                
                  foreach ($tableData as $key)
                  {
                  
                    ?>
                   <div class="row">
                      <div class="col-md-12 home-banner-settings">
                        <h5>Category : <?php echo $key['cat_name'];?></h5><h4>Title :<?php echo $key['title'];?></h4>
                        <div class="d-sm-flex align-items-baseline report-summary-header">
                        <img src="<?php echo base_url('assets/admin/banner_images/'. $key['banner_image'])?>" class="bannerimg" /> 
                          <div class="action-btns">
                          <a href="<?php echo base_url('admin/edit_ban/'.$key['id']); ?>" ><button class="badge badge-success">Edit</button></a>
                          <a href="<?php echo base_url('admin/delete_ban/'.$key['id']); ?>" onClick="return confirm('Are you sure you want to delete?')"><button class="badge badge-danger">Deelet</button></a>
                          </div>
                        </div>
                      </div>
                      
                    </div>

                    <?php  }  } ?>
                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
         
           
           
          </div>
          <!-- content-wrapper ends -->
          

          