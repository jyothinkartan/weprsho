    <div class="col-12 grid-margin">
                <div class="card">

                  <div class="card-body">
                  <?php if($this->session->flashdata('error')){
                      ?>
                  <h5 class="error-msg">
               
                  <?php echo $this->session->flashdata('error'); ?>
          
                   </h5>
                   <?php } 
                   if($this->session->flashdata('success')){?>
                   <h5 class="success-msg">
                
                <?php echo $this->session->flashdata('success'); ?>
                 </h5>
                 <?php } ?>
                    <div class="form-group row">
                            <label class="col-sm-3 col-form-label">
                              <a href="<?php echo base_url();?>admin/customers"><button type="submit" class="btn btn-warning mb-2">Back</button></a>

                            </label>
                           
                          </div>
                          <h4 class="card-description">   <?php


        if (isset($id)) {
            echo "Edit product Details";
        } else {
            echo "Add New product";
        }     
        ?>
    </h4>
                        <h4 class="card-tistle">Product Info</h4>


                    <form class="form-sample" method="post" action="<?php echo base_url(); ?>admin/insert_product" enctype='multipart/form-data'>
                     <input type="hidden" name="id" value="<?php echo empty($id) ? "new" : $id ?>"/>
                    <div class="row">

                         <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Vendor</label>
                            <div class="col-sm-9">
                              <select class="form-control" name="orgid">
                                           
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">category</label>
                            <div class="col-sm-9">
                              <select class="form-control" name="catid">
              
                              </select>
                              
                            </div>
                          </div>
                        </div>
                                        
                   
                      </div>
                  

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Product name</label>
                            <div class="col-sm-9">
                              <input type="text" name="product_name" class="form-control" value="<?php echo empty($item_name) ? "" : $product_name ?>" />
                            </div>
                          </div>
                        </div>
                        
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Product Price</label>
                            <div class="col-sm-9">
                              <input type="number" name="item_price" class="form-control" value="<?php echo empty($item_price) ? "" : $product_price ?>" />
                            </div>
                          </div>
                        </div>
                         
                      </div>
                      <div class="row">

                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Stock</label>
                            <div class="col-sm-9">
                              <select class="form-control" name="stock">
                   
                                <option value="1" <?php if (isset($id)) if ($is_stock == 1) { ?> selected="selected" <?php } ?>>In stock</option>
                           <option value="0" <?php if (isset($id)) if ($is_stock == 0) { ?> selected="selected" <?php } ?>>Out of stock</option>
                              </select>
                            </div>
                          </div>
                        </div>
                     
                        <div class="col-md-3">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Description</label>
                            
                          </div>
                        </div>

                        <div class="col-md-3">
                          <div class="form-group row">
                            
                            <div class="col-sm-3">
                              <textarea name="description"><?php echo empty($description) ? "" : $description ?></textarea>
                            </div>
                          </div>
                        </div>
                      
                      </div>
                 
                      <p class="card-title">Product Image </p>
                      <div class="row">
                       
                       <?php if (isset($id)) {?> 
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Product Image</label>
                            <div class="col-sm-9">
                              <img src="<?php echo base_url();?>assets/itm_images/<?php echo $image ?>" class="org_img"><input type="hidden" name="image" value="<?php echo base_url();?>assets/ven_images/<?php echo $image ?>">
                              <input type="file" name="image"  />
                            </div>
                          </div>
                        </div>
                      <?php }else{
                        ?>
 <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Product Image</label>
                            <div class="col-sm-9">
                              <input type="file" name="image"  />
                            </div>
                          </div>
                        </div>
 <?php }
                        ?>
                          
                      </div>
                    
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">
                              
                            </label>
                            <div class="col-sm-9">
                             
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">
                              
                       <button type="submit" class="btn btn-primary mb-2">Submit</button>
                            </label>
                            <div class="col-sm-9">
                             
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                    
                  </div>
                </div>
              </div>