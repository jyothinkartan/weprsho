<style>
table {
    display: block;
    overflow-x: auto;
    white-space: nowrap;
}
.stock{
  color:green;
}
.outstock{
  color:red;
}
</style>
<div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body" >
                  <?php if($this->session->flashdata('error')){
                      ?>
                  <h5 class="error-msg">
               
                  <?php echo $this->session->flashdata('error'); ?>
          
                   </h5>
                   <?php } 
                   if($this->session->flashdata('success')){?>
                   <h5 class="success-msg">
                
                <?php echo $this->session->flashdata('success'); ?>
                 </h5>
                 <?php } ?>
                    <h4 class="card-title">All Products</h4>
                    <a href="<?php echo base_url();?>admin/add_product"><button type="button" class="btn btn-primary btn-rounded btn-fw">Add new Product</button></a>
                    <table id="datatable1" class="table table-bordered" >
                      <thead>
                        <tr>
                    
                     
                          <th>  SKU ID</th>
                          <th>  Product</th>
                          <th>Vendor</th>
                          <th>Category</th>
                          <th>Sub Category</th>
                          <th> Product Price </th>
                          <th> Is Stock </th>
                          <th> Description </th>
                          <th> Action </th>
                        </tr>
                      </thead>
                      <tbody>
                             <?php
                            
                if($tableData)
                {

                  
                  foreach ($tableData as $key)
                  {
                    ?>
                        <tr>
                        <td> <?php echo $key->sku_id;?></td>
                          <td> <img src="<?php echo base_url('assets/product_images/' .$key->sku_id .'/'. $key->pro_image)?>" /> &nbsp &nbsp  <?php echo $key->pro_name;?>  </td>
                          <td> <?php echo $key->name;?></td>
                          <td> <?php echo $key->cat_name;?></td>
                          <td> <?php echo $key->subcat_name;?></td>
                          <td> <?php echo $key->pro_price;?> </td>
                          <?php $stock=$key->is_stock;
                          $quantity=$key->quantity;

                        if ($stock==1 && $quantity>0) {?>

                         <td class="stock"> <?php echo "In stock";?> </td>

                        <?php
                        }else{  ?>

                      <td class="outstock"> <?php echo "Out of stock";?> </td>

                        <?php
                         
                        }

                          ?>
                          
                          <td> <?php echo $key->description;?> </td>
                          <td>  <a href="<?php echo base_url('admin/edit_product/'.$key->id); ?>"><button class="badge badge-info">Edit</button></a> | 
        <a href="<?php echo base_url('admin/delete_product/'.$key->id); ?>" onClick="return confirm('Are you sure you want to delete?')"><button class="badge badge-danger">Delete</button></a></td>
                        </tr>
                      <?php } } ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>