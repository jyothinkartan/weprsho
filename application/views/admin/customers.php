<style>
table {
    display: block;
    overflow-x: auto;
    white-space: nowrap;
}
</style>
<div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                  <?php if($this->session->flashdata('error')){
                      ?>
                  <h5 class="error-msg">
               
                  <?php echo $this->session->flashdata('error'); ?>
          
                   </h5>
                   <?php } 
                   if($this->session->flashdata('success')){?>
                   <h5 class="success-msg">
                
                <?php echo $this->session->flashdata('success'); ?>
                 </h5>
                 <?php } ?>
                    <h4 class="card-title">All Users</h4>
                  
                    <table id="datatable1" class="table table-bordered">
                      <thead>
                        <tr>
                          <th>First Name </th>
                            <th>Last Name </th>
                          <th> phone </th>
                          <th> email </th>
                          <th> country </th>
                          <th> city </th>
                          <th> state </th>
                          <th> postcode </th>
                          <th> address </th>
                         
                          <th> Action </th>
                        </tr>
                      </thead>
                      <tbody>
                                   <?php
                if($tableData)
                {

                  
                  foreach ($tableData as $key)
                  {
                    ?>
                        <tr>
                          <td> <?php echo $key->fname;?></td>
                          <td> <?php echo $key->lname;?> </td>
                           <td> <?php echo $key->phone;?> </td>
                            <td> <?php echo $key->email;?> </td>
                             <td> <?php echo $key->country;?> </td>
                              <td> <?php echo $key->city;?> </td>
                               <td> <?php echo $key->state;?> </td>
                                <td> <?php echo $key->postcode;?> </td>
                                 <td> <?php echo $key->address;?> </td>
                          
                          <td>  <a href="<?php echo base_url('admin/view_customers/'.$key->id); ?>"><button class="badge badge-info">View</button></a> | 
        <a href="<?php echo base_url('admin/delete_cus/'.$key->id); ?>" onClick="return confirm('Are you sure you want to delete?')"><button class="badge badge-danger">Delete</button></a></td>
                        </tr>
                      <?php } } ?>

                        
                        
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>