    <div class="col-12 grid-margin">
                <div class="card">

                  <div class="card-body">
                  <?php if($this->session->flashdata('error')){
                      ?>
                  <h5 class="error-msg">
               
                  <?php echo $this->session->flashdata('error'); ?>
          
                   </h5>
                   <?php } 
                   if($this->session->flashdata('success')){?>
                   <h5 class="success-msg">
                
                <?php echo $this->session->flashdata('success'); ?>
                 </h5>
                 <?php } ?>
                    <div class="form-group row">
                            <label class="col-sm-3 col-form-label">
                              <a href="<?php echo base_url();?>admin/product"><button type="submit" class="btn btn-warning mb-2">Back</button></a>

                            </label>
                           
                          </div>
                          <h4 class="card-description">   
                            <?php


        if (isset($id)) {
            echo "Edit product Details";
        } else {
            echo "Add New product";
        }     
        ?>
    </h4>
                        <h4 class="card-tistle">Product Info</h4>


                    <form class="form-sample product-add" method="post" action="<?php echo base_url(); ?>admin/insert_product" enctype='multipart/form-data'>
                     <input type="hidden" name="id" value="<?php echo empty($id) ? "new" : $id ?>"/>
                    <div class="row">

                         <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-12 col-form-label">SKU</label>
                            <div class="col-sm-12">
                              <select class="form-control" name="skuid">
                                            <?php
                          if($sku)
                          {

                            foreach ($sku as $key)
                            {
                              ?>
                                <option value="<?php echo $key->id; ?>" <?php if (isset($id)) if ($key->id == $skuid) { ?> selected="selected" <?php } ?>><?php echo $key->sku_id; ?></option>
                             

                               <?php } }?>
                              </select>
                            </div>
                          </div>
                        </div>
                       
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-12 col-form-label">Vendor</label>
                            <div class="col-sm-12">
                              <select class="form-control" name="venid">
                                            <?php
                                             $role=$this->session->userdata('role');
                                             if ($role==0) {
                          if($tableData)
                          {

                            foreach ($tableData as $key)
                            {
                              ?>
                                <option value="<?php echo $key->id; ?>" <?php if (isset($id)) if ($key->id == $venid) { ?> selected="selected" <?php } ?>><?php echo $key->name; ?></option>
                             

                               <?php } } }else{
                                 if($vendor)
                                 {
                                 ?>
                               <option value="<?php echo $vendor[0]->id; ?>" <?php if (isset($id)) if ($vendor[0]->id == $venid) { ?> selected="selected" <?php } ?>><?php echo $vendor[0]->name; ?></option>
                                <?php } }?>
                              </select>
                            </div>
                          </div>
                        </div>
                   
                      </div>
                  

                      <div class="row">
                       
                        
                        <div class="col-md-4">
                          <div class="form-group row">
                            <label class="col-sm-12 col-form-label">Product Price</label>
                            <div class="col-sm-12">
                              <input type="number" name="product_price" class="form-control" value="<?php echo empty($pro_price) ? "" : $pro_price ?>" />
                            </div>
                          </div>
                        </div>
                         
                      
                     
                      <div class="col-md-4">
                          <div class="form-group row">
                            <label class="col-sm-12 col-form-label">Quantity</label>
                            <div class="col-sm-12">
                            <input type="number" name="quantity" class="form-control" value="<?php echo empty($quantity) ? "" : $quantity ?>" />
                           
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group row">
                            <label class="col-sm-12 col-form-label">Stock</label>
                            <div class="col-sm-12">
                              <select class="form-control" name="stock">
                   
                                <option value="1" <?php if (isset($id)) if ($is_stock == 1) { ?> selected="selected" <?php } ?>>In stock</option>
                           <option value="0" <?php if (isset($id)) if ($is_stock == 0) { ?> selected="selected" <?php } ?>>Out of stock</option>
                              </select>
                            </div>
                          </div>
                        </div>
                     
                        </div>

                        
                      <div class="row">
                      <div class="col-md-12">
                          <div class="form-group row">
                            <label class="col-sm-12 col-form-label">Description</label>
                            <div class="col-md-12">
                              <div class="form-group ">
                                
                                <div class="">
                                  <textarea style="width: 100%;border: 1px solid #e8ecf1;height: 80px;" name="description"><?php echo empty($description) ? "" : $description ?></textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        </div>
                 
                    
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">
                              
                            </label>
                            <div class="col-sm-9">
                             
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">
                              
                       <button type="submit" class="btn btn-primary mb-2">Submit</button>
                            </label>
                            <div class="col-sm-9">
                             
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                    
                  </div>
                </div>
              </div>
              
              <script>
                  $('.category_select').change(function(){
                    var value=$(this).val();
                    
        if(value!='')
        {
           $.ajax({
            url: "<?php echo base_url() ?>admin/getsub_category/"+value,
            type:'GET',
            contentType:'application/json',
            dataType:'json',
            cache:false,
            success: function(result){
             //result=JSON.parse(result);
              console.log(result.status);
              if(result.status==200)
              {
                var option='';
                $(result.data).each(function(){
                  option=option+"<option value='"+this.id+"'>"+this.subcat_name+"</option>";
                });

                $('select#sub_cat').empty().append(option);
              }
              else
              {
                alert('No Item category found for selected Organisation');
                $('select#sub_cat').empty().append("<option value=''>--Select--</option>");
              }

          }});
        }
            else
            {
            alert('Please select Organisation.');
            }
                  });
      
                </script>