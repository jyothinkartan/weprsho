
<div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                  <?php if($this->session->flashdata('error')){
                      ?>
                  <h5 class="error-msg">
               
                  <?php echo $this->session->flashdata('error'); ?>
          
                   </h5>
                   <?php } 
                   if($this->session->flashdata('success')){?>
                   <h5 class="success-msg">
                
                <?php echo $this->session->flashdata('success'); ?>
                 </h5>
                 <?php } ?>
                    <h4 class="card-title">All Vendors</h4>
                    <?php $role = $this->session->userdata('role'); 
                    if ($role==0) {
                    
                    ?>
                   <a href="<?php echo base_url();?>admin/add_vendor"><button type="button" class="btn btn-primary btn-rounded btn-fw">Add New Vendor</button></a>
                 <?php } ?>
                    <table id="datatable1" class="table table-bordered">
                      <thead>
                        <tr>
                        
                          <th> Vendor </th>
                        
                          <th> Started Year </th>
                          <th> City </th>
                          <th> Address </th>
                           <th> Status </th>
                             <th> Action </th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php
                if($tableData)
                {

                  foreach ($tableData as $key)
                  {
                    ?>

                        <tr>
                         
                <td> <img src="<?php echo base_url('assets/ven_images/'. $key->image)?>" />&nbsp&nbsp&nbsp<?php echo $key->name; ?> </td>
                       
                          <td> <?php echo $key->year; ?> </td>
                          <td> <?php echo $key->city; ?> </td>
                          <td> <?php echo $key->address; ?> </td>
                          
                                            <td><?php  if ($key->status==1){
                                              ?>
                      <a href="<?php echo base_url('admin/makeinactive/'.$key->id); ?>"> <button class="badge badge-success"  onClick="return confirm('Are you sure you want to inactive?')">Active</button></a>
                                              <?php
                                            }else{
                                              ?>
                      <a href="<?php echo base_url('admin/makeactive/'.$key->id); ?>"> <button class="badge badge-danger"   onClick="return confirm('Are you sure you want to active?')">InActive</button> </a>
                                              <?php
                                            } ?>
                                              
                                          </td>
                          <td> 
                           <a href="<?php echo base_url('admin/view_vendor/'.$key->id); ?>"><button class="badge badge-primary">View</button></a> 
     <!--    <a href="<?php echo base_url('admin/edit_vendor/'.$key->id); ?>"><button class="badge badge-info">Edit</button></a -->
         <?php $role = $this->session->userdata('role'); 
                    if ($role==0) {
                    
                    ?> | <a href="<?php echo base_url('admin/delete_vendor/'.$key->id); ?>" onClick="return confirm('Are you sure you want to delete?')"><button class="badge badge-danger">Delete</button></a>


                    <?Php } ?>
         
         </td>
                        </tr>
                        <?php 
                         } 
                      }
                      ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <script type="text/javascript">
                function inactive() {
         $.ajax({
        data:{'id':userName,'loginpassword':loginpassword},
        url:'<?php echo base_url(); ?>',
        type:'post',
        success:function(data) {
            alert(data);
        });

                  }
              </script>