
<div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body" >
                  <?php if($this->session->flashdata('error')){
                      ?>
                  <h5 class="error-msg">
               
                  <?php echo $this->session->flashdata('error'); ?>
          
                   </h5>
                   <?php } 
                   if($this->session->flashdata('success')){?>
                   <h5 class="success-msg">
                
                <?php echo $this->session->flashdata('success'); ?>
                 </h5>
                 <?php } ?>
                    <h4 class="card-title">All Orders</h4>
                    <a href="<?php echo base_url();?>admin/add_product"><button type="button" class="btn btn-primary btn-rounded btn-fw">Add new Product</button></a>
                    <table id="datatable1" class="table table-bordered" >
                      <thead>
                        <tr>
                    
                     
                          <th> Customer name</th>   
                          <th>Order Id</th>
                          <th>Payment Type</th>
                          <th>Total Amount</th>
                          <th> Order placed date</th>
                          <th> Status </th>
                          <th> Products </th>
                        </tr>
                      </thead>
                      <tbody>
                             <?php
                if($orders)
                {

                  
                  foreach ($orders as $value)
                  {
                    ?>
                        <tr>
                  
                          <td> <?php echo $value->fname;?> <?php echo $value->lname;?> </td>
                          <td> <?php echo $value->order_id;?></td>
                          <td> <?php echo $value->payment_type;?></td>
                          <td> <?php echo $value->total_price;?></td>
                          <td> <?php echo $value->date;?> </td>
                          <td> <?php echo "Initiated";?> </td>
                          <td> <a href="<?php echo base_url('admin/order_products/'.$value->id); ?>"><button class="badge badge-info">View Products</button></a> </td>
                        </tr>
                      <?php } } ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>