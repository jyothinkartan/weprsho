   <div class="col-12 grid-margin">
                <div class="card">
          
                  <div class="card-body">
                    <?php if($this->session->flashdata('error')){
                      ?>
                  <h5 class="error-msg">
               
                  <?php echo $this->session->flashdata('error'); ?>
          
                   </h5>
                   <?php } 
                   if($this->session->flashdata('success')){?>
                   <h5 class="success-msg">
                
                <?php echo $this->session->flashdata('success'); ?>
                 </h5>
                 <?php } ?>
                    <div class="form-group row">
                            <label class="col-sm-3 col-form-label">
                              <a href="<?php echo base_url();?>admin/banner"><button type="submit" class="btn btn-warning mb-2">Back</button></a>
                            </label>
                          </div>
                          <h4 class="card-description">   <?php
        if (isset($id)) {
            echo "Choose To Edit Banner Image";
        } else {
            echo "Choose To Add New Banner image";
        }     
        ?>
    </h4>
                    <form class="form-sample" method="post" action="<?php echo base_url(); ?>admin/insert_img" enctype='multipart/form-data'>
                     <input type="hidden" name="id" value="<?php echo empty($id) ? "new" : $id ?>"/>
                    
                     <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Category</label>
                            <div class="col-sm-9">
                            <select class="form-control" name="catid">
                                            <?php
                if($category)
                {

               
                  foreach ($category as $key)
                  {
                    ?>
                                <option value="<?php echo $key->id; ?>" <?php if (isset($id)) if ($key->id == $catid) { ?> selected="selected" <?php } ?>><?php echo $key->cat_name; ?></option>
                             

                               <?php } }?>
                              </select>
                              </div>
                          </div>
                        </div>
                        
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Title</label>
                            <div class="col-sm-9">
                            <input type="text" name="title" placeholder="Please enter title">
                           </div>
                          </div>
                        </div>
                         
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Image</label>
                            <div class="col-sm-9">
                            <input type="file" id="imgInp" name="image" />
                            <?php if (isset($id)) {?>
                              <img id="bimg" src="<?php echo base_url('assets/admin/banner_images/'. $banner_image)?>" class="bannerimg" /> 
                            <?php }?>
                            <img id="blah" src="#" alt="your image" style="height:200px;width:400px;"/>
                          
                          </div>
                          </div>
                        </div>
                        
                      
                         
                      </div>




                        <div class="row">
                        <div class="col-md-6">
                        <?php
        if (isset($id)) {
            ?>
<button type="submit" class="btn btn-primary mb-2">Update</button>
            <?php
        } else {?>
<button type="submit" class="btn btn-primary mb-2">Add</button>
        <?php
            
        }     
        ?>
                        
                        </div>
                       
                      </div>
                    </form>
                    
                  </div>
                </div>
              </div>

            
  <script>
    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
            const element = document.getElementById("bimg");
element.remove();
            
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function(){
    readURL(this);
});
    </script>