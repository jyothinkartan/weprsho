    <div class="col-12 grid-margin">
                <div class="card">

                  <div class="card-body">
                  <?php if($this->session->flashdata('error')){
                      ?>
                  <h5 class="error-msg">
               
                  <?php echo $this->session->flashdata('error'); ?>
          
                   </h5>
                   <?php } 
                   if($this->session->flashdata('success')){?>
                   <h5 class="success-msg">
                
                <?php echo $this->session->flashdata('success'); ?>
                 </h5>
                 <?php } ?>
                    <div class="form-group row">
                            <label class="col-sm-3 col-form-label">
                              <a href="<?php echo base_url();?>admin/sku"><button type="submit" class="btn btn-warning mb-2">Back</button></a>

                            </label>
                           
                          </div>
                          <h4 class="card-description">  
                             <?php


if (isset($id)) {
    echo "Edit Sku Details";
} else {
    echo "Add New Sku";
}     
?>
    </h4>
                        <h4 class="card-tistle">sku Info</h4>


                    <form class="form-sample product-add" method="post" action="<?php echo base_url(); ?>admin/insert_sku" enctype='multipart/form-data'>
                     <input type="hidden" name="id" value="<?php echo empty($id) ? "new" : $id ?>"/>
                
                      <div class="row">
                      <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-12 col-form-label">category</label>
                            <div class="col-sm-12">
                              <select class="form-control category_select" name="catid">
                              <option value=''>--Select--</option>
                                            <?php
                              if($catdata)
                              {

                                foreach ($catdata as $key)
                                {
                                  ?>
                                <option value="<?php echo $key->id; ?>"<?php if (isset($id)) if ($key->id == $catid) { ?> selected="selected" <?php } ?> ><?php echo $key->cat_name; ?></option>
                             

                               <?php } }?>
                              </select>
                              
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-12 col-form-label">Sub-category</label>
                            <div class="col-sm-12">
                              <select class="form-control sub_cat" name="subcatid" id="sub_cat">
                              <option value=''>--Select--</option>
                              <?php
                                $scatid=empty($scatid)?'':$scatid;
                                foreach ($subcatdata as $key) 
                                {
                                  ?>
                                  <option value="<?php echo $key->id; ?>" <?php if($key->id==$scatid){?>selected="selected" <?php } ?>><?php echo ucfirst($key->subcat_name); ?></option>
                                  <?php
                                }
                                ?>
                              </select>
                              
                            </div>
                          </div>
                        </div> 
                        
                      </div>
                     
                 
                     
                      <div class="row">
                      <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-12 col-form-label">Product name</label>
                            <div class="col-sm-12">
                              <input type="text" name="product_name" class="form-control" value="<?php echo empty($pro_name) ? "" : $pro_name ?>" />
                            </div>
                          </div>
                        </div> 

                       <?php if (isset($id)) {?> 
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Product Image</label>
                            <div class="col-sm-9">
                              <img src="<?php echo base_url();?>assets/product_images/<?php echo $sku_id ?>/<?php echo $image ?>" class="org_img">
                              <input type="file" name="image"  />
                            </div>
                          </div>
                        </div>
                      <?php }else{
                        ?>
                                <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Product Futured Image* </label>
                            <div class="col-sm-9">
                              <input type="file" name="image"  />
                            </div>
                          </div>
                        </div>
             <?php }
                        ?>
                          
                      </div>
                    
                      <div class="row">
                        <?php if (isset($id)) {  }else {?>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">
                             Images (Optional)
                            </label>
                            <div class="col-sm-9">
                            <input type="file" name="files[]"  multiple/>
                            </div>
                          </div>
                        </div>
                        <?php  }?>


                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">
                              
                             <button type="submit" class="btn btn-primary mb-2">Submit</button>
                            </label>
                            <div class="col-sm-9">
                             
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                    
                  </div>
                </div>
              </div>
              
              <script>
                  $('.category_select').change(function(){
                    var value=$(this).val();
                    
        if(value!='')
        {
           $.ajax({
            url: "<?php echo base_url() ?>admin/getsub_category/"+value,
            type:'GET',
            contentType:'application/json',
            dataType:'json',
            cache:false,
            success: function(result){
             //result=JSON.parse(result);
              console.log(result.status);
              if(result.status==200)
              {
                var option='';
                $(result.data).each(function(){
                  option=option+"<option value='"+this.id+"'>"+this.subcat_name+"</option>";
                });

                $('select#sub_cat').empty().append(option);
              }
              else
              {
                alert('No Item category found for selected Organisation');
                $('select#sub_cat').empty().append("<option value=''>--Select--</option>");
              }

          }});
        }
            else
            {
            alert('Please select Organisation.');
            }
                  });
      
                </script>