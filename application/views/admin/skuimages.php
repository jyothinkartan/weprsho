<div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                  <?php if($this->session->flashdata('error')){
                      ?>
                  <h5 class="error-msg">
               
                  <?php echo $this->session->flashdata('error'); ?>
          
                   </h5>
                   <?php } 
                   if($this->session->flashdata('success')){?>
                   <h5 class="success-msg">
                
                <?php echo $this->session->flashdata('success'); ?>
                 </h5>
                 <?php } ?>
                    <h4 class="card-title">Sku Images</h4>
                     <div class="form-group row">
                            <label class="col-sm-3 col-form-label">
                              <a href="<?php echo base_url();?>admin/sku"><button type="submit" class="btn btn-warning mb-2">Back</button></a>

                            </label>
                           
                          </div>

                    <div class="row">

                    <?php
                if($images)
                {

                  
                  foreach ($images as $value)
                  {
                   
                    ?>
                        <div class="col-md-4">
                        <img src="<?php echo base_url('assets/product_images/sku'. $value->sku_id . '/' . $value->image)?>" class="sku-img"/>
                        </div>

                        <?php } }?>
                        
                      </div>

                  </div>
                </div>
              </div>
