
  
​
<!-- Amsify Plugin -->


  

   <div class="col-12 grid-margin">
                <div class="card">

                  <div class="card-body">
                  <?php if($this->session->flashdata('error')){
                      ?>
                  <h5 class="error-msg">
               
                  <?php echo $this->session->flashdata('error'); ?>
          
                   </h5>
                   <?php } 
                   if($this->session->flashdata('success')){?>
                   <h5 class="success-msg">
                
                <?php echo $this->session->flashdata('success'); ?>
                 </h5>
                 <?php } ?>
                    <div class="form-group row">
                            <label class="col-sm-3 col-form-label">
                              <a href="<?php echo base_url();?>admin/blog"><button type="submit" class="btn btn-warning mb-2">Back</button></a>

                            </label>
                           
                          </div>
                          <h4 class="card-description">   <?php
        if (isset($id)) {
            echo "Edit Blog Details";
        } else {
            echo "Add New Blog";
        }     
        ?>
    </h4>


                    <form class="form-sample" method="post" action="<?php echo base_url(); ?>admin/insert_category" enctype='multipart/form-data'>
                     <input type="hidden" name="id" value="<?php echo empty($id) ? "new" : $id ?>"/>
                    

                      
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Vendor</label>
                            <div class="col-sm-9">
                              <select class="form-control" name="venid">
                                            <?php
                if($tableData)
                {

                  foreach ($tableData as $key)
                  {
                    ?>
                                <option value="<?php echo $key->id; ?>" <?php if (isset($id)) if ($key->id == $orgid) { ?> selected="selected" <?php } ?>><?php echo $key->name; ?></option>
                             

                               <?php } }?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Category </label>
                            <div class="col-sm-9">
                              <input type="text" name="cname" class="form-control" value="<?php echo empty($cname) ? "" : $cname ?>" required=""/>
                            </div>
                          </div>
                        </div>
                      </div>
                  
                      
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Sub-Category </label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" name="category"/>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Category Image</label>
                            <div class="col-sm-9">
                            <input type="file" id="imgInp" name="image" />
                            <img id="blah" src="#" alt="your image" style="height:200px;width:400px;"/>
                          
                            </div>
                          </div>
                        </div>
                      </div>
                        <div class="row">
                        <div class="col-md-6">
                          
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">
                              
                       <button type="submit" class="btn btn-primary mb-2">Submit</button>
                            </label>
                            <div class="col-sm-9">
                             
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                    
                  </div>
                </div>
              </div>

              
  
             

  <script>
  
    function readURL(input) {
   
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
            
            
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function(){
    readURL(this);
});
    </script>

  