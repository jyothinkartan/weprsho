
<div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
             <div class="card-body" >
                  <?php if($this->session->flashdata('error')){
                      ?>
                  <h5 class="error-msg">
               
                  <?php echo $this->session->flashdata('error'); ?>
          
                   </h5>
                   <?php } 
                   if($this->session->flashdata('success')){?>
                   <h5 class="success-msg">
                
                <?php echo $this->session->flashdata('success'); ?>
                 </h5>
                 <?php } ?>
                    <a href="<?php echo base_url();?>admin/orders"><button type="button" class="btn btn-warning mb-2">Back</button></a>
                    <h4 class="card-title">Ordered Products Details</h4>
                    <div class="row">

                    <?php  if(!empty($orders))
                    {
                        
                        ?>
              <div class="col-md-12 grid-margin">
                 <div class="card">
                  <div class="card-body">
                    <div class="row income-expense-summary-chart-text">
                      <div class=" col-md-6 col-xl-6">
                     
                      <p class="small text-muted">
                      <h5>Mobile : <?php echo $orders[0]->phone;?></h5><h5>Postcode : <?php echo $orders[0]->postcode;?></h5>
                      <h5>Address :</h5><?php echo $orders[0]->city;?>,<?php echo $orders[0]->state;?>,
                          <?php echo $orders[0]->country;?><?php echo $orders[0]->landmark;?><?php echo $orders[0]->address;?></p>
                    
                      </div>
                      <div class=" col-md-6 col-xl-6">
                      <h5>OrderId : <?php echo $orders[0]->order_id;?></h5>
                        <h4>CustomerName : <?php echo $orders[0]->fname;?><?php echo $orders[0]->lname;?></h4>
                        
                      </div>
                 
                      
         
                  </div>
                </div>

             <?php } ?>
          </div>
        
          <div class="row">

            <div class="col-md-12 grid-margin">
            <div class="card">
            <div class="card-body">

            <table id="datatable1" class="table table-bordered" >
                      <thead>
                        <tr>
                    
                     
                          <th>Ordered By Vendor</th>
                          <th>Product</th>     
                          <th>Quantity</th>
                          <th>Price</th>
                          <th>Total Price</th>
                          <th> Order placed date</th>
                          <th> Status </th>
                         
                        </tr>
                      </thead>
                      <tbody>
                             <?php
                if($products)
                {
                  
                  
                  foreach ($products as $value)
                  {
                    ?>
                        <tr>
                         <td> <?php echo $value->name;?></td>
                          <td> <img src="<?php echo base_url('assets/product_images/'. $value->pro_image)?>" /> &nbsp &nbsp  <?php echo $value->pro_name;?>  </td>
                          <td> <?php echo $value->quantity;?></td>
                          <td> <?php echo $value->price;?></td>
                          <td> <?php echo $value->total_price;?> </td>
                          <td> <?php echo "Initiated";?> </td>
                          <td> <a href=""><button class="badge badge-info">Status Change</button></a> </td>
                        </tr>
                      <?php } } ?>
                        
                      </tbody>
                    </table>
            
            
            
            
            

            

            </div>
            </div>

   
            </div>



         </div>
     </div>
</div>
