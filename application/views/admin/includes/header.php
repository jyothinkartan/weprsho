<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>WePrSho Admin</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendors/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendors/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/vendors/chartist/chartist.min.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- Layout styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/admin/images/favicon.png" />

<link rel="stylesheet" href="//cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css">

  
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/amsify.suggestags.css">

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      
        <div class="navbar-menu-wrapper d-flex align-items-center flex-grow-1">
          <h5 class="mb-0 font-weight-medium d-none d-lg-flex">Welcome WePrSho dashboard!</h5>
          <ul class="navbar-nav navbar-nav-right ml-auto">
            <form class="search-form d-none d-md-block" action="#">
              <i class="icon-magnifier"></i>
              <input type="search" class="form-control" placeholder="Search Here" title="Search here">
            </form>
            <li class="nav-item dropdown">
              <a class="nav-link count-indicator message-dropdown" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                <i class="icon-speech"></i>
                <span class="count">7</span>
              </a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list pb-0" aria-labelledby="messageDropdown">
                <a class="dropdown-item py-3">
                  <p class="mb-0 font-weight-medium float-left">You have 7 unread mails </p>
                  <span class="badge badge-pill badge-primary float-right">View all</span>
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <img src="<?php echo base_url();?>assets/admin/images/faces/face10.jpg" alt="image" class="img-sm profile-pic">
                  </div>
                  <div class="preview-item-content flex-grow py-2">
                    <p class="preview-subject ellipsis font-weight-medium text-dark">Marian Garner </p>
                    <p class="font-weight-light small-text"> The meeting is cancelled </p>
                  </div>
                </a>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <img src="<?php echo base_url();?>assets/admin/images/faces/face12.jpg" alt="image" class="img-sm profile-pic">
                  </div>
                  <div class="preview-item-content flex-grow py-2">
                    <p class="preview-subject ellipsis font-weight-medium text-dark">David Grey </p>
                    <p class="font-weight-light small-text"> The meeting is cancelled </p>
                  </div>
                </a>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <img src="<?php echo base_url();?>assets/admin/images/faces/face1.jpg" alt="image" class="img-sm profile-pic">
                  </div>
                  <div class="preview-item-content flex-grow py-2">
                    <p class="preview-subject ellipsis font-weight-medium text-dark">Travis Jenkins </p>
                    <p class="font-weight-light small-text"> The meeting is cancelled </p>
                  </div>
                </a>
              </div>
            </li>
           
            <li class="nav-item dropdown d-none d-xl-inline-flex user-dropdown">
              <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                <?php  $role = $this->session->userdata('role');?>
                <img class="img-xs rounded-circle ml-2" src="<?php echo base_url();?>assets/admin/images/shotlogo.png" alt="Profile image"> <span class="font-weight-normal">
                      <?php  $role = $this->session->userdata('role');
                      $name = $this->session->userdata('name');
                      if ($role==1) {
                        echo $name;
                      }else{?>

            Weprsho <?php } ?></span></a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                <div class="dropdown-header text-center">
                  <img class="img-md rounded-circle" src="<?php echo base_url();?>assets/admin/images/logo.png" alt="Profile image">
                  <p class="mb-1 mt-3">
                    <?php  $role = $this->session->userdata('role');
                      $name = $this->session->userdata('name');
                      if ($role==1) {
                        echo $name;
                      }else{?>

            Weprsho <?php } ?></p>
               
                </div>
                <a class="dropdown-item"><i class="dropdown-item-icon icon-user text-primary"></i> My Profile <span class="badge badge-pill badge-danger">1</span></a>
                
                <a class="dropdown-item" href="<?php echo base_url(); ?>admin/login/logout"><i class="dropdown-item-icon icon-power text-primary"></i>Sign Out</a>
              </div>
            </li>
          </ul>
          <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="icon-menu"></span>
          </button>
        </div>
      </nav>
      
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-profile">
              <a href="#" class="nav-link">
              <!--   <div class="profile-image">
                  <img class="img-xs rounded-circle" src="<?php echo base_url();?>assets/admin/images/logo.png" alt="profile image">
                  <div class="dot-indicator bg-success"></div>
                </div> -->
                <div class="text-wrapper">
                  <p class="profile-name">WePrSho</p>
                  <p class="designation">Admin</p>
                </div>
               
              </a>
            </li>
           
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url();?>admin/dashboard">
                <span class="menu-title">Dashboard</span>
                <i class="icon-screen-desktop menu-icon"></i>
              </a>
            </li>
            <?php  $role = $this->session->userdata('role');
                   
                   if ($role==0) {
                  
                   ?>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url();?>admin/banner">
                <span class="menu-title">Banner</span>
                <i class="icon-screen-banner menu-icon"></i>
              </a>
            </li>
              <?php }  $role = $this->session->userdata('role');
                   
                      if ($role==0) {
                     
                      ?>
              <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url();?>admin/vendor">
                <span class="menu-title">Vendor</span>
                <i class="icon-book-open menu-icon"></i>
              </a>
            </li>
<?php  } ?>
                    <li class="nav-item"> 
                      <a class="nav-link" href="<?php echo base_url();?>admin/category">  
                        <span class="menu-title">Category</span>
                <i class="icon-bag menu-icon"></i>
              </a>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url();?>admin/sku">
                <span class="menu-title">Sku</span>
                <i class="icon-basket menu-icon"></i>
              </a>
            </li>
       
      <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url();?>admin/product">
                <span class="menu-title">Product</span>
                <i class="icon-bag menu-icon"></i>
              </a>
            </li>
                <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url();?>admin/orders">
                <span class="menu-title">Orders</span>
                <i class="icon-basket menu-icon"></i>
              </a>
            </li>
            
 <?php  $role = $this->session->userdata('role');
                      $name = $this->session->userdata('name');
                      if ($role==0) {
                        ?>

                       <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url();?>admin/customers">
                <span class="menu-title">Customers</span>
                <i class="icon-user menu-icon"></i>
              </a>
            </li>
                     <?php } ?>
                     

             <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-basic1" aria-expanded="false" aria-controls="ui-basic">
                <span class="menu-title">Promotions</span>
                <i class="icon-present menu-icon"></i>
              </a>
              <div class="collapse" id="ui-basic1">
                <ul class="nav flex-column sub-menu">
       <li class="nav-item"> <a class="nav-link" href="<?php echo base_url();?>admin/offer"> Offers</a></li>
       <li class="nav-item"> <a class="nav-link" href="<?php echo base_url();?>admin/coupon"> Coupons</a></li>
                  
                </ul>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url();?>admin/blog">
                <span class="menu-title">Blog</span>
                <i class="icon-layers menu-icon"></i>
              </a>
            </li>
            <?php  $role = $this->session->userdata('role');
                   
                   if ($role!=0) {
                  
                   ?>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url();?>admin/employee">
                <span class="menu-title">Employee</span>
                <i class="icon-user menu-icon"></i>
              </a>
            </li>
            <?php  } ?>
          
          </ul>
        </nav>
        
        <!-- partial -->
        <div class="main-panel">