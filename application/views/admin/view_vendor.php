    <div class="col-12 grid-margin">
                <div class="card">

                  <div class="card-body">
                  <?php if($this->session->flashdata('error')){
                      ?>
                  <h5 class="error-msg">
               
                  <?php echo $this->session->flashdata('error'); ?>
          
                   </h5>
                   <?php } 
                   if($this->session->flashdata('success')){?>
                   <h5 class="success-msg">
                
                <?php echo $this->session->flashdata('success'); ?>
                 </h5>
                 <?php } ?>
                    <div class="form-group row">
                            <label class="col-sm-3 col-form-label">
                              <a href="<?php echo base_url();?>admin/vendor"><button type="submit" class="btn btn-warning mb-2">Back</button></a>

                            </label>
                           
                          </div>
                          <h4 class="card-description">  View Vendor
    </h4>
                        <h4 class="card-tistle"> Personal Info</h4>


                    <form class="form-sample" method="post" action="<?php echo base_url(); ?>admin/ven_insert" enctype='multipart/form-data'>
                     <input type="hidden" name="id" value="<?php echo empty($id) ? "new" : $id ?>"/>
                     <?php if (isset($id)) {?> 
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Name</label>
                            <div class="col-sm-9">
                              <input type="text" name="name" class="form-control" value="<?php echo empty($name) ? "" : $name ?>" readonly=""/>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                              <input type="text" name="email" class="form-control" value="<?php echo empty($email) ? "" : $email ?>" readonly="" />
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Mobile</label>
                            <div class="col-sm-9">
                             <input type="number" name="mobile" class="form-control" value="<?php echo empty($mobile) ? "" : $mobile ?>" readonly="" />
                            </div>
                          </div>
                        </div>
                       
                      </div>
                    <?php }else{ ?>
<div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Name</label>
                            <div class="col-sm-9">
                              <input type="text" name="name" class="form-control"  />
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                              <input type="text" name="email" class="form-control" />
                            </div>
                          </div>
                        </div>
                      </div>
                 
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Mobile</label>
                            <div class="col-sm-9">
                             <input type="number" name="mobile" class="form-control"  />
                            </div>
                          </div>
                        </div>
                       
                      </div>
                         <?php } ?>

                      <p class="card-title"> Vendor Deatils </p>
                      <div class="row">
                         <?php if (isset($id)) {?> 
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Vendor Image</label>
                            <div class="col-sm-9">
                              <img src="<?php echo base_url();?>assets/ven_images/<?php echo $image ?>" class="org_img"><input type="hidden" name="image" value="<?php echo base_url();?>assets/ven_images/<?php echo $image ?>">
                              <input type="file" name="image"  />
                            </div>
                          </div>
                        </div>
                      <?php }else{
                        ?>
 <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Vendor Image</label>
                            <div class="col-sm-9">
                              <input type="file" name="image"  />
                            </div>
                          </div>
                        </div>
 <?php }
                        ?>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Vendor Name</label>
                            <div class="col-sm-9">
                              <input type="text" name="oname" class="form-control" value="<?php echo empty($oname) ? "" : $oname ?>" />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Started Year</label>
                            <div class="col-sm-9">
                              <input type="date" name="year" class="form-control" value="<?php echo empty($year) ? "" : $year ?>"/>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">GST%</label>
                            <div class="col-sm-9">
                              <input type="number" name="gst" class="form-control" value="<?php echo empty($gst) ? "" : $gst ?>"/>
                            </div>
                          </div>
                        </div>
                      
                      </div>
                 
                      <p class="card-title">Vendor Address </p>
                      <div class="row">
                       
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">City</label>
                            <div class="col-sm-9">
                              <input type="text" name="city" class="form-control" value="<?php echo empty($city) ? "" : $city ?>"/>
                            </div>
                          </div>
                        </div>
                          <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">State</label>
                            <div class="col-sm-9">
                              <input type="text" name="state" class="form-control" value="<?php echo empty($state) ? "" : $state ?>"/>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Country</label>
                            <div class="col-sm-9">
                              <select name="country" class="form-control">
                        <option value="america"  <?php if (isset($id)) if ($country == 'america') { ?> selected="selected" <?php } ?>>America</option>
                                <option value="italy" <?php if (isset($id)) if ($country == 'italy') { ?> selected="selected" <?php } ?>>Italy</option>
                                <option value="russia" <?php if (isset($id)) if ($country == 'russia') { ?> selected="selected" <?php } ?>>Russia</option>
                                <option value="britain" <?php if (isset($id)) if ($country == 'britain') { ?> selected="selected" <?php } ?>>Britain</option>
                              </select>
                            </div>
                          </div>
                        </div>
                       <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Postcode</label>
                            <div class="col-sm-9">
                              <input type="text" name="postcode" class="form-control" value="<?php echo empty($pincode) ? "" : $pincode ?>"/>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        
                         <div class="col-md-12">
                          <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Address</label>
                            <div class="col-sm-9">
                               <textarea class="form-control" name="address" id="exampleTextarea1" ><?php echo empty($address) ? "" : $address ?></textarea>
                            </div>
                          </div>
                        </div>


                     
                         
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">
                              
                            </label>
                            <div class="col-sm-9">
                             
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">

                              
                       <button type="submit" class="btn btn-success mb-2">Update</button>
                            </label>
                            <div class="col-sm-9">
                              <label class="col-sm-3 col-form-label">
                       <button type="submit" class="btn btn-warning mb-2">Back</button>
                            </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                    
                  </div>
                </div>
              </div>