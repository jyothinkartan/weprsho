<div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                  <?php if($this->session->flashdata('error')){
                      ?>
                  <h5 class="error-msg">
               
                  <?php echo $this->session->flashdata('error'); ?>
          
                   </h5>
                   <?php } 
                   if($this->session->flashdata('success')){?>
                   <h5 class="success-msg">
                
                <?php echo $this->session->flashdata('success'); ?>
                 </h5>
                 <?php } ?>
                    <h4 class="card-title">All Sku</h4>
                    <?php $role = $this->session->userdata('role'); 
                    if ($role==0) {?>
                    <a href="<?php echo base_url();?>admin/add_sku"><button type="button" class="btn btn-primary btn-rounded btn-fw">Add sku</button></a>
                    <?php }else{
                      echo "Please Contact/mail us admin@admin.com for add new category";
                    } ?>
                    <table id="datatable1" class="table table-bordered">
                      <thead>
                        <tr>
                          
                        
                          <th> Product   </th>
                          <th> SKU ID</th>
                          <th> Category</th>
                            <th>SUb Category  </th>
                            <?php $role = $this->session->userdata('role'); 
                    if ($role==0) {?>
                          <th> Action </th>
                          <?php }?>
                        </tr>
                      </thead>
                      <tbody>
                                   <?php
                if($tableData)
                {

                  
                  foreach ($tableData as $key)
                  {
                   
                    ?>
                        <tr>
                          
                      
                          <td> <img src="<?php echo base_url('assets/product_images/'. $key->sku_id.'/'. $key->pro_image)?>" />&nbsp<?php echo $key->pro_name;?> <div class="span4 proj-div"><a href="<?php echo base_url('admin/sku_images/'.$key->id); ?>"><button class="badge badge-success">Images</button></a></div></td>
                          <td> <?php echo $key->sku_id;?></td>
                          <td> <?php echo $key->cat_name;?></td>
                          <td> <?php echo $key->subcat_name;?></td>
                          <?php $role = $this->session->userdata('role'); 
                    if ($role==0) {?>
                          <td> 
                            <a href="<?php echo base_url('admin/edit_sku/'.$key->id); ?>" ><button class="badge badge-warning">Edit</button></a>
                          <a href="<?php echo base_url('admin/delete_sku/'.$key->id); ?>" onClick="return confirm('Are you sure you want to delete?')"><button class="badge badge-danger">Deelet</button></a></td>
                       <?php } ?>
                        </tr>
                      <?php } } ?>

                        
                        
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

<div id="GSCCModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
 <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
       
      </div>
      <div class="modal-body">
      
          <div class="row">

                <div class="col-md-6">
                <img src="<?php echo base_url('assets/product_images/'. $key->sku_id.'/'. $key->pro_image)?>">
                </div> 
                <div class="col-md-6">
                <img src="<?php echo base_url('assets/product_images/'. $key->sku_id.'/'. $key->pro_image)?>">
                </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning btn-rounded" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>