<!-- Product Details Section Begin -->

<section class="product-details spad">
        <div class="container">
            <div class="row">
             
            <div id="frontsuccess">
                            <?php echo $this->session->flashdata('success'); ?>
                        </div>
                            <div id="fronterror"><?php echo $this->session->flashdata('error'); ?>
                                    </div>
                <div class="col-lg-12">
                    <div class="product__details__tab">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab"
                                    aria-selected="true"><button type="button" class="btn btn-info btn-round" >Profile
                                </button></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab"
                                    aria-selected="false"><button type="button" class="btn btn-info btn-round" >Orders
                                </button></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab"
                                    aria-selected="false"><button type="button" class="btn btn-info btn-round" >Description
                                </button></a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tabs-1" role="tabpanel">
                                <div class="product__details__tab__desc">
                                    <h6>Profile Details</h6>
                                  
                             <div class="col-lg-12 col-md-12">
                                    <div class="row">
                                       <div class="col-lg-4">
 
                                       <img src="<?php echo base_url();?>assets/profile/<?php echo  $customer[0]->image?>" alt="profile" class="">
                                       <form method="post" enctype="multipart/form-data" >
                                           <input type="file" name="" id="imginput" style="display:none;">
                                           <input type="submit" id="upload" style="display:none;" value="Upload">
                                      </from>
                                       <button class="site-btn editimg" data-id="<?php echo $customer[0]->id ?>"> Edit Image</button>
                                        </div>
                                        <div class="col-lg-4">
                                       <input type="text" value="<?php echo empty($customer[0]->email) ? "" : $customer[0]->email ?>">

                                        </div>
                                    </div>
                                 <div class="row">

                                     <?php if(!empty($address)){
                                            foreach ($address as  $value) {
                                    
                                        ?>
                                        <div class="col-lg-4">
                                        <div class="address-box">
                                            <p><b><?php echo $value->fullname ?></b><br>
                                            <?php echo $value->phone ?><br>
                                            Landmark : <?php echo $value->landmark ?><br>
                                            Address : <?php echo $value->address ?> <br>
                                            <?php echo $value->city ?> , &nbsp <?php echo $value->state ?>, &nbsp <?php echo $value->country ?>&nbsp <?php echo $value->postcode ?>
                                        
                                        </p>
                                        <a href="<?php echo base_url('editaddress/'.$value->id);?>"><button> Edit </button></a>
                                        
                                        <button class="remove" data-id="<?php echo $value->id ?>"> Delete</button>
                                        </div>
                                        </div>
                                         
                                        <?php    }
                                        } ?>
                                </div>
                            </div>

             
                     
                
                                </div>
                            </div>
                            <div class="tab-pane" id="tabs-2" role="tabpanel">
                                <div class="product__details__tab__desc">
                                    <h6>Order Summary</h6>
                                    <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                    <?php   
                         
                         if($orders){
                                foreach ($orders as  $value) {
                                 ?>
                            <div class="row">
                           
                                <div class="col-lg-3">
                                    <div class="checkout__input">
                                        <p>Order Id<span>*</span></p>
                                        <?php echo  $value->order_id?>
                                        </div>
                                </div>  
                                <div class="col-lg-3">
                                    <div class="checkout__input">
                                        <p>Payment Type <span>*</span></p>
                                        <?php echo  $value->payment_type?>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="checkout__input">
                                        <p>Total Price<span>*</span></p>
                                        <?php echo  $value->total_price?> 
                                    </div>
                                    
                                </div>
                                <div class="col-lg-3">
                                    <div class="checkout__input">
                                        <p>Status <span>*</span></p>
                                        <?php $sta=$value->status;
                                        if($sta==1)
                                        {
                                            $status="Initiated";
                                        } echo $status;?> 
                                    </div>
                                    <a href="#" id="show-hidden-menu" class="warning-btn">Show Products</a>
                                </div>
                                <div class="row">
                                <div class="col-lg-12">
                                <div class="shoping__cart__table hidden-menu" style="display: none;">
                                <table>
                                    <thead>
                                <tr>
                                    <th class="shoping__product">Name</th>
                                    <th>Quantity</th>
                                    <th> Price</th>
                                    <th>total price</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php   
                         
                         if($order_products){
                         foreach ($order_products as  $products) {
                          ?>
                            <tr>
                            <td class="shoping__cart__item">
                            <img src="<?php echo base_url();?>assets/product_images/<?php echo  $products->pro_image?>" alt="" class="cartimg">
                                    <?php echo  $products->pro_name?>
                                    </td>
                                    <td class="shoping__cart__price">
                                    <?php echo  $products->quantity?>
                                    </td>
                         
                                    <td class="shoping__cart__quantity">
                                    <?php echo  $products->price?>
                                    </td>
                                    <td class="shoping__cart__quantity">
                                    <?php echo  $products->total_price?>
                                    </td>
                                    <td class="shoping__cart__quantity">
                                    <a href="<?php echo base_url('track/'. $products->id); ?>"  class="primary-btn">Track Your Order</a>
                                    </td>

                            </tr>
                            <?php  } } ?>
                            </tbody>
                        </table>
                        </div>
                        </div>
                                </div>
                               
                            </div>
                            <hr>
                           <?php } } ?>
                          
                          
                        </div>


              
                        </div>


                                </div>
                            </div>
                            <div class="tab-pane" id="tabs-3" role="tabpanel">
                                <div class="product__details__tab__desc">
                                    <h6>Activities</h6>
                                    <p>Hello ac diam sit amet quam vehicula elementum sed sit amet dui.
                                        Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus.
                                        Vivamus suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam
                                        sit amet quam vehicula elementum sed sit amet dui. Donec rutrum congue leo
                                        eget malesuada. Vivamus suscipit tortor eget felis porttitor volutpat.
                                        Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Praesent
                                        sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac
                                        diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ante
                                        ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                                        Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.
                                        Proin eget tortor risus.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Product Details Section End -->
    <script>
    
    $(".editimg").click(function(e){
    
        $("#imginput").show();
        $("#upload").show();
        $(".editimg").hide();
       
    e.preventDefault();
    
 });

    $(".remove").click(function(){
     var id = $(this).data("id");

 if(confirm('Are you sure you want delete?')){
         $.ajax({
            url: '<?php echo base_url('Customer/delete_address'); ?>',
            type: 'POST',
            data: {
             'adrsid': id,
             },
            dataType: 'json',
            success: function(data) { 
                       if(data.status==200)
                         {
                             window.location = "<?php echo base_url('placeorder'); ?>";
                         } 
            }
         });
 }
 return false;
 });

    $(document).ready(function() {
  $('#show-hidden-menu').click(function() {
    $('.hidden-menu').slideToggle("fast");
  

    // Alternative animation for example
    // slideToggle("fast");
  });
});
        setTimeout(function() {
            $('#frontsuccess').hide('fast');
        }, 3000);
        setTimeout(function() {
            $('#fronterror').hide('fast');
        }, 3000);
    </script>   