<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Ogani Template">
    <meta name="keywords" content="Ogani, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WePrSho </title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css" type="text/css">
     <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
       
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Humberger Begin -->
    <div class="humberger__menu__overlay"></div>
    <div class="humberger__menu__wrapper">
        <div class="humberger__menu__logo">
            <a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/img/logo.png" alt=""></a>
        </div>
        <div class="humberger__menu__cart">
            <ul>     <?php  $cid = $this->session->userdata('cid');
              $this->load->model('product_model');
                                    
                                    if($cid){ 
                                        $wishlist=0;
                                        $wishvalue=$this->product_model->wish_count($cid);
                                        
                                        if($wishvalue){
                                            $wishlist=count($wishvalue);
                                        }
                                        ?>
                            <li><a href="<?php echo base_url();?>wishlist"><i class="fa fa-heart"></i><span><?php echo $wishlist;?></span> </a></li>
                            <?php } ?>
                
                <?php  
                          
                            $cookie_id  = get_cookie('cookie_id'); 
                            $count=0;
                            if ($cookie_id) {

                                $cvalue=$this->product_model->cart_count($cookie_id);
                                if($cvalue){
                                    $count=count($cvalue);
                                }
                                
                                
                            }?>
                <li>Cart <a href="<?php echo base_url();?>cart"><i class="fa fa-shopping-cart" ></i> <span><?php echo $count;?></span></a></li>
            </ul>
           
        </div>
        <div class="humberger__menu__widget">
           
            <div class="header__top__right__auth">
                <a href="<?php echo base_url();?>signin"><i class="fa fa-user"></i> Login</a>
            </div>
        </div>
        <nav class="humberger__menu__nav mobile-menu">
            <ul>
                <li class="active"><a href="<?php echo base_url();?>">Home</a></li>
                <li><a href="<?php echo base_url();?>shop">Shop</a></li>
                <li><a href="<?php echo base_url();?>blog">Blog</a></li>
                <li><a href="<?php echo base_url();?>cantact">Contact</a></li>
            </ul>
        </nav>
        <div id="mobile-menu-wrap"></div>
        <div class="header__top__right__social">
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-pinterest-p"></i></a>
        </div>
       
    </div>
    <!-- Humberger End -->

    <!-- Header Section Begin -->
    <header class="header">
       
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/img/logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <nav class="header__menu">
                        <ul>
                            <li class="active"><a href="<?php echo base_url();?>">Home</a></li>
                            <li><a href="<?php echo base_url();?>shop">Shop</a></li>
                            <li><a href="<?php echo base_url();?>blog">Blog</a></li>
                            <li><a href="<?php echo base_url();?>cantact">Contact</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-4">
                    <div class="header__cart">
                        <ul>
                            <?php  $cid = $this->session->userdata('cid');
                                $this->load->model('product_model');
                                
                                    
                                    if($cid){ 
                                        $wishlist=0;
                                        $wishvalue=$this->product_model->wish_count($cid);
                                        
                                        if($wishvalue){
                                            $wishlist=count($wishvalue);
                                        }
                                    ?>
                            <li><a href="<?php echo base_url();?>wishlist"><i class="fa fa-heart"></i> <span><?php echo $wishlist;?></span></a></li>
                            <?php } 
                            
                        
                            $cookie_id  = get_cookie('cookie_id'); 
                            $count=0;
                            if ($cookie_id) {

                                $cvalue=$this->product_model->cart_count($cookie_id);
                                if($cvalue){
                                    $count=count($cvalue);
                                }
                            }
                            ?>

                            <li>Cart <a href="<?php echo base_url();?>cart"><i class="fa fa-shopping-cart" ></i> <span><?php echo $count;?></span></a></li>
                        </ul>

                        <?php 
                            
                            $cid = $this->session->userdata('cid');
                                    
                            if($cid){

                              $name = $this->session->userdata('fname');
                              ?>
                              <div class="header__top__right__language">
                                <div>   <button type="button" class="btn btn-info btn-round" ><?php echo $name ;?> 
                                <span class="arrow_carrot-down"></span></button>  </div>
                                
                                <ul>
                                    <li><a href="<?php echo base_url();?>dashboard">Dashboard</a></li>
                                    <li><a href="<?php echo base_url();?>signout">Signout</a></li>
                                </ul>
                            </div>
                              <?php
                            }else{
                                ?>

                            <div class="header__top__right__auth">
                              
                              <a href="<?php echo base_url();?>signin" class="text-info">
                            
  
                                <button type="button" class="btn btn-info btn-round" >SignIn 
                                </button>  
                                </a>
                                
                            </div>

                                <?php

                            }
                            ?>

                    </div>
                </div>
            </div>
            
            <div class="humberger__open">
                <i class="fa fa-bars"></i>
            </div>
        </div>
    </header>
    <!-- Header Section End -->

    <!-- Hero Section Begin -->
    <section class="hero hero-normal">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="hero__categories">
                        <div class="hero__categories__all">
                            <i class="fa fa-bars"></i>
                            <span>All Categories</span>
                        </div>
                        
                        <ul>
                      
            <?php 
             $this->load->model('vendor_model');
            if ($category) { 
            foreach ($category as $value) {
            $subcat=$this->vendor_model->show_subcategory($value->id);
            ?>
        <li>
        <a href="#" class="accordion-heading" data-toggle="collapse" data-target="#submenu<?php  echo $value->id; ?>"><?php  echo $value->cat_name; ?><span class="arrow_carrot-down"></span></a>
								
								<div class="nav nav-list collapse" id="submenu<?php  echo $value->id; ?>">
                                <p>  <?php if(!empty($subcat)){
                                        foreach($subcat as $values){?>
									<a href="<?php echo base_url('subcategory/'.$values->id); ?>" title="Title"><?php echo $values->subcat_name?></a>
                                    <?php } } ?>
                                    </p>
								</div>
                                	
								
							</li>

                            
                            
                          <?php    } }  
                else
                {
                    ?>

                    <li><a href="#"><?php  echo "WePrSho" ?></a></li>
                    <?php
                }
                ?>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="hero__search">
                        <div class="hero__search__form">
                        <form action="<?php echo base_url(); ?>search" method="post">
                                
                                <input type="text" name="search" placeholder="What do yo u need?">
                                <button type="submit" class="site-btn">SEARCH</button>
                            </form>
                        </div>
                        <div class="hero__search__phone">
                            <div class="hero__search__phone__icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="hero__search__phone__text">
                                <h5>+65 11.188.888</h5>
                                <span>support 24/7 time</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    