
<section class="checkout spad">
        <div class="container">
            
          <div class="checkout__form">
           <a href="<?php echo base_url(); ?>placeorder"> <button type="submit" >Back</button></a>
                <h4>Edit Address</h4>
                
                <form method="post" action="<?php echo base_url(); ?>updateaddress">
                    <div class="row">
                        <div class="col-lg-8 col-md-6">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Full Name<span>*</span></p>
                                        <input type="hidden" name="id" value="<?php echo empty($address[0]->id) ? "" : $address[0]->id ?>" >
                                        <input type="text" name="fname" value="<?php echo empty($address[0]->fullname) ? "" : $address[0]->fullname ?>" >
                                    </div>
                                </div>  
                               <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Phone<span>*</span></p>
                                        <input type="number" name="phone" value="<?php echo empty($address[0]->phone) ? "" : $address[0]->phone ?>" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                    <p>Town/City<span>*</span></p>
                                        <input type="text" name="city" value="<?php echo empty($address[0]->city) ? "" : $address[0]->city ?>" >
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                    <p>State<span>*</span></p>
                                        <input type="text" name="state" value="<?php echo empty($address[0]->state) ? "" : $address[0]->state ?>" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                    <p>Country<span>*</span></p>
                                        <input type="text" name="country" value="<?php echo empty($address[0]->country) ? "" : $address[0]->country ?>" >
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                    <p>Postcode <span>*</span></p>
                                        <input type="number" name="postcode" value="<?php echo empty($address[0]->postcode) ? "" : $address[0]->postcode ?>" >
                                     </div>
                                </div>
                            </div>
                            <div class="checkout__input">
                            <p>Landmark<span>*</span></p>
                                <input type="text" name="landmark" placeholder="Street Address" class="checkout__input__add" value="<?php echo empty($address[0]->landmark) ? "" : $address[0]->landmark ?>" >
                                
                                <p>Address<span>*</span></p>
                                
                                <input type="text" name="address" placeholder="Apartment, suite, unite ect (optinal)" value="<?php echo empty($address[0]->address) ? "" : $address[0]->address ?>" >
                            </div>
                          
                           
                            <button type="submit" class="site-btn">Update</button>
                        </div>


                    </div>
                </form>
            </div>
        </div>
    </section>