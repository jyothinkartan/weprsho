<style>
    .order-tracking{ position: relative;height: 400px;}
		.moretext {display: none;}
		.order-tracking h4{font-weight: 600; margin-bottom: 20px;}
		.track-status {    position: relative;padding-left: 50px;margin: 20px 0;}
		.track-status h5{font-weight: 600;}
		.track-status span{color: #999;}
		.track-status p{    margin: 0; color: #000;}
		.track-status::before{ position: absolute; content: ''; width: 15px; height: 15px; background: #09A64E;    border-radius: 50%;  top: 4px; left: 0;}
		.track-status::after{position: absolute; content: '';width: 2px; height: 120%;background: #09A64E;  left: 6px; top: 18px;}
		.moreless-button{    padding-left: 50px; color: #C50227; font-weight: 700;}
		.moreless-button:hover{color: #C50227;}
    </style>

<section class="checkout spad">
        <div class="container">
<div class="order-tracking">
		<h4>Order Tracking</h4>
		<div class="track-status">
			<h5>Order Placed</h5>
			<span>08:00 AM, 04 March, 2022</span>
		</div>
		<div class="track-status">
			<h5>Shipped</h5>
			<span>04 March, 2022</span>
		</div>
		<div class="track-status ">
			<h5>Delivered</h5>
			<p>Your item has been delivered</p>
			<span>07:54 PM, 07 March, 2022</span>
		</div>
		<div class="moretext">
			<div class="track-status">
			<h5>Shipped</h5>
			<span>04 March, 2022</span>
		</div>
		
		</div>
		<a class="moreless-button" href="#">Show More </a>
	</div>

    </div>
    </section>
<script>
		$('.moreless-button').click(function() {
  $('.moretext').slideToggle();
  if ($('.moreless-button').text() == "Show Less") {
    $(this).text("Show More")
  } else {
    $(this).text("Show Less")
  }
});
	</script>