
    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="<?php echo base_url();?>assets/img/breadcrumb.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Product Details</h2>
                        <div class="breadcrumb__option">
                            <a href="<?php echo base_url();?>">Home</a>
                            <a href="<?php echo base_url();?>shop">Shop</a>
                            <span>Product Details</span>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Product Details Section Begin -->
    <section class="product-details spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                
                            <div id="frontsuccess">
                            <?php echo $this->session->flashdata('success'); ?>
                        </div>
                            <div id="fronterror"><?php echo $this->session->flashdata('error'); ?>
                                    </div>
                    <div class="product__details__pic">
                        <div class="product__details__pic__item">
                        <?php if(!empty($product)){ ?>
                            <img class="product__details__pic__item--large"
                          
                                src="<?php echo base_url();?>assets/product_images/<?php echo  $product[0]->sku_id?>/<?php echo  $product[0]->pro_image?>" alt="">
                                <?php }?>
                        </div>
                        <div class="product__details__pic__slider owl-carousel">
                        <?php if(!empty($skuimages)){
                              foreach ($skuimages as  $value) { ?>
                            <img data-imgbigurl="<?php echo base_url();?>assets/product_images/sku<?php echo  $value->sku_id?>/<?php echo  $value->image?>"
                                src="<?php echo base_url();?>assets/product_images/sku<?php echo  $value->sku_id?>/<?php echo  $value->image?>" alt="">
                                <?php } }?>
                            </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="product__details__text">
                        <h3><?php if(!empty($product)){ echo $product[0]->pro_name;}?></h3>
                        <div class="product__details__rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                            <span>(18 reviews)</span>
                        </div>
                        <div class="product__details__price">$<?php if(!empty($product)){ echo $product[0]->pro_price;}?> <p><h5>About this product</h5></div>
                        <p><?php if(!empty($product)){ echo $product[0]->description;}?></p>
                        <input type="hidden" name="proid" id="proid" value="<?php echo $product[0]->id; ?>">
                        <label>Quantity:</label>
                          <div>
                        <select name="qty" id="qty">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            </select>
                        </div><br>
                        <br>
                        <?php if(!empty($product)){ if($product[0]->is_stock==1 && $product[0]->quantity>0){ ?> 
                           
                            <a href="" class="warning-btn buynow">Buy Now</a>
                           <a href="" class="primary-btn addtocart">ADD TO CARD</a>
                        <?php }else{ ?>
                            <samp> <a href="" class="warning-btn">Notify Me</a></samp> 
                            <?php } }?>
                        <?php if($this->session->userdata('cid'))
	                             {?>
                        <a data-id="<?php echo $product[0]->id; ?>" data-value="<?php echo $product[0]->id; ?>" class="heart-icon wishlist"><span class="icon_heart_alt"></span></a>
                        <?php } ?> 
                        <ul>
                            <li><b>Availability</b> <span> 

                            <?php if(!empty($product)){ if($product[0]->is_stock==1 && $product[0]->quantity>0){ ?> InStock<?php }else{

                            ?><samp>Outofstock</samp> <?php } }?> &nbsp &nbsp<samp> Sold By:</samp><?php if(!empty($product)){ echo $product[0]->venname;}?></span></li>
                            <li><b>Shipping</b> <span>01 day shipping. <samp>Free pickup today</samp></span></li>
                            <li><b>Weight</b> <span>0.5 kg</span></li>
                            <li><b>Share on</b>
                                <div class="share">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                    <a href="#"><i class="fa fa-pinterest"></i></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="product__details__tab">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab"
                                    aria-selected="true">Description</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab"
                                    aria-selected="false">Information</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab"
                                    aria-selected="false">Reviews <span>(1)</span></a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tabs-1" role="tabpanel">
                                <div class="product__details__tab__desc">
                                    <h6>Products Infomation</h6>
                                    <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.
                                        Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus. Vivamus
                                        suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam sit amet quam
                                        vehicula elementum sed sit amet dui. Donec rutrum congue leo eget malesuada.
                                        Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat,
                                        accumsan id imperdiet et, porttitor at sem. Praesent sapien massa, convallis a
                                        pellentesque nec, egestas non nisi. Vestibulum ac diam sit amet quam vehicula
                                        elementum sed sit amet dui. Vestibulum ante ipsum primis in faucibus orci luctus
                                        et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam
                                        vel, ullamcorper sit amet ligula. Proin eget tortor risus.</p>
                                        <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem
                                        ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit aliquet
                                        elit, eget tincidunt nibh pulvinar a. Cras ultricies ligula sed magna dictum
                                        porta. Cras ultricies ligula sed magna dictum porta. Sed porttitor lectus
                                        nibh. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.
                                        Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Sed
                                        porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum
                                        sed sit amet dui. Proin eget tortor risus.</p>
                                </div>
                            </div>
                            <div class="tab-pane" id="tabs-2" role="tabpanel">
                                <div class="product__details__tab__desc">
                                    <h6>Products Infomation</h6>
                                    <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.
                                        Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus.
                                        Vivamus suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam
                                        sit amet quam vehicula elementum sed sit amet dui. Donec rutrum congue leo
                                        eget malesuada. Vivamus suscipit tortor eget felis porttitor volutpat.
                                        Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Praesent
                                        sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac
                                        diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ante
                                        ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                                        Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.
                                        Proin eget tortor risus.</p>
                                    <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem
                                        ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit aliquet
                                        elit, eget tincidunt nibh pulvinar a. Cras ultricies ligula sed magna dictum
                                        porta. Cras ultricies ligula sed magna dictum porta. Sed porttitor lectus
                                        nibh. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.</p>
                                </div>
                            </div>
                            <div class="tab-pane" id="tabs-3" role="tabpanel">
                                <div class="product__details__tab__desc">
                                    <h6>Products Infomation</h6>
                                    <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.
                                        Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus.
                                        Vivamus suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam
                                        sit amet quam vehicula elementum sed sit amet dui. Donec rutrum congue leo
                                        eget malesuada. Vivamus suscipit tortor eget felis porttitor volutpat.
                                        Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Praesent
                                        sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac
                                        diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ante
                                        ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                                        Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.
                                        Proin eget tortor risus.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Product Details Section End -->

    <!-- Related Product Section Begin -->
    <section class="related-product">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title related__product__title">
                        <h2>Related Products</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php if(!empty($relatedpro)){
                    foreach ($relatedpro as  $value) {
                        
                    ?>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="product__item">
                        <div class="product__item__pic set-bg" data-setbg="<?php echo base_url();?>assets/product_images/<?php echo  $value->sku_id?>/<?php echo  $value->pro_image?>">
                            <ul class="product__item__pic__hover">
                            <?php  $cid = $this->session->userdata('cid');
                                    
                                    if($cid){ ?>
                             <li><a  data-id="<?php echo $value->id; ?>" data-value="<?php echo $cid; ?>" class="wishlist"><i class="fa fa-heart"></i> </a></li>
                            <?php } ?>

                                        <li><a href="<?php echo base_url('product/'.$value->id); ?>"><i class="fa fa-retweet"></i></a></li>
                                        
                            </ul>
                        </div>
                        <div class="product__item__text">
                            <h6><a href="<?php echo base_url('product/'.$value->id); ?>"><?php echo  $value->pro_name?></a></h6>
                            <h5>$<?php echo  $value->pro_price?></h5>
                        </div>
                    </div>
                </div>
                <?php  } }?>
                
            </div>
        </div>
    </section>



    <script>
       
            $('.buynow').click(function() {
            var proid = $("#proid").val();
        var qty = $("#qty").val();
        
    $.ajax({
        url: '<?php echo base_url('Cart/buynow'); ?>',
        type: 'POST',
        data: {
            'proid': proid,
            'qty': qty,
        },
        dataType: 'json',
        success: function(data) {
                            //result=JSON.parse(result);
                            // console.log(data);
                            if(data.status==200)
                            {
                                window.location = "<?php echo base_url('checkout'); ?>";
                                
                            }
                            if(data.status==300)
                            {
                                window.location = "<?php echo base_url('signin'); ?>";
                                
                            }
                            if(data.status==400)
                            {
                               alert('Internal server error')
                                
                            }
        }
    });
});



        $('.addtocart').click(function(e) {
            var proid = $("#proid").val();
            var qty = $("#qty").val();
            
     
    $.ajax({
        url: '<?php echo base_url('Cart/addtocart'); ?>',
        type: 'POST',
        data: {
            'proid': proid,
            'qty': qty,
        },
        dataType: 'json',
        success: function(data) {
                            //result=JSON.parse(result);
                            //  console.log(data);
                          
                            $(".cart").append(qty);
                                $("#frontsuccess").empty().append('Successfully added to cart');
                               
                            
                            
        }
       
    });
    e.preventDefault();
});


$('.wishlist').click(function() {
    var proid = $(this).attr("data-id");
            var uid =$(this).attr("data-value");
            
      
    $.ajax({
        url: '<?php echo base_url('Customer/add_wishlist'); ?>',
        type: 'POST',
        data: {
            'proid': proid,
            'uid': uid,
        },
        dataType: 'json',
        success: function(data) {
                            // result=JSON.parse(result);
 console.log(data);
                            if(data.status==200)
                            {
                                alert('Successfully added to Wishlist');
                                location.reload();
                               
                            }
                            if(data.status==500)
                            {
                                alert('Already added to Wishlist');
                               
                            }
        }
    });
});


       </script>