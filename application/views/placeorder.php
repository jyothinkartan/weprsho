<style> 

#checkout{
    display: none;
}
.add-address{
    display: none;
    color:red;
}
.address-box {
  width: 370px;
  height: 200px;  
  padding: 15px;
  border-style: dotted;
}
</style>

<section class="checkout spad">
        <div class="container">
            
            <div class="checkout__form">
                <h4>Billing Details</h4>
                <form action="#">
                    <div class="row">


                    <div class="col-lg-8 col-md-6">
                           <div class="row">
                           <?php if(!empty($address)){
                                    foreach ($address as  $value) {
                                    
                                ?>
                                <div class="col-lg-6">
                                <div class="address-box">
                                    <p><b><?php echo $value->fullname ?></b><br>
                                    <?php echo $value->phone ?><br>
                                    Landmark : <?php echo $value->landmark ?><br>
                                    Address : <?php echo $value->address ?> <br>
                                    <?php echo $value->city ?> , &nbsp <?php echo $value->state ?>, &nbsp <?php echo $value->country ?>&nbsp <?php echo $value->postcode ?>
                                
                                   </p>
                                  <a href="<?php echo base_url('editaddress/'.$value->id);?>"> Edit </a>
                                  
                                  <button class="remove" data-id="<?php echo $value->id ?>"> Delete</button>
                                 </div>
                                 
                                 <button class="warning-btn useadrs" data-id="<?php echo $value->id ?>"> Use This Address</button>
                                   
                                </div>
                                
                                <?php    }
                                } ?>
                            </div>
                            <button id="addnewaddress" class="primary-btn">Add New Address</button>
                            
                            <div class="adrsadding" style="display:none;">
                               <div class="row">
                               <div id="inserterror" class="add-address" >
                                Unable to insert
                                    </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Full Name<span>*</span></p>
                                        <input type="text" id="fname">
                                        <div id="errorfname" class="add-address" >
                                Full name is required
                                    </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Phone<span>*</span></p>
                                        <input type="number" id="phone">
                                        <div id="errorphone" class="add-address" >
                                Mobile number is required
                                    </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Town/City<span>*</span></p>
                                        <input type="text" id="city">
                                        <div id="errorcity" class="add-address" >
                                city field is required
                                    </div>
                                    </div>
                                </div> 
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>State<span>*</span></p>
                                        <input type="text" id="state">
                                        <div id="errorstate" class="add-address" >
                                State field is required
                                    </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Country<span>*</span></p>
                                        <input type="text" id="country">
                                        <div id="errorcountry" class="add-address" >
                                Country field is required
                                     </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Postcode<span>*</span></p>
                                        <input type="number" id="postcode">
                                        <div id="errorpostcode" class="add-address" >
                                Postcode field is required
                                    </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Landmark<span>*</span></p>
                                        <input type="text" id="landmark">
                                        <div id="errorlandmark" class="add-address" >
                                Landmark field is required
                                    </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Address<span>*</span></p>
                                        <input type="text" id="address">
                                        <div id="erroraddress" class="add-address" >
                                          Address field is required
                                        </div>
                                  </div>
                                </div>
                               
                             </div>
                             <button type="submit" class="site-btn adrsadd">Add</button>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                        <input type="hidden" id="adrsid" value="">
                        <?php 
                    $cookie_id  = get_cookie('cookie_id');
                        if($cookie_id){
                            ?>
                             <input type="hidden" id="cookieid" value="<?php echo $cookie_id?>">
                            <?php
                        }else{
                            $cookie_id=0;
                            ?>
                            <input type="hidden" id="cookieid" value="<?php echo $cookie_id?>">
                            <?php
                        }
                        ?>
                       
                            <div class="checkout__order" id="checkout">
                                <h4>Your Order</h4>
                                <div class="checkout__order__products">Products <span>Total</span></div>
                                <ul>
                                <?php 
                                  $total=0; 
                                  if($product) 
                                  {
                                    
                                     foreach ($product as $value) {
                                  ?>
                                  <li><?php echo $value->pro_name;?> <span>$<?php echo $value->pro_price;?></span></li>
                                  <?php 
                                  $total+=$value->pro_price * $value->quantity;
                                  }
                                  }?>
                                </ul>
                                <div class="checkout__order__subtotal">Subtotal <span>$<?php echo  $total;?></span></div>
                                <div class="checkout__order__total">Total <span>$<?php echo  $total;?></span></div>
                                
                                <div class="checkout__input__checkbox">
                                <h4>Choose Payment Method</h4>
                                <div id="fronterror" style="display: none">
                                Please choose payment method
                                    </div>
                                    <label for="payment">
                                    Cash On Delivery
                                        <input type="checkbox" id="payment" onchange="getchecked()">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <!-- <div class="checkout__input__checkbox">
                                    <label for="paypal">
                                        Paypal
                                        <input type="checkbox" id="paypal">
                                        <span class="checkmark"></span>
                                    </label>
                                </div> -->
                                <div id="adrserror" style="display: none"  class="add-address">
                                Please choose Address 
                                    </div>
                                <button type="submit"  class="site-btn placeorder">PLACE ORDER</button>
                                
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- Checkout Section End -->

    

<script>
    
       $(".remove").click(function(){
        var id = $(this).data("id");

    if(confirm('Are you sure you want delete?')){
            $.ajax({
               url: '<?php echo base_url('Customer/delete_address'); ?>',
               type: 'POST',
               data: {
                'adrsid': id,
                },
               dataType: 'json',
               success: function(data) { 
                          if(data.status==200)
                            {
                                window.location = "<?php echo base_url('placeorder'); ?>";
                            } 
               }
            });
    }
    return false;
    });

    
  $('#addnewaddress').click(function(e) {
      
    e.preventDefault();
    $('.adrsadding').slideToggle("fast");

    // Alternative animation for example
    // slideToggle("fast");
  });






$('.adrsadd').click(function() {

    
    if (!$('#fname').val()) {
        $("#errorfname").show();
        return false;
    }

    if (!$('#phone').val()) {
        $("#errorphone").show();
        return false;
    }

    if (!$('#city').val()) {
        $("#errorcity").show();
        return false;
    }

    if (!$('#state').val()) {
        $("#errorstate").show();
        return false;
    }

    if (!$('#country').val()) {
        $("#errorcountry").show();
        return false;
    }
    if (!$('#postcode').val()) {
        $("#errorpostcode").show();
        return false;
    }

    if (!$('#landmark').val()) {
        $("#errorlandmark").show();
        return false;
    }

    if (!$('#address').val()) {
        $("#erroraddress").show();
        return false;
    }

    var fname = $("#fname").val();
    var phone = $("#phone").val();
    var city = $("#city").val();
    var state = $("#state").val();
    var country = $("#country").val();
    var postcode = $("#postcode").val();
    var landmark = $("#landmark").val();
    var address = $("#address").val();
    

    $.ajax({
    
        url: '<?php echo base_url('Customer/adrsadd'); ?>',
        type: 'POST',
        data: {
            'fname': fname,
            'phone': phone,
            'city': city,
            'state': state,
            'country': country,
            'postcode': postcode,
            'landmark': landmark,
            'address': address,
        },
        dataType: 'json',
        success: function(data) {
                            //result=JSON.parse(result);
    //  console.log(data);
                            if(data.status==200)
                            {
                                
                                window.location = "<?php echo base_url('placeorder'); ?>";
                            }
                            if(data.status==300)
                            {
                                $("#inserterror").show();
                            }
        }
    });


});

$(document).ready(function(){

  $("#fname").keypress(function(){
    $('#errorfname').hide();
  });

  $('#phone').keypress(function() {
    $('#errorphone').hide();
    });

    $('#city').keypress(function() {
    $('#errorcity').hide();
    });

    $('#state').keypress(function() {
    $('#errorstate').hide();
    });
    $('#country').keypress(function() {
    $('#errorcountry').hide();
    });
    $('#postcode').keypress(function() {
    $('#errorpostcode').hide();
    });
    $('#landmark').keypress(function() {
    $('#errorlandmark').hide();
    });
    $('#address').keypress(function() {
    $('#erroraddress').hide();
    });

    
});

  


function getchecked()
{
    if($('#payment').is(":checked"))   
    $("#fronterror").hide();
    else
    $("#fronterror").show();
}


$('.useadrs').click(function(e) { 
    var aid = $(this).attr("data-id");
    $("#adrsid").val(function() {
        $("#checkout").show();
        return  aid;

    });
    e.preventDefault();
});


$('.placeorder').click(function() { 
    
    if (!$('#payment').is(':checked')) {
        $("#fronterror").show();
    
        return false;
    } 
    if (!$('#adrsid').val()) {
        $("#adrserror").show();
        return false;
    }
   
    
    var adrsid = $("#adrsid").val();
    var cookieid = $("#cookieid").val();
    $.ajax({
        url: '<?php echo base_url('Customer/finish_order'); ?>',
        type: 'POST',
        data: {
            'adrsid': adrsid,
            'cookieid': cookieid,
        },
        dataType: 'json',
        success: function(data) {
                            //result=JSON.parse(result);
    //  console.log(data);
     
                            if(data.status==200)
                            {
                                
                                window.location = "<?php echo base_url('success'); ?>";
                            }
                            if(data.status==300)
                            {
                                alert('Unable to place order Some products seems out of stock')
                            }
        }
    });
});



 

    </script>