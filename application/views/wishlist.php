
<div id="frontsuccess">
                            <?php echo $this->session->flashdata('success'); ?>
                        </div>
                            <div id="fronterror"><?php echo $this->session->flashdata('error'); ?>
                                    </div>
  

    <!-- Shoping Cart Section Begin -->
    <section class="shoping-cart spad">
        <div class="container">
        <?php            
                if($wishproduct){
                    $subtotal=0;
                    ?>
            <div class="row">
               
                <div class="col-lg-12">
                    <div class="shoping__cart__table">
                        <table>
                            <thead>
                                <tr>
                                    <th class="shoping__product">Products</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            <?php   
                         
                         
                                foreach ($wishproduct as  $value) {
                                 ?>
                                <tr>
                                    <td class="shoping__cart__item">
                                        <img src="<?php echo base_url();?>assets/product_images/<?php echo  $value->pro_image?>" alt="" class="cartimg">
                                        <h5><a  href="<?php echo base_url('product/'.$value->proid); ?>"><?php echo  $value->pro_name?></a></h5>
                                    </td>
                                    <td class="shoping__cart__price">$
                                    <?php echo  $value->pro_price?>
                                    </td>
                         
                                    <td class="shoping__cart__quantity">
                                        <div class="quantity">
                                  
                                            <button><span class="dec decqtybtn" data-id="<?php echo $value->id?>" >-</span></button>
                                            
                                               <?php echo $value->quantity?>
                                                <button><span class="inc incqtybtn" data-id="<?php echo $value->id?>">+</span></button>
                                        </div>
                                      
                                    </td>
                                       
                                    
                                    <td class="shoping__cart__total">$
                                    <?php 
                                    $total=$value->quantity * $value->pro_price;
                                    echo $total
                                    ?>
                                    </td>
                                    <td class="shoping__cart__item__close">
                                    <button><span class="icon_close" data-id="<?php echo $value->id?>"></span> </button>
                                    </td>
                                    <td class="shoping__cart__quantity">
                                    <a data-id="<?php echo $value->proid?>" data-qty="<?php echo $value->quantity?>" data-listid="<?php echo $value->id?>" class="primary-btn addtocart">ADD TO CARD</a>
                                </td>
                                </tr>
                               <?php  $subtotal +=$value->quantity * $value->pro_price; }  ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            
              
            </div>
            <div class="row">
              <h5>Add more products to Wishlist</h5>
                <div class="col-lg-12">
                    <div class="shoping__cart__btns">
                        <a href="<?php echo base_url();?>shop" class="warning-btn">CONTINUE SHOPPING</a>
                       
                    </div>
                </div>
            </div>
            <?php } else{
                ?>

            <div class="row">
              <h5>Your Cart is empty</h5>
                <div class="col-lg-12">
                    <div class="shoping__cart__btns">
                        <a href="<?php echo base_url();?>shop" class="warning-btn">CONTINUE SHOPPING</a>
                       
                    </div>
                </div>
            
            </div>
            <?php } ?>
        </div>
    </section>
    <script>

$('.decqtybtn').click(function() {
 var listid =$(this).attr("data-id");
 
 $.ajax({
     url: '<?php echo base_url('Customer/wish_updateqty'); ?>',
     type: 'POST',
     data: {
         'listid': listid,
         'qty': 'dec',
     },
     dataType: 'json',
     success: function(data) {
                         //result=JSON.parse(result);
//  console.log(data);
                         if(data.status==200)
                         {
                         
                             window.location = "<?php echo base_url('wishlist'); ?>";
                         }
     }
 });
});

$('.incqtybtn').click(function() {
 var listid =$(this).attr("data-id");

 $.ajax({
     url: '<?php echo base_url('Customer/wish_updateqty'); ?>',
     type: 'POST',
     data: {
         'listid': listid,
         'qty': 'inc',
     },
     dataType: 'json',
     success: function(data) {
                         //result=JSON.parse(result);
//  console.log(data);
                         if(data.status==200)
                         {
                     
                             window.location = "<?php echo base_url('wishlist'); ?>";
                         }
     }
 });
});

$('.icon_close').click(function() {
 var listid =$(this).attr("data-id");
 $.ajax({
     url: '<?php echo base_url('Customer/wishlist_delete'); ?>',
     type: 'POST',
     data: {
         'listid': listid,
     },
     dataType: 'json',
     success: function(data) {
                         //result=JSON.parse(result);
//  console.log(data);
                         if(data.status==200)
                         {
                         
                             window.location = "<?php echo base_url('wishlist'); ?>";
                         }
     }
 });
});

$('.addtocart').click(function() {
    var proid =$(this).attr("data-id");
    var qty =$(this).attr("data-qty");
    var listid =$(this).attr("data-listid");
        

    $.ajax({
        url: '<?php echo base_url('Cart/addtocart'); ?>',
        type: 'POST',
        data: {
            'proid': proid,
            'qty': qty,
        },
        dataType: 'json',
        success: function(data) {
                            //result=JSON.parse(result);
 console.log(data);
                            if(data.status==200)
                            {
                                alert('Successfully added to cart')
                                    $.ajax({
                                        url: '<?php echo base_url('Customer/wishlist_delete'); ?>',
                                        type: 'POST',
                                        data: {
                                            'listid': listid,
                                        },
                                        dataType: 'json',
                                        success: function(data) { }
                                     });

                                window.location = "<?php echo base_url('cart'); ?>";
                            }
        }
    });


});
     </script>