<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();
		$this->load->model('product_model');
		$this->load->model('vendor_model');
		$this->load->helper('cookie');
	

     }


public function index()
{
	
	
	$cookie_id  = get_cookie('cookie_id'); 
	
	$data = array(
		'category' =>$this->vendor_model->show_category(),
		'products' =>$this->product_model->get_allpro(),
		'cartproduct' =>$this->product_model->get_cartpro($cookie_id),
	);
	
	$this->load->view('includes/header',$data);
	$this->load->view('cart');
	  $this->load->view('includes/footer');

}

public function buynow()
{
	$random  = substr(rand(), 0, 4);

		
			$this->input->set_cookie("name","buynow", 0);
			$this->input->set_cookie("cookie_id",$random, 0);
	
			$data=array(
				'cookie_id' =>$random,
				'product_id' =>$this->input->post('proid'),
				'quantity' =>$this->input->post('qty'),
				'status' =>0,
			);
	
			$insertcart = $this->product_model->cart_insert($data);
			if($insertcart)
			{ 
				if($this->session->userdata('cid'))
				{
					$response = array('status' => 200, 'data' =>'place order');

				}else{
					$response = array('status' => 300, 'data' =>'need to login');

				}			
				
			}else{
				$response = array('status' => 400, 'data' =>'unable to insert');
			}

			$this->output->set_content_type('application/json');
		echo json_encode($response);
	
}

public function addtocart()
{
    if($this->input->post('proid'))
    {
		$cookie_id  = get_cookie('cookie_id'); 
	
		if(!empty($cookie_id)){

			$proid =  $this->input->post('proid');
			$proexist = $this->product_model->check_pro($cookie_id,$proid);
				if($proexist==false){

					$data=array(
						'cookie_id' =>$cookie_id,
						'product_id' =>$this->input->post('proid'),
						'quantity' =>$this->input->post('qty'),
						'status' =>0,
					);

					$insertcart = $this->product_model->cart_insert($data);
					if($insertcart!=false)
					{
						$response = array('status' => 200, 'data' =>$insertcart);
					}

				}else{
					$cartid=$proexist[0]->id;
					$qty=$proexist[0]->quantity+1;
					$data=array(
						'quantity' =>$qty,
						
					);

					$updateqty = $this->product_model->cart_update($cartid,$data);
					if($updateqty!=false)
			{
				$response = array('status' => 200, 'data' =>$updateqty);
			}
				}
		}else{

			$random  = substr(rand(), 0, 4);

		
			$this->input->set_cookie("name","addtocart", 0);
			$this->input->set_cookie("cookie_id",$random, 0);
	
			$data=array(
				'cookie_id' =>$random,
				'product_id' =>$this->input->post('proid'),
				'quantity' =>$this->input->post('qty'),
				'status' =>0,
			);
	
			$insertcart = $this->product_model->cart_insert($data);
			if($insertcart!=false)
			{
				$response = array('status' => 200, 'data' =>$insertcart);
			}
		}

		
    }
	$this->output->set_content_type('application/json');
		echo json_encode($response);
}

public function updateqty()
{
	$cartid =$this->input->post('cartid');
        $qty =$this->input->post('qty');

		$insertcart = $this->product_model->get_qty($cartid );

		if($qty=="dec"){
			$delqty= $insertcart[0]->quantity;
			if($delqty==1)
			{
				$delete = $this->product_model->cart_prodel($cartid);
		if($delete==true)
		{
			$response = array('status' => 200, 'data' =>$delete);
		}
			}
			$data=array(
				'quantity' =>$insertcart[0]->quantity-1,
			);
		}
		if($qty=="inc"){
			$data=array(
				'quantity' =>$insertcart[0]->quantity+1,
			);
		}

		$updateqty = $this->product_model->cart_update($cartid,$data);
		if($updateqty!=false)
		{
			$response = array('status' => 200, 'data' =>$updateqty);
		}

		$this->output->set_content_type('application/json');
		echo json_encode($response);
}


public function deletecart()
{
	$cartid =$this->input->post('cartid');
      

		$delete = $this->product_model->cart_prodel($cartid);
		if($delete==true)
		{
			$response = array('status' => 200, 'data' =>$delete);
		}

		$this->output->set_content_type('application/json');
		echo json_encode($response);
}

public function checkout()
{
    if($this->session->userdata('cid'))
    {
		$cid=$this->session->userdata('cid');
		$cookieid =$this->input->post('cookieid');
		$login='';

		if(empty($cookieid))
		{
			$cookieid  = get_cookie('cookie_id'); 
			$login  = 'login';  
		}
		

		$getcartproducts = $this->product_model->get_cartpro($cookieid);
		if($getcartproducts==false)
		{
			$response = array('status' => 400, 'data' =>'Products not found');
		}else{
			$random  = substr(rand(), 0, 5);
			$orderid= 'WPS'.$random;

			$orderdata = array(
			
				'order_id' => $orderid,
				'user_id' => $cid,
							);

			$cartproducts = $this->product_model->insert_order($orderdata);
			if($cartproducts)
			{
				$getproducts = $this->product_model->get_cartpro($cookieid);
				$total_prices=0;
				foreach($getproducts as $value)
				{
					$data = array(
						'order_id' => $cartproducts,
						'product_id' => $value->product_id,
						'quantity' => $value->quantity,
						'price' =>$value->pro_price,
						'total_price' =>$value->quantity*$value->pro_price,
						);

						$insertpro = $this->product_model->insert_orderpro($data);

						$total_prices +=$value->quantity*$value->pro_price;
				}
				$totalprice = array(
					'total_price' => $total_prices,
					);

					$updateprice = $this->product_model->update_totalprice($cartproducts,$totalprice);
			
						if ($updateprice == true) {
						
									$emptycart = $this->product_model->empty_cart($cookieid);
									delete_cookie('cookie_id'); 
									if($login =='login')
									{
										redirect('placeorder'); 
									}else{
										$response = array('status' => 200, 'data' =>'true');
									}
							
						}	

			}
			
			
		
		}
		 

        

    }else{
		
        $response = array('status' => 300, 'data' =>'Need to login');
    }

	$this->output->set_content_type('application/json');
	echo json_encode($response);
	
}

}
?>
