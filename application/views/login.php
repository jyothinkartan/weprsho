<!-- Checkout Section Begin -->
<section class="checkout spad">
        <div class="container">
            <div class="checkout__form">
               
                <form method="post" action="<?php echo base_url();?>check">
                    <div class="row">
                   
                    <div class="col-lg-3 col-md-3">
</div>
                        <div class="col-lg-6 col-md-6">
                            <div class="checkout__order">
                            <h4>Sign In</h4> 
                            <div id="frontsuccess">
                            <?php echo $this->session->flashdata('success'); ?>
                        </div>
                            <div id="fronterror"><?php echo $this->session->flashdata('error'); ?>
                                    </div>
                            
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Email<span>*</span></p>
                                        <input type="text" name="email">
                                    <div id="fronterror" ><?php echo $this->session->flashdata('email') ?>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Password<span>*</span></p>
                                        <input type="text" name="password">
                                        <div id="fronterror" ><?php echo $this->session->flashdata('password') ?></div>
                                    </div>
                                </div>
                            </div>
                            <div  class="row">
                                <div class="col-lg-12">
                                       <center><input class="site-btn" type="submit" value=Signin></center> 
                                    </div>
                                </div>
                                <div class="signup-section">Not a Register User Yet? <a href="<?php echo base_url();?>signup" class="text-info"> Sign Up</a>.</div>
      
                            </div>
                       
                                
                            </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3">
</div>
                    </div>
                </form>

            </div>
        </div>
    </section>
    <!-- Checkout Section End -->

    <script>
         setTimeout(function() {
            $('#frontsuccess').hide('fast');
        }, 3000);
        </script>