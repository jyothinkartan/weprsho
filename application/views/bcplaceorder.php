<style> 


.boxdiv {
  width: 500px;
  height: 200px;  
  padding: 50px;
  border: 1px solid black;
}
</style>

    <!-- Checkout Section Begin -->
    <section class="checkout spad">
        <div class="container">
            
            <div class="checkout__form">
                <h4>Billing Address</h4>
                <div id="fronterror"><?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                    <h3>Choose Address</h3>
                <form method="post">
                <div class="row"> 
                    <div class="col-lg-6 col-md-6">
                        <?php if(!empty($address)){
                            foreach ($address as  $value) {
                               
                           ?>
                            
                        
                          <div class="col-md-3 col-md-3">
                           <a href="#" id="show-hidden-menu" >
                            <div class="boxdiv">
                            <div class="checkout__order__products"><b><?php echo $value->fullname ?></b></div> 
                            <div class="checkout__order__products"> <?php echo $value->phone ?> </div>
                            <div class="checkout__order__products">Landmark : <?php echo $value->landmark ?> </div>
                            <div class="checkout__order__products">Address : <?php echo $value->address ?>  </div>
                            <div class="checkout__order__products"> <?php echo $value->city ?> &nbsp <?php echo $value->state ?>&nbsp <?php echo $value->country ?>&nbsp <?php echo $value->postcode ?></div>
                            </a>
                            <a href="#" class="primary-btn cart-btn" data-toggle="modal" data-target="#myModal">Edit Address</a>
                                    <a href="#" class="primary-btn cart-btn" data-toggle="modal" data-target="#myaddModal">Add New Address</a> 
                      
                        </div>
                         <?php    }
                            } ?>
                   </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="checkout__order hidden-menu" style="display: none;">
                                <h4>Your Order</h4>
                                <div class="checkout__order__products">Products <span>Total</span></div>
                                <ul>
                                    <?php 
                                  
                                    if($product) 
                                    {
                                       foreach ($product as $value) {
                                    ?>
                                    <li><?php echo $value->pro_name;?> <span>$<?php echo $value->total_price;?></span></li>
                                    <?php }
                                    }?>
                                </ul>
                                <div class="checkout__order__subtotal">Subtotal <span>$<?php echo $value->final_price;?></span></div>
                                <div class="checkout__order__total">Total <span>$<?php echo $value->final_price;?></span></div>
                              
                                <div class="checkout__input__checkbox">
                                   <h4>Choose Paymemt method</h4>
                                   <div id="fronterror"></div>
                                    <label for="paypal">
                                        Cach on delivery
                                        <input type="checkbox" name="payment" id="paypal" value="cod">
                                        <span class="checkmark"></span>
                                    </label>
                                   
                                </div>
                                <inut type="hidden" name="orderid" id="orderid" value="<?php echo empty($product[0]->oid) ? "" : $product[0]->oid ?>">
                               <inut type="hidden" name="ordertid" id="otableid"value="<?php echo empty($product[0]->order_id) ? "" : $product[0]->order_id ?>">
                                
                                <button type="submit" data-oid="<?php echo $product[0]->oid?>" data-tid="<?php echo $product[0]->order_id?>" class="site-btn placeorder">PLACE ORDER</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- Checkout Section End -->

<!-- for edit  Address -->
<div class="container">
  
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        
        </div>
        <div class="modal-body">
        <form method="post" action="<?php echo base_url(); ?>customeraddress">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Fist Name<span>*</span></p>
                                        <input type="hidden" name="id" value="<?php echo empty($customer[0]->id) ? "" : $customer[0]->id ?>" >
                                        <input type="text" name="fname" value="<?php echo empty($customer[0]->fname) ? "" : $customer[0]->fname ?>" >
                                    </div>
                                </div>  
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Last Name<span>*</span></p>
                                        <input type="text" name="lname" value="<?php echo empty($customer[0]->lname) ? "" : $customer[0]->lname ?>" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Phone<span>*</span></p>
                                        <input type="number" name="phone" value="<?php echo empty($customer[0]->phone) ? "" : $customer[0]->phone ?>" >
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                    <p>Town/City<span>*</span></p>
                                        <input type="text" name="city" value="<?php echo empty($customer[0]->city) ? "" : $customer[0]->city ?>" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                    <p>State<span>*</span></p>
                                        <input type="text" name="state" value="<?php echo empty($customer[0]->state) ? "" : $customer[0]->state ?>" >
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                    <p>Country<span>*</span></p>
                                        <input type="text" name="country" value="<?php echo empty($customer[0]->country) ? "" : $customer[0]->country ?>" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                    <p>Postcode / ZIP<span>*</span></p>
                                        <input type="number" name="postcode" value="<?php echo empty($customer[0]->postcode) ? "" : $customer[0]->postcode ?>" >
                                     </div>
                                </div>
                                
                            </div>
                          
                            <div class="checkout__input">
                            <p>Landmark<span>*</span></p>
                                <input type="text" name="landmark" placeholder="Street Address" class="checkout__input__add" value="<?php echo empty($customer[0]->landmark) ? "" : $customer[0]->landmark ?>" >
                                
                                <p>Address<span>*</span></p>
                                
                                <input type="text" name="address" placeholder="Apartment, suite, unite ect (optinal)" value="<?php echo empty($customer[0]->address) ? "" : $customer[0]->address ?>" >
                            </div>
                          
                           
                            <button type="submit" class="site-btn">Update</button>
                        </div>


                    </div>
                </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>

<!-- for Add New Address -->
<div class="container">
  
  <!-- Modal -->
  <div class="modal fade" id="myaddModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        
        </div>
        <div class="modal-body">
        <form method="post" action="<?php echo base_url(); ?>updateaddress">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Fist Name<span>*</span></p>
                                        <input type="hidden" name="id" value="<?php echo empty($customer[0]->id) ? "" : $customer[0]->id ?>" >
                                        <input type="text" name="fname" value="" >
                                    </div>
                                </div>  
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Last Name<span>*</span></p>
                                        <input type="text" name="lname" value="" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Phone<span>*</span></p>
                                        <input type="number" name="phone" value="" >
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                    <p>Town/City<span>*</span></p>
                                        <input type="text" name="city" value="" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                    <p>State<span>*</span></p>
                                        <input type="text" name="state" value="" >
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                    <p>Country<span>*</span></p>
                                        <input type="text" name="country" value="" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                    <p>Postcode / ZIP<span>*</span></p>
                                        <input type="number" name="postcode" value="" >
                                     </div>
                                </div>
                                
                            </div>
                          
                            <div class="checkout__input">
                            <p>Landmark<span>*</span></p>
                                <input type="text" name="landmark" placeholder="Street Address" class="checkout__input__add" value="" >
                                
                                <p>Address<span>*</span></p>
                                
                                <input type="text" name="address" placeholder="Apartment, suite, unite ect (optinal)" value="" >
                            </div>
                          
                           
                            <button type="submit" class="site-btn">Add</button>
                        </div>


                    </div>
                </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>

<script>
    $(document).ready(function() {
  $('#show-hidden-menu').click(function() {
    $('.hidden-menu').slideToggle("slow");
    $( "#show-hidden-menu" ).css('background', 'green');

    // Alternative animation for example
    // slideToggle("fast");
  });
}); 

$('.placeorder').click(function() {
    if (!$('#paypal').is(':checked')) {
        $("#fronterror").empty().append('please choose payment type');
     
        return false;
    }
    
    var oid = $(this).attr("data-oid");
    var tid = $(this).attr("data-tid");
      
    $.ajax({
        url: '<?php echo base_url('Customer/finish_order'); ?>',
        type: 'POST',
        data: {
            'oid': oid,
            'tid': tid,
        },
        dataType: 'json',
        success: function(data) {
                            //result=JSON.parse(result);
//  console.log(data);
                            if(data.status==200)
                            {
                                
                                window.location = "<?php echo base_url('success'); ?>";
                            }
                            if(data.status==300)
                            {
                                alert('Unable to place order Some products seems out of stock')
                            }
        }
    });
});

    </script>